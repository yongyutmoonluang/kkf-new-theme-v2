<?php
    $select = 'open_mk';
    $select2 = 'mk_1';
?>
<?php include('inc.header.php');?>
<?php include('inc.header_top.php');?>


    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <?php include('inc.menu.php');?>


        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- <div class="m-subheader-search">
                    <h2 class="m-subheader-search__title">
                        รายการออเดอร์
                        <span class="m-subheader-search__desc">ข้อความอธิบาย...</span>
                    </h2>
                </div> -->
            <div class="m-content">
                <!--Begin::Section-->
                <!-- <div class="row">
                    <div class="col-xl-12">
                        <div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            </button>
                            เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256)
                            <a href="#" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air btn_alert_small">
                                <i class="fa fa-sign-in-alt"></i>
                            </a>
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div class="col-xl-12">
                        <div class="m-portlet at-m-portlet-content m-portlet--mobile ">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-caption">
                                    <div class="m-portlet__head-title">
                                        <h3 class="m-portlet__head-text">
                                            รายการรออนุมัติ &nbsp;
                                            <span class="wa_head_text">ข้อมูลเวลาตามวันสอบถาม l สัปดาห์ที่ผ่านมาและสัปดาห์ปัจจุบัน l 15/11/2018 - 30/11/2018</span>
                                        </h3>
                                    </div>
                                </div>
                                <div class="m-portlet__head-tools">
                                    <ul class="m-portlet__nav">
                                        <!-- <li class="m-portlet__nav-item">
                                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="click" aria-expanded="true">
                                                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="การแสดงผล">
                                                    <i class="la la-th-list m--font-brand"></i>
                                                </a>
                                                <div class="m-dropdown__wrapper">
                                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                    <div class="m-dropdown__inner">
                                                        <div class="m-dropdown__body">
                                                            <div class="m-dropdown__content">
                                                                <ul class="m-nav">
                                                                    <li class="m-nav__section m-nav__section--first">
                                                                        <span class="m-nav__section-text">ตัวเลือกการแสดงผล</span>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon fa fa-calendar-alt"></i>
                                                                            <span class="m-nav__link-text">แสดงแผน</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon fas fa-file-alt"></i>
                                                                            <span class="m-nav__link-text">ตรวจสอบ/อนุมัติใบสั่งผลิต</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon fa fa-plus-circle"></i>
                                                                            <span class="m-nav__link-text">ทอเพิ่ม/ทอซ่อม</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon fa fa-minus-circle"></i>
                                                                            <span class="m-nav__link-text">รายการถูกยกเลิก</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon fa fa-crosshairs"></i>
                                                                            <span class="m-nav__link-text">กระทบจากผลผลิตไม่ได้เป้า</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon fa fa-random"></i>
                                                                            <span class="m-nav__link-text">เปลื่ยนแปลงออเดอร์</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon fa fa-flag"></i>
                                                                            <span class="m-nav__link-text">สินค้าเร่งด่วน</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon fa fa-tachometer-alt"></i>
                                                                            <span class="m-nav__link-text">น้อยกว่าขั้นต่ำ</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon fa fa-exclamation-triangle"></i>
                                                                            <span class="m-nav__link-text">จำนวนผลิตเกิน</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon fa fa-history"></i>
                                                                            <span class="m-nav__link-text">Lead time เกิน</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                            <i class="m-nav__link-icon fa fa-exclamation-circle"></i>
                                                                            <span class="m-nav__link-text">แผนเกินส่งมอบ</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="m-nav__section">
                                                                        <span class="m-nav__section-text">Useful Links</span>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                                <i class="m-nav__link-icon flaticon-info"></i>
                                                                                <span class="m-nav__link-text">FAQ</span>
                                                                            </a>
                                                                    </li>
                                                                    <li class="m-nav__item">
                                                                        <a href="" class="m-nav__link">
                                                                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                                                <span class="m-nav__link-text">Support</span>
                                                                            </a>
                                                                    </li>
                                                                    <li class="m-nav__separator m-nav__separator--fit m--hide">
                                                                    </li>
                                                                    <li class="m-nav__item m--hide">
                                                                        <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li> -->
                                        <!-- <li class="m-portlet__nav-item">
                                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
                                                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="การค้นหา">
                                                    <i class="la la-search-plus m--font-brand"></i>
                                                </a>
                                            </div>
                                        </li> -->
                                        <li class="m-portlet__nav-item">
                                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" id="m_accordion_3">
                                                <a class="collapsed m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="false">
                                                    <i class="la la-search-plus m--font-brand"></i>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="m-portlet__nav-item">
                                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
                                                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="พิมพ์">
                                                    <i class="la la-print m--font-brand"></i>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="m-portlet__nav-item">
                                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
                                                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ส่งออกข้อมูล">
                                                    <i class="la la-external-link-square m--font-brand"></i>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="m-portlet__body at-m-portlet-content-body">

                                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_3">
                                    <div class="m-accordion__item-content">
                                        <div class="m-portlet m-portlet--skin-dark m-portlet--rounded">
                                            <!-- <div class="m-portlet m-portlet--skin-dark m-portlet--bordered m-portlet--bordered-semi m-portlet--rounded"> -->
                                            <!-- <div class="m-portlet__head">
                                                <div class="m-portlet__head-caption">
                                                    <div class="m-portlet__head-title">
                                                        <h3 class="m-portlet__head-text">
                                                            Semi Bordered Style
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="m-portlet__body">
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-4 form-group">
                                                        <label>ตัวเลือกการค้นหา</label>
                                                        <select class="form-control m-input" id="exampleSelect1">
                                                            <option>ข้อมูลเวลาตามวันสอบถาม</option>
                                                            <option>ข้อมูลเวลาตามวันส่งมอบ</option>
                                                        </select>
                                                        <!-- <span class="m-form__help">Please enter your full name</span> -->
                                                    </div>
                                                    <div class="col-lg-4 form-group">
                                                        <label class="">ตามช่วงเวลา</label>
                                                        <select class="form-control m-input" id="exampleSelect1">
                                                            <option>สัปดาห์ที่ผ่านมาและสัปดาห์ปัจจุบัน</option>
                                                            <option>สัปดาห์ปัจจุบัน</option>
                                                            <option>เดือนที่ผ่านมาและเดือนปัจจุบัน</option>
                                                            <option>เดือนปัจจุบัน</option>
                                                            <option>กำหนดเอง</option>
                                                        </select>
                                                        <!-- <span class="m-form__help">Please enter your email</span> -->
                                                    </div>
                                                    <div class="col-lg-4 form-group">
                                                        <label>เลือกวันที่</label>
                                                        <div class="input-daterange input-group m_datepicker">
                                                            <input type="text" class="form-control m-input" name="start" placeholder="From" data-col-index="5" />
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                                                            </div>
                                                            <input type="text" class="form-control m-input" name="end" placeholder="To" data-col-index="5" />
                                                        </div>
                                                        <!-- <span class="m-form__help">Please enter your username</span> -->
                                                    </div>
                                                </div>

                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-6">
                                                        <div class="row">
                                                            <div class="col-lg-12 form-group">
                                                                <label>สาขา</label>
                                                                <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                            </div>
                                                            <div class="col-lg-12 form-group">
                                                                <label>สถานะ</label>
                                                                <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="row">
                                                            <div class="col-lg-6 form-group">
                                                                <label>ลูกค้า</label>
                                                                <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                            </div>
                                                            <div class="col-lg-6 form-group">
                                                                <label>ค้นหา</label>
                                                                <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                            </div>
                                                            <div class="col-lg-12 form-group">
                                                                <label>สัญลักษณ์</label>
                                                                <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <hr class="light">

                                                <div id="accordion">
                                                    <a data-toggle="collapse" href="#collapseOne">
                                                        <h6 class="m--font-brand"><u>ค้นหาแบบละเอียด</u></h6>
                                                    </a>
                                                    <div id="collapseOne" class="collapse" data-parent="#accordion">
                                                        <div class="form-group m-form__group row">

                                                            <div class="col-lg-3">
                                                                <div class="row">
                                                                    <div class="col-lg-12 form-group">
                                                                        <label>ชื่อลูกค้า</label>
                                                                        <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                                    </div>
                                                                    <div class="col-lg-12 form-group">
                                                                        <label>ประเทศ</label>
                                                                        <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                                    </div>
                                                                    <div class="col-lg-12 form-group">
                                                                        <label>กลุ่ม</label>
                                                                        <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <div class="row">
                                                                    <div class="col-lg-12 form-group">
                                                                        <label>วันที่สอบถาม</label>
                                                                        <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                                    </div>
                                                                    <div class="col-lg-12 form-group">
                                                                        <label>วันที่สั่งซื้อ</label>
                                                                        <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <div class="row">
                                                                    <div class="col-lg-12 form-group">
                                                                        <label>ประเภทสินค้า</label>
                                                                        <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                                    </div>
                                                                    <div class="col-lg-12 form-group">
                                                                        <label>ประเภทวัตถุดิบ</label>
                                                                        <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                                    </div>
                                                                    <div class="col-lg-12 form-group">
                                                                        <label>สีอวน</label>
                                                                        <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <div class="row">
                                                                    <div class="col-lg-12 form-group">
                                                                        <label>เบอร์ใย</label>
                                                                        <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                                    </div>
                                                                    <div class="col-lg-12 form-group">
                                                                        <label>ขนาดตา</label>
                                                                        <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                                    </div>
                                                                    <div class="col-lg-12 form-group">
                                                                        <label>จำนวนตา</label>
                                                                        <input class="form-control form-control-sm m-input" type="text" value="" id="">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="clearfix"></div>


                                                <div class="modal-footer m-stack__item m-stack__item--center" style="border-top: 0px solid #66677b;">
                                                    <button type="button" class="btn btn-brand"><i class="fa fa-search"></i> ค้นหา</button>
                                                    <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> บันทึก</button>
                                                    <button type="button" class="btn btn-metal-drak"><i class="fa fa-times"></i> ยกเลิก</button>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--begin: Datatable
                                <!--begin: Datatable -->
                                <!--begin: Datatable -->
                                <table class="table table-striped- table-bordered table-hover table-checkable at-vertical-align-middle" id="m_table_3">
                                    <!-- <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1"> -->
                                    <thead>
                                    <tr>
                                        <th>เลขที่สอบถาม/ออเดอร์</th>
                                        <th>ชื่อลูกค้า</th>
                                        <th>วันที่สอบถาม/สั่งซื้อ</th>
                                        <th>สถานะ</th>
                                        <th>สัญลักษณ์</th>
                                        <th>ตัวเลือก</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr collapse-table>
                                        <td id="m_table_item_1_head" data-toggle="collapse" href="#m_table_item_1_body" aria-expanded="false" class="at-cursor-pointer" onclick="Wahide3(this)">สอบถาม : DEQ1807060<br>ออเดอร์ : 61/07348</td>
                                        <td id="m_table_item_1_head" data-toggle="collapse" href="#m_table_item_1_body" aria-expanded="false" class="at-cursor-pointer wa-align-td" onclick="Wahide3(this)" nowrap>
													<span class="at-m-table-flags">
														<img class="img-fluid" src="assets/app/media/img/flags/th.png">
													</span>
                                            ร้านโชคไพบูลย์
                                        </td>
                                        <td id="m_table_item_1_head" data-toggle="collapse" href="#m_table_item_1_body" aria-expanded="false" class="at-cursor-pointer" onclick="Wahide3(this)">01/11/2018</td>
                                        <td nowrap>
                                            <!-- <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                    <i class="la la-ellipsis-v"></i>
                                                </a>
                                                <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                                    <div class="m-dropdown__inner">
                                                        <div class="m-dropdown__body ">
                                                            <div class="m-dropdown__content at-m-dropdown__content">
                                                                <table class="table table-striped- table-bordered table-hover">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                        <td class="m--font-brand">15/11/2018 - 26/12/2018</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>แผนผลิตส่วนหลัง เริ่ม - เสร็จ</td>
                                                                        <td class="m--font-brand">22/11/2018 - 27/12/2018</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                        <td class="m--font-brand">24/11/2018 - 30/12/2018</td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!--                                            <span class="m-badge m-badge--brand m-badge--wide">รอทอ</span>-->
                                            <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                        </td>
                                        <td nowrap>
                                            <div id="icon-hide3">
                                                <a href="#" class="btn btn-metal-drak m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                    <span class="m-badge at-m-badge-hourglass">7</span>
                                                    <i class="fa fa-hourglass-half"></i>
                                                </a>
                                                <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                    <i class="fa fa-exclamation"></i>
                                                </a>
                                                <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                    <i class="fa fa-history "></i>
                                                </a>
                                                <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                    <i class="fa fa-exclamation-triangle"></i>
                                                </a>
                                            </div>


<!--                                            <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">-->
<!--                                                <i class="fa fa-star"></i>-->
<!--                                            </a>-->
                                            <!-- <a href="#" class="btn btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ตรวจสอบ/อนุมัติใบสั่งผลิต">
                                                <i class="fab fa-wpforms"></i>
                                            </a> -->
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1">
                                                <i class="far fa-list-alt"></i>
                                            </a>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                <i class="fab fa-wpforms"></i>
                                            </a>
                                            <!-- <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="แก้ไข">
                                                <i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
                                            </a>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
                                                <i class="la la-trash-o"></i>
                                            </a> -->
                                        </td>
                                    </tr>
                                    </tr>
                                    <td nowrap colspan="6" style="padding: 0;border-top: 0px;">
                                        <div class="m-accordion__item-body collapse" id="m_table_item_1_body" class=" " role="tabpanel" aria-labelledby="m_table_item_1_head" data-parent="#m_accordion_3">
                                            <div class="m-accordion__item-content">

                                                <div class="at-m_table_item_1_body">
                                                    <div class="at-m_table_top_detail">
                                                        <div class="m-dropdown m-dropdown--inline m-dropdown--huge m-dropdown--arrow m-dropdown--align-left" m-dropdown-toggle="click" aria-expanded="true">
                                                            <a href="#" data-toggle="modal" data-target="#waModal_1" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                <i class="la la-ellipsis-v"></i>
                                                            </a>

                                                        </div>
                                                        เลขที่สอบถาม <span class="m--font-brand">DEQ0000003</span>
                                                        สถานะ <span class="m--font-warning">รอตลาดอนุมัติ</span>
                                                    </div>
                                                    <table class="table table-striped- table-bordered table-hover table-checkable at-vertical-align-top">
                                                        <thead>
                                                        <tr>
                                                            <th>ลำดับ</th>
                                                            <th>รหัสสินค้าตลาด/ผลิต</th>
                                                            <th>ชื่อสินค้า</th>
                                                            <th>สาขา</th>
                                                            <th>จำนวนสั่งซื้อ</th>
                                                            <th>สถานะ</th>
                                                            <th>สัญลักษณ์</th>
                                                            <th>ตัวเลือก</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
<!--                                                            tr main 1-->
                                                            <td>1</td>
                                                            <td>
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-left" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" data-toggle="modal" data-target="#m_modal_table" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>

                                                                </div>
                                                                <div class="wa_flex">
                                                                    ตลาด: H01400*11260250<br>  ผลิต : ( รหัส 15 หลัก )
                                                                </div>
                                                            </td>
                                                            <td nowrap>

                                                                0.14mm 1.1/2"*25md*20yd DK YOKO NW M206
                                                            </td>
                                                            <td>B&S1</td>
                                                            <td>500 PC</td>
                                                            <td nowrap>
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand">15/11/2018 - 01/12/2018</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand">25/11/2018 - 20/12/2018</td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
<!--                                                                <span class="m-badge m-badge--brand m-badge--wide">รอทอ</span>-->
                                                                <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                                            </td>
                                                            <td nowrap>
                                                                    <a href="#"  class="btn btn-metal-drak m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                                        <span class="m-badge at-m-badge-hourglass">7</span>
                                                                        <i class="fa fa-hourglass-half"></i>
                                                                    </a>
                                                                    <a href="#" data-toggle="modal" data-target="#m_modal_tree_icon_1" class="btn btn-danger m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                                        <i class="fa fa-exclamation"></i>
                                                                    </a>

<!--                                                                    <a href="#" class="btn btn-success m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">-->
<!--                                                                        <i class="fa fa-flag"></i>-->
<!--                                                                    </a>-->


<!--                                                                <a href="#" class="btn btn-success m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">-->
<!--                                                                    <i class="fa fa-star"></i>-->
<!--                                                                </a>-->
                                                                <!-- <a href="#" class="btn btn-brand m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ตรวจสอบ/อนุมัติใบสั่งผลิต">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                            <td nowrap>
                                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_3">
                                                                                    <i class="fa fa-chart-line"></i>
                                                                                </a>
                                                                <!-- <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                        </tr>
<!--                                                        tr main 2-->
                                                        <tr>
                                                            <td rowspan="4">2</td>
                                                            <td rowspan="4">ตลาด: H0A500H82010900<br>ผลิต : ( รหัส 15 หลัก )</td>
                                                            <td rowspan="4" class="product-name-line-1-box">110/2 3.70cm*90md*12m SK YOKO BLACK N101</td>
                                                        <tr>
                                                            <td  style="border-left: 1px solid #3c3d48;">BWC</td>
                                                            <td>400 PC</td>
                                                            <td nowrap>
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand">15/11/2018 - 25/11/2018</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand">20/11/2018 - 10/12/2018</td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                                            </td>
                                                            <td nowrap>
                                                                <a href="#"  class="btn btn-metal-drak m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                                    <span class="m-badge at-m-badge-hourglass">7</span>
                                                                    <i class="fa fa-hourglass-half"></i>
                                                                </a>
                                                                <a href="#" data-toggle="modal" data-target="#m_modal_tree_icon_5" class="btn btn-danger m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                                    <i class="fa fa-history "></i>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_3">
                                                                                    <i class="fa fa-chart-line"></i>
                                                                                </a>
                                                                <!-- <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="border-left: 1px solid #3c3d48; border-top: 0; "></td>
                                                            <td style="border-top: 0; ">300 PC</td>
                                                            <td nowrap style="border-top: 0; ">
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand">05/12/2018 - 15/12/2018</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand">10/12/2018 - 25/12/2018</td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td style="border-top: 0; ">
                                                                <!-- <a href="#" class="btn btn-brand m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ตรวจสอบ/อนุมัติใบสั่งผลิต">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                            <td style="border-top: 0; "></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="border-left: 1px solid #3c3d48; border-top: 0; "></td>
                                                            <td style="border-top: 0; ">200 PC</td>
                                                            <td nowrap style="border-top: 0; ">
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand">25/12/2018 - 30/12/2018</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand">30/12/2018 - 05/01/2019</td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td style="border-top: 0; "></td>
                                                            <td style="border-top: 0; "></td>
                                                        </tr>
                                                        </tr>
<!--                                                        tr 3-->
                                                        <tr>
                                                            <td rowspan="4">3</td>
                                                            <td rowspan="4">ตลาด: H0A500H82010900<br>ผลิต : ( รหัส 15 หลัก )</td>
                                                            <td rowspan="4" class="product-name-line-1-box">110/2 3.70cm*90md*12m SK YOKO BLACK N101</td>
                                                        <tr>
                                                            <td style="border-left: 1px solid #3c3d48;">BWC</td>
                                                            <td>400 PC</td>
                                                            <td nowrap >
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand">15/11/2018 - 25/11/2018</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand">20/11/2018 - 10/12/2018</td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
<!--                                                                <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>-->
                                                                <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                                            </td>
                                                            <td>
                                                                <a href="#"  class="btn btn-metal-drak m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                                    <span class="m-badge at-m-badge-hourglass">7</span>
                                                                    <i class="fa fa-hourglass-half"></i>
                                                                </a>
                                                                <a href="#" data-toggle="modal" data-target="#m_modal_tree_icon_4" class="btn btn-danger m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                                    <i class="fa fa-exclamation-triangle"></i>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_3">
                                                                                    <i class="fa fa-chart-line"></i>
                                                                                </a>
                                                                <!-- <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="border-left: 1px solid #3c3d48;  border-top: 0; "></td>
                                                            <td style=" border-top: 0; ">300 PC</td>
                                                            <td nowrap style="border-top: 0; ">
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand">05/12/2018 - 15/12/2018</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand">10/12/2018 - 25/12/2018</td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td style="border-top: 0; "></td>
                                                            <td style="border-top: 0; "></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="border-left: 1px solid #3c3d48;  border-top: 0; "></td>
                                                            <td style=" border-top: 0; ">200 PC</td>
                                                            <td nowrap style="border-top: 0; ">
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand">25/12/2018 - 30/12/2018</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand">30/12/2018 - 05/01/2019</td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td style="border-top: 0; "></td>
                                                            <td style="border-top: 0; "></td>
                                                        </tr>
                                                        <!-- tr 4-->
                                                        <tr>
                                                            <td>4</td>
                                                            <td>
                                                                ตลาด: M1701/*600Qก320<br>ผลิต : M0706\15CZ1110P
                                                            </td>
                                                            <td nowrap>

                                                                0.70mm 6*1110md*6 5ml DK YOKO SW
                                                            </td>
                                                            <td>BWC</td>
                                                            <td>50 PC</td>
                                                            <td nowrap>
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand">15/11/2018 - 01/12/2018</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand">25/11/2018 - 20/12/2018</td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
<!--                                                                <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>-->
                                                                <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                                            </td>
                                                            <td nowrap>
                                                                <a href="#"  class="btn btn-metal-drak m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                                    <span class="m-badge at-m-badge-hourglass">7</span>
                                                                    <i class="fa fa-hourglass-half"></i>
                                                                </a>
                                                                <a href="#" data-toggle="modal" data-target="#m_modal_tree_icon_2" class="btn btn-danger m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                                    <i class="fa fa-exclamation"></i>
                                                                </a>
                                                                <a href="#" data-toggle="modal" data-target="#m_modal_tree_icon_2" class="btn btn-danger m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                                    <i class="fa fa-history "></i>
                                                                </a>
                                                                <!--                    <a href="#" class="btn btn-success m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">-->
                                                                <!--                        <i class="fa fa-star"></i>-->
                                                                <!--                    </a>-->
                                                                <!-- <a href="#" class="btn btn-brand m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ตรวจสอบ/อนุมัติใบสั่งผลิต">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                            <td nowrap>
                                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_3">
                                                                                    <i class="fa fa-chart-line"></i>
                                                                                </a>
                                                                <!-- <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                        </tr>
                                                        <!-- tr 5-->
                                                        <tr>
                                                            <td>5</td>
                                                            <td>
                                                                ตลาด: X02009095<br>ผลิต : ( รหัส 15 หลัก )
                                                            </td>
                                                            <td nowrap>
                                                                MONO LINE 0.23mm 250 g / spool GREEN M528
                                                            </td>
                                                            <td>KKF2</td>
                                                            <td>90 PC</td>
                                                            <td nowrap>
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand">15/11/2018 - 01/12/2018</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand">25/11/2018 - 20/12/2018</td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
<!--                                                                <span class="m-badge m-badge--focus m-badge--wide">กำลังทอ</span>-->
<!--                                                                <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>-->
                                                                <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                                            </td>
                                                            <td nowrap>
                                                                <a href="#"  class="btn btn-metal-drak m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                                    <span class="m-badge at-m-badge-hourglass">7</span>
                                                                    <i class="fa fa-hourglass-half"></i>
                                                                </a>
                                                                <a href="#" data-toggle="modal" data-target="#m_modal_tree_icon_3" class="btn btn-danger m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                                    <i class="fa fa-exclamation"></i>
                                                                </a>
                                                                <a href="#" data-toggle="modal" data-target="#m_modal_tree_icon_3" class="btn btn-danger m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                                    <i class="fa fa-history "></i>
                                                                </a>

                                                                <a href="#" data-toggle="modal" data-target="#m_modal_tree_icon_3" class="btn btn-danger m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                                    <i class="fa fa-exclamation-triangle"></i>
                                                                </a>
                                                                <!--                    <a href="#" class="btn btn-success m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">-->
                                                                <!--                        <i class="fa fa-star"></i>-->
                                                                <!--                    </a>-->
                                                                <!-- <a href="#" class="btn btn-brand m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ตรวจสอบ/อนุมัติใบสั่งผลิต">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                            <td nowrap>
                                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_3">
                                                                                    <i class="fa fa-chart-line"></i>
                                                                                </a>
                                                                <!-- <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                        </tr>
                                                        <!-- tr 6-->
                                                        <tr>
                                                            <td>6</td>
                                                            <td>
                                                                ตลาด: X02009095<br>ผลิต : ( รหัส 15 หลัก )
                                                            </td>
                                                            <td nowrap>
                                                                MONO LINE 0.23mm 250 g / spool GREEN M528
                                                            </td>
                                                            <td>KKF2</td>
                                                            <td>190 KG <span class="m--font-brand">(152 PC)</span></td>
                                                            <td nowrap>
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand">15/11/2018 - 01/12/2018</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand">25/11/2018 - 20/12/2018</td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                                            </td>
                                                            <td nowrap>
                                                                <!--                    <a href="#" class="btn btn-success m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">-->
                                                                <!--                        <i class="fa fa-star"></i>-->
                                                                <!--                    </a>-->
                                                                <!-- <a href="#" class="btn btn-brand m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ตรวจสอบ/อนุมัติใบสั่งผลิต">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                            <td nowrap>
                                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_3">
                                                                                    <i class="fa fa-chart-line"></i>
                                                                                </a>
                                                                <!-- <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                        </tr>
                                                        <!-- tr 7-->
                                                        <tr>
                                                            <td>7</td>
                                                            <td>
                                                                <span>ตลาด: P16000/1205+140</span>
                                                                <br>
                                                                <span class="m--font-brand">[1st] : M1600//1205+140</span>
                                                                <br>
                                                                <span class="m--font-brand">ผลิต : ( รหัส 15 หลัก )</span>
                                                                <br>
                                                                <span class="m--font-brand">[2nd] : M190O/40005+028</span>
                                                                <br>
                                                                <span class="m--font-brand">ผลิต : ( รหัส 15 หลัก )</span>
                                                            </td>
                                                            <td nowrap>
                                                                <br>
                                                                [1st] : 0.60mm  120mmsq*14.5md*500ml  DK  YOKO  DARK GREEN M502
                                                                <br>
                                                                [2nd] : 0.90mm  400mmsq*2.5md*100ml  DK  YOKO  SW M215
                                                            </td>
                                                            <td>
                                                                KKF2
                                                                <br>
                                                                <span class="m--font-brand">B&S1</span>
                                                                <br>
                                                                <span class="m--font-brand">NR</span>

                                                            </td>
                                                            <td>
                                                                500 PC
                                                                <br>
                                                                <span class="m--font-brand">500 PC</span>
                                                                <br>
                                                                <span class="m--font-brand">1000 PC</span>
                                                            </td>
                                                            <td nowrap>
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand"></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                                            </td>
                                                            <td nowrap>
                                                                <!--                    <a href="#" class="btn btn-success m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">-->
                                                                <!--                        <i class="fa fa-star"></i>-->
                                                                <!--                    </a>-->
                                                                <!-- <a href="#" class="btn btn-brand m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ตรวจสอบ/อนุมัติใบสั่งผลิต">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                            <td nowrap>
                                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_3">
                                                                                    <i class="fa fa-chart-line"></i>
                                                                                </a>
                                                                <!-- <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                        </tr>
                                                        <!-- tr 8-->
                                                        <tr>
                                                            <td>8</td>
                                                            <td>
                                                                <span>ตลาด: P1129B4.5060503</span>
                                                                <br>
                                                                <span class="m--font-brand">[1st] : M1129//4.5060503</span>
                                                                <br>
                                                                <span class="m--font-brand">ผลิต : ( รหัส 15 หลัก )</span>
                                                                <br>
                                                                <span class="m--font-brand">[2nd] : N0040/*5125+121</span>
                                                                <br>
                                                                <span class="m--font-brand">ผลิต : ( รหัส 15 หลัก )</span>
                                                            </td>
                                                            <td nowrap>
                                                                <br>
                                                                [1st] : 0.60mm  120mmsq*14.5md*500ml  DK  YOKO  DARK GREEN M502
                                                                <br>
                                                                [2nd] : 0.90mm  400mmsq*2.5md*100ml  DK  YOKO  SW M215
                                                            </td>
                                                            <td>
                                                                KKF2
                                                                <br>
                                                                <span class="m--font-brand">B&S1</span>
                                                                <br>
                                                                <span class="m--font-brand">NR</span>

                                                            </td>
                                                            <td>
                                                                200 PC
                                                                <br>
                                                                <span class="m--font-brand">200 PC</span>
                                                                <br>
                                                                <span class="m--font-brand">200 PC</span>
                                                            </td>
                                                            <td nowrap>
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand"></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                                            </td>
                                                            <td nowrap>

                                                                <!--                    <a href="#" class="btn btn-success m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">-->
                                                                <!--                        <i class="fa fa-star"></i>-->
                                                                <!--                    </a>-->
                                                                <!-- <a href="#" class="btn btn-brand m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ตรวจสอบ/อนุมัติใบสั่งผลิต">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                            <td nowrap>
                                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_3">
                                                                                    <i class="fa fa-chart-line"></i>
                                                                                </a>
                                                                <!-- <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                        </tr>
                                                        <!-- tr 9-->
                                                        <tr>
                                                            <td>9</td>
                                                            <td>
                                                                <span>ตลาด : </span>
                                                                <br>
                                                                <span class="">ผลิต : </span>
                                                            </td>
                                                            <td nowrap>
                                                                <div>( สินค้าเตลา )</div>
                                                            </td>
                                                            <td>
                                                                BWC
                                                            </td>
                                                            <td>
                                                                50 PC
                                                            </td>
                                                            <td nowrap>
                                                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">
                                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                        <i class="la la-ellipsis-v"></i>
                                                                    </a>
                                                                    <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">
                                                                        <span class="m-dropdown__arrow light m-dropdown__arrow--center"></span><!-- light -->
                                                                        <div class="m-dropdown__inner at_inner_light"><!-- light -->
                                                                            <div class="m-dropdown__body">
                                                                                <div class="m-dropdown__content at-m-dropdown__content">
                                                                                    <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>
<!--                                                                                            <td class="m--font-brand">15/12/2018</td>-->
                                                                                            <td class="m--font-brand" data-toggle="modal" data-target="#modal_Sp_Date">
                                                                                                <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_Sp_Date" class="wa_href">15/12/2018</a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>จำนวนชุดทอ : ผืนทอ</td>
                                                                                            <td class="m--font-brand">
                                                                                                <a href="#" data-toggle="modal" data-target="#modal_amountChuthor" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
                                                                                                    <i class="la la-ellipsis-v"></i>
                                                                                                </a>
                                                                                                <a href="" data-toggle="modal" data-target="#modal_amountChuthor" class="wa_href">50 ชุด : 550 ผืน (+10%)</a>
<!--                                                                                                50 ชุด : 550 ผืน (+10%)-->
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                                                                            <td class="m--font-brand"></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                                                                            <td class="m--font-brand"></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                                            </td>
                                                            <td nowrap>

                                                                <!--                    <a href="#" class="btn btn-success m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">-->
                                                                <!--                        <i class="fa fa-star"></i>-->
                                                                <!--                    </a>-->
                                                                <!-- <a href="#" class="btn btn-brand m-btn btn-sm m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ตรวจสอบ/อนุมัติใบสั่งผลิต">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                            <td nowrap>
                                                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_3">
                                                                                    <i class="fa fa-chart-line"></i>
                                                                                </a>
                                                                <!-- <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn-not-b m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                                    <i class="fab fa-wpforms"></i>
                                                                </a> -->
                                                            </td>
                                                        </tr>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>สอบถาม : DEQ1807060<br>ออเดอร์ : 61/07348</td>
                                        <td nowrap>
													<span class="at-m-table-flags">
														<img class="img-fluid" src="assets/app/media/img/flags/th.png">
													</span>
                                            ร้านโชคไพบูลย์
                                        </td>
                                        <td>01/12/2018</td>
                                        <td nowrap>
<!--                                            <span class="m-badge m-badge--warning m-badge--wide">รอวางแผนปรับแผนใหม่</span>-->
                                            <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="btn btn-metal-drak m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                <span class="m-badge at-m-badge-hourglass">7</span>
                                                <i class="fa fa-hourglass-half"></i>
                                            </a>
                                            <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่วางแผนแล้ว เกินกำหนดวันส่งเข้าคลังครบ">
                                                <i class="fa fa-exclamation"></i>
                                            </a>
<!--                                            <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่มีจำนวนสั่งซื้อ น้อยกว่าจำนวนผลิตขั้นต่ำ">-->
<!--                                                <i class="fa fa-greater-than-equal"></i>-->
<!--                                            </a>-->
                                            <!-- <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">
                                                <i class="fa fa-flag"></i>
                                            </a> -->
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1">
                                                <i class="far fa-list-alt"></i>
                                            </a>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                <i class="fab fa-wpforms"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สอบถาม : DEQ1807060<br>ออเดอร์ : 61/07348</td>
                                        <td nowrap>
													<span class="at-m-table-flags">
														<img class="img-fluid" src="assets/app/media/img/flags/th.png">
													</span>
                                            ร้านโชคไพบูลย์
                                        </td>
                                        <td>01/12/2018</td>
                                        <td nowrap>
                                            <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="btn btn-metal-drak m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                <span class="m-badge at-m-badge-hourglass">7</span>
                                                <i class="fa fa-hourglass-half"></i>
                                            </a>
                                            <!-- <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">
                                                <i class="fa fa-flag"></i>
                                            </a>
                                            <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">
                                                <i class="fa fa-flag"></i>
                                            </a> -->
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1">
                                                <i class="far fa-list-alt"></i>
                                            </a>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                <i class="fab fa-wpforms"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สอบถาม : DEQ1807060<br>ออเดอร์ : 61/07348</td>
                                        <td nowrap>
													<span class="at-m-table-flags">
														<img class="img-fluid" src="assets/app/media/img/flags/th.png">
													</span>
                                            ร้านโชคไพบูลย์
                                        </td>
                                        <td>01/12/2018</td>
                                        <td nowrap>
<!--                                            <span class="m-badge m-badge--success m-badge--wide">ทอครบแล้ว</span>-->
                                            <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="btn btn-metal-drak m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                <span class="m-badge at-m-badge-hourglass">7</span>
                                                <i class="fa fa-hourglass-half"></i>
                                            </a>
                                            <!-- <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">
                                                <i class="fa fa-flag"></i>
                                            </a>
                                            <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">
                                                <i class="fa fa-flag"></i>
                                            </a> -->
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1">
                                                <i class="far fa-list-alt"></i>
                                            </a>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                <i class="fab fa-wpforms"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สอบถาม : DEQ1807060<br>ออเดอร์ : 61/07348</td>
                                        <td nowrap>
													<span class="at-m-table-flags">
														<img class="img-fluid" src="assets/app/media/img/flags/th.png">
													</span>
                                            ร้านโชคไพบูลย์
                                        </td>
                                        <td>01/12/2018</td>
                                        <td nowrap>
<!--                                            <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>-->
                                            <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="btn btn-metal-drak m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                <span class="m-badge at-m-badge-hourglass">7</span>
                                                <i class="fa fa-hourglass-half"></i>
                                            </a>
<!--                                            <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">-->
<!--                                                <i class="fa fa-star"></i>-->
<!--                                            </a>-->

                                        </td>
                                        <td nowrap>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1">
                                                <i class="far fa-list-alt"></i>
                                            </a>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                <i class="fab fa-wpforms"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สอบถาม : DEQ1807060<br>ออเดอร์ : 61/07348</td>
                                        <td nowrap>
													<span class="at-m-table-flags">
														<img class="img-fluid" src="assets/app/media/img/flags/cn.png">
													</span>
                                            SINHONLY FISH NETS PTE LTD.
                                        </td>
                                        <td>01/12/2018</td>
                                        <td nowrap>
<!--                                            <span class="m-badge m-badge--warning m-badge--wide">รอวางแผนปรับแผนใหม่<br>จากการเปลี่ยนออเดอร์</span>-->
                                            <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="btn btn-metal-drak m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                <span class="m-badge at-m-badge-hourglass">7</span>
                                                <i class="fa fa-hourglass-half"></i>
                                            </a>
                                            <a href="#" class="btn btn-metal-drak m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่มีการเปลี่ยนแปลงออเดอร์">
                                                <i class="fa fa-random"></i>
                                            </a>
                                            <!-- <button type="button" class="btn btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รอวางแผนใหม่" disabled="disabled">
                                                <i class="fab fa-wpforms"></i>
                                            </button> -->
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1">
                                                <i class="far fa-list-alt"></i>
                                            </a>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                <i class="fab fa-wpforms"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สอบถาม : DEQ1807060<br>ออเดอร์ : 61/07348</td>
                                        <td nowrap>
													<span class="at-m-table-flags">
														<img class="img-fluid" src="assets/app/media/img/flags/cn.png">
													</span>
                                            SINHONLY FISH NETS PTE LTD.
                                        </td>
                                        <td>01/12/2018</td>
                                        <td nowrap>
<!--                                            <span class="m-badge m-badge--brand m-badge--wide">รอทอ</span>-->
                                            <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>

                                        </td>
                                        <td nowrap>
                                            <a href="#" class="btn btn-metal-drak m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                <span class="m-badge at-m-badge-hourglass">7</span>
                                                <i class="fa fa-hourglass-half"></i>
                                            </a>
                                            <!-- <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">
                                                <i class="fa fa-flag"></i>
                                            </a> -->
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1">
                                                <i class="far fa-list-alt"></i>
                                            </a>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                <i class="fab fa-wpforms"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สอบถาม : DEQ1807060<br>ออเดอร์ : 61/07348</td>
                                        <td nowrap>
													<span class="at-m-table-flags">
														<img class="img-fluid" src="assets/app/media/img/flags/cn.png">
													</span>
                                            SINHONLY FISH NETS PTE LTD.
                                        </td>
                                        <td>01/12/2018</td>
                                        <td nowrap>
<!--                                            <span class="m-badge m-badge--brand m-badge--wide">รอทอ</span>-->
                                            <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="btn btn-metal-drak m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                <span class="m-badge at-m-badge-hourglass">7</span>
                                                <i class="fa fa-hourglass-half"></i>
                                            </a>
<!--                                            <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="แผนเกินส่งมอบ">-->
<!--                                                <i class="fa fa-exclamation"></i>-->
<!--                                            </a>-->
<!--                                            <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการที่มีการขอทอเพิ่มหรือทอซ่อม">-->
<!--                                                <i class="fa fa-plus"></i>-->
<!--                                            </a>-->
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1">
                                                <i class="far fa-list-alt"></i>
                                            </a>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                <i class="fab fa-wpforms"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สอบถาม : DEQ1807060<br>ออเดอร์ : 61/07348</td>
                                        <td nowrap>
													<span class="at-m-table-flags">
														<img class="img-fluid" src="assets/app/media/img/flags/cn.png">
													</span>
                                            SINHONLY FISH NETS PTE LTD.
                                        </td>
                                        <td>01/12/2018</td>
                                        <td nowrap>
<!--                                            <span class="m-badge m-badge--brand m-badge--wide">รอทอ</span>-->
                                            <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="btn btn-metal-drak m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                <span class="m-badge at-m-badge-hourglass">7</span>
                                                <i class="fa fa-hourglass-half"></i>
                                            </a>
<!--                                            <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="แผนเกินส่งมอบ">-->
<!--                                                <i class="fa fa-exclamation"></i>-->
<!--                                            </a>-->
<!--                                            <button type="button" class="btn btn-dark m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการถูกยกเลิกจำนวนเมื่อเริ่มผลิตแล้ว">-->
<!--                                                <i class="fa fa-minus"></i>-->
<!--                                            </button>-->
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1">
                                                <i class="far fa-list-alt"></i>
                                            </a>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                <i class="fab fa-wpforms"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>สอบถาม : DEQ1807060<br>ออเดอร์ : 61/07348</td>
                                        <td nowrap>
													<span class="at-m-table-flags">
														<img class="img-fluid" src="assets/app/media/img/flags/cn.png">
													</span>
                                            SINHONLY FISH NETS PTE LTD.
                                        </td>
                                        <td>01/12/2018</td>
                                        <td nowrap>
                                            <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
<!--                                            <span class="m-badge m-badge--brand m-badge--wide">รอทอ</span>-->
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="btn btn-metal-drak m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="จะหมดอายุในอีก 7 วัน">
                                                <span class="m-badge at-m-badge-hourglass">7</span>
                                                <i class="fa fa-hourglass-half"></i>
                                            </a>
<!--                                            <a href="#" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" >-->
<!--                                                <div class="m-demo-icon">-->
<!--                                                    <div class="m-demo-icon__preview">-->
<!--                                                        <i class="la la-flag"></i>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                            </a>-->
<!--                                            <span class="icon icon-flag more-size red">  	la-flag   </span>-->
                                            <!-- <a href="#" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="สินค้าเร่งด่วน">
                                                <i class="fa fa-flag"></i>
                                            </a> -->
<!--                                            <button type="button" class="btn btn-dark m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="รายการผลกระทบจากผลผลิตไม่ได้เป้า">-->
<!--                                                <i class="fa fa-crosshairs"></i>-->
<!--                                            </button>-->
                                        </td>
                                        <td nowrap>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1">
                                                <i class="far fa-list-alt"></i>
                                            </a>
                                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_2">
                                                <i class="fab fa-wpforms"></i>
                                            </a>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                            <!--end: Datatable -->
                        </div>
                    </div>
                </div>
            </div>
            <!--End::Section-->
        </div>
    </div>
    </div>
    <!-- end:: Body -->









<?php include('inc.modal.php');?>
<?php include('inc.footer.php');?>