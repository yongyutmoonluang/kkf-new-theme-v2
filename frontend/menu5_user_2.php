<?php include('inc.header.php');?>
		<!-- BEGIN: Header -->
		<header id="m_header" class="m-grid__item m-header" m-minimize-offset="200" m-minimize-mobile-offset="200">
		    <div class="m-container m-container--fluid m-container--full-height">
		        <div class="m-stack m-stack--ver m-stack--desktop">
		            <!-- BEGIN: Brand -->
		            <div class="m-stack__item m-brand  m-brand--skin-light ">
		                <div class="m-stack m-stack--ver m-stack--general">
		                    <div class="m-stack__item m-stack__item--middle m-brand__logo">
		                        <a href="index.html" class="m-brand__logo-wrapper">
		                                <img alt="" src="assets/demo/media/img/logo/logo.png" class="pc" />
		                                <img alt="" src="assets/demo/media/img/logo/logo2_color.png" class="mobile" />
		                            </a>
		                        <h3 class="m-header__title">ระบบวางแผนการผลิต</h3>
		                    </div>
		                    <div class="m-stack__item m-stack__item--middle m-brand__tools">
		                        <!-- BEGIN: Responsive Aside Left Menu Toggler -->
		                        <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
		                                <span></span>
		                            </a>
		                        <!-- END -->
		                        <!-- BEGIN: Responsive Header Menu Toggler -->
		                        <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
		                                <span></span>
		                            </a>
		                        <!-- END -->
		                        <!-- BEGIN: Topbar Toggler -->
		                        <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
		                                <i class="flaticon-more"></i>
		                            </a>
		                        <!-- BEGIN: Topbar Toggler -->
		                    </div>
		                </div>
		            </div>
		            <!-- END: Brand -->
		            <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
		                <!-- <div class="m-header__title">
		                        <h3 class="m-header__title-text">รายการออเดอร์</h3>
		                    </div> -->
		                <!-- BEGIN: Horizontal Menu -->
		                <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
		                    <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
		                        <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
		                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link">
		                                <i class="m-menu__link-icon flaticon-line-graph"></i>
		                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน</span>
		                                <i class="m-menu__hor-arrow la la-angle-down"></i>
		                                <i class="m-menu__ver-arrow la la-angle-right"></i>
		                            </a>
		                                <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left"><span class="m-menu__arrow m-menu__arrow--adjust"></span>
		                                    <ul class="m-menu__subnav">
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-chart-area"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 1</span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-chart-bar"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 2</span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-chart-line"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 3</span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-chart-pie"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 5</span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-tasks"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 6</span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-tablets"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 7</span>
		                                            </a>
		                                        </li>
		                                    </ul>
		                                </div>
		                            </li>
		                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link">
		                                <i class="m-menu__link-icon flaticon-users m--font-warning"></i>
		                                <span class="m-menu__link-text m--font-warning">การตั้งค่าผู้ใช้งาน</span>
		                                <i class="m-menu__hor-arrow la la-angle-down m--font-warning"></i>
		                                <i class="m-menu__ver-arrow la la-angle-right m--font-warning"></i>
		                            </a>
		                                <div class="m-menu__submenu  m-menu__submenu--fixed m-menu__submenu--left" style="width:430px">
		                                    <span class="m-menu__arrow m-menu__arrow--adjust"></span>
		                                    <div class="m-menu__subnav">
		                                        <ul class="m-menu__content">
		                                            <li class="m-menu__item">
		                                                <h3 class="m-menu__heading m-menu__toggle">
		                                                    <i class="m-menu__link-icon fa fa-user-cog"></i>
		                                                    <span class="m-menu__link-text">ผู้ดูแลระบบ</span>
		                                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
		                                                </h3>
		                                                <ul class="m-menu__inner">
		                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                                        <a href="#" class="m-menu__link ">
		                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
		                                                        <span class="m-menu__link-text">
		                                                            กำหนดสิทธิ์ผู้ใช้งานระบบ
		                                                        </span>
		                                                        </a>
		                                                    </li>
		                                                    <li class="m-menu__item m-menu__item--active" m-menu-link-redirect="1" aria-haspopup="true">
		                                                        <a href="menu5_user_2.php" class="m-menu__link ">
		                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
		                                                        <span class="m-menu__link-text">
		                                                            จัดการผู้ใช้งานระบบ
		                                                        </span>
		                                                        </a>
		                                                    </li>
		                                                </ul>
		                                            </li>
		                                            <li class="m-menu__item">
		                                                <h3 class="m-menu__heading m-menu__toggle">
		                                                    <i class="m-menu__link-icon fa fa-user-edit"></i>
		                                                    <span class="m-menu__link-text">ผู้ใช้งาน</span>
		                                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
		                                                </h3>
		                                                <ul class="m-menu__inner">
		                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                                        <a href="#" class="m-menu__link ">
		                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
		                                                        <span class="m-menu__link-text">
		                                                            ข้อมูลส่วนตัว
		                                                        </span>
		                                                        </a>
		                                                    </li>
		                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                                        <a href="#" class="m-menu__link ">
		                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
		                                                        <span class="m-menu__link-text">
		                                                            การแจ้งเตือน
		                                                        </span>
		                                                        </a>
		                                                    </li>
		                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                                        <a href="#" class="m-menu__link ">
		                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
		                                                        <span class="m-menu__link-text">
		                                                            ข้อความ
		                                                        </span>
		                                                        </a>
		                                                    </li>
		                                                </ul>
		                                            </li>
		                                        </ul>
		                                    </div>
		                                </div>
		                            </li>
		                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
		                                <a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link">
		                                    <i class="m-menu__link-icon flaticon-alert"></i>
		                                    <span class="m-menu__link-title"> 
		                                        <span class="m-menu__link-wrap"> 
		                                            <span class="m-menu__link-text">การแจ้งเตือน</span> 
		                                            <span class="m-menu__link-badge">
		                                                <span class="m-badge m-badge--danger m-badge--wide">ใหม่(12)</span>
		                                            </span>
		                                        </span>
		                                    </span>
		                                    <i class="m-menu__hor-arrow la la-angle-down"></i>
		                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
		                                </a>
		                                <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left" style="width:450px">
		                                    <span class="m-menu__arrow m-menu__arrow--adjust"></span>
		                                    <ul class="m-menu__subnav">
		                                        <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-bell text-danger"></i>
		                                                <span class="m-menu__link-text">
		                                                    เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256)
		                                                </span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-bell text-danger"></i>
		                                                <span class="m-menu__link-text">
		                                                    เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256)
		                                                </span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-bell text-danger"></i>
		                                                <span class="m-menu__link-text">
		                                                    เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256)
		                                                </span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-bell text-warning"></i>
		                                                <span class="m-menu__link-text">
		                                                    เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256)
		                                                </span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-bell text-warning"></i>
		                                                <span class="m-menu__link-text">
		                                                    เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256)
		                                                </span>
		                                            </a>
		                                        </li>
		                                    </ul>
		                                </div>
		                            </li>
		                        </ul>
		                    </div>
		                <!-- END: Horizontal Menu -->
		                <!-- BEGIN: Topbar -->
		                <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
		                    <div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-light" id="m_quicksearch" m-quicksearch-mode="default">
		                        <!--BEGIN: Search Form -->
		                        <!-- <form class="m-header-search__form">
		                            <div class="m-header-search__wrapper">
		                                <span class="m-header-search__icon-search" id="m_quicksearch_search">
		                                        <i class="flaticon-search"></i>
		                                    </span>
		                                <span class="m-header-search__input-wrapper">
		                                        <input autocomplete="off" type="text" name="q" class="m-header-search__input" value="" placeholder="Search..." id="m_quicksearch_input">
		                                    </span>
		                                <span class="m-header-search__icon-close" id="m_quicksearch_close">
		                                        <i class="la la-remove"></i>
		                                    </span>
		                                <span class="m-header-search__icon-cancel" id="m_quicksearch_cancel">
		                                        <i class="la la-remove"></i>
		                                    </span>
		                            </div>
		                        </form> -->
		                        <!--END: Search Form -->
		                        <!--BEGIN: Search Results -->
		                        <div class="m-dropdown__wrapper">
		                            <div class="m-dropdown__arrow m-dropdown__arrow--center"></div>
		                            <div class="m-dropdown__inner">
		                                <div class="m-dropdown__body">
		                                    <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-height="300" data-mobile-height="200">
		                                        <div class="m-dropdown__content m-list-search m-list-search--skin-light">
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                        <!--BEGIN: END Results -->
		                    </div>
		                    <div class="m-stack__item m-topbar__nav-wrapper">
		                        <ul class="m-topbar__nav m-nav m-nav--inline">
		                            <li class="m-nav__item m-topbar__quick-actions m-dropdown m-dropdown--skin-light m-dropdown--large m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light">
		                                <a href="#" class="m-nav__link m-dropdown__toggle">
		                                    <span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
		                                    <span class="m-nav__link-icon">
		                                        <span class="m-nav__link-icon-wrapper">
		                                            <i class="fa fa-expand"></i>
		                                        </span>
		                                    </span>
		                                </a>
		                            </li>
		                            <li class="m-nav__item m-topbar__languages m-dropdown m-dropdown--small m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click">
		                                    <a href="#" class="m-nav__link m-dropdown__toggle">
		                                        <span class="m-nav__link-text">
		                                            <img class="m-topbar__language-selected-img" src="assets/app/media/img/flags/th.png">
		                                        </span>
		                                    </a>
		                                    <div class="m-dropdown__wrapper">
		                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
		                                        <div class="m-dropdown__inner">
		                                            <!-- <div class="m-dropdown__header m--align-center" style="background: url(assets/app/media/img/misc/quick_actions_bg.jpg); background-size: cover;">
		                                                <span class="m-dropdown__header-subtitle">Select your language</span>
		                                            </div> -->
		                                            <div class="m-dropdown__body">
		                                                <div class="m-dropdown__content">
		                                                    <ul class="m-nav m-nav--skin-light">
		                                                        <!-- <li class="m-nav__item">
		                                                            <a href="#" class="m-nav__link">
		                                                                <span class="m-nav__link-icon"><img class="m-topbar__language-img" src="assets/app/media/img/flags/th.png"></span>
		                                                                <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">ไทย</span>
		                                                            </a>
		                                                        </li> -->
		                                                        <li class="m-nav__item m-nav__item--active">
		                                                            <a href="#" class="m-nav__link m-nav__link--active">
		                                                                <span class="m-nav__link-icon"><img class="m-topbar__language-img" src="assets/app/media/img/flags/en.png"></span>
		                                                                <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">English</span>
		                                                            </a>
		                                                        </li>
		                                                        <li class="m-nav__item">
		                                                            <a href="#" class="m-nav__link">
		                                                                <span class="m-nav__link-icon"><img class="m-topbar__language-img" src="assets/app/media/img/flags/cn.png"></span>
		                                                                <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">中国</span>
		                                                            </a>
		                                                        </li>
		                                                    </ul>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </li>
		                            <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
		                                <a href="#" class="m-nav__link m-dropdown__toggle">
		                                    <span class="m-topbar__userpic ">
		                                        <img src="assets/app/media/img/users/user1.jpg" class="m--img-rounded m--marginless m--img-centered" alt="" />
		                                    </span>
		                                    <span class="m-nav__link-icon m-topbar__usericon m--hide">
		                                        <span class="m-nav__link-icon-wrapper">
		                                            <i class="fa fa-user-alt"></i>
		                                        </span>
		                                    </span>
		                                    <span class="m-topbar__username m--hide">AAA</span>
		                                </a>
		                                <div class="m-dropdown__wrapper">
		                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
		                                    <div class="m-dropdown__inner">
		                                            <div class="m-dropdown__header m--align-center">
		                                                <div class="m-card-user m-card-user--skin-light">
		                                                    <div class="m-card-user__pic">
		                                                        <img src="assets/app/media/img/users/user1.jpg" class="m--img-rounded m--marginless" alt="" />
		                                                    </div>
		                                                    <div class="m-card-user__details">
		                                                        <span class="m-card-user__name m--font-weight-500">ชื่อ นามสกุล</span>
		                                                        <span class="">ตำแหน่งหรือสิทธิ์การใช้งาน</span>
		                                                    </div>
		                                                </div>
		                                            </div>
		                                            <!-- <div class="m-dropdown__body m--bg-brand">
		                                                <div class="m-dropdown__content">
		                                                    <ul class="m-nav m-nav--skin-light">
		                                                        <li class="m-nav__item">
		                                                            <a href="snippets/pages/user/login-1.html" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">ออกจากระบบ</a>
		                                                        </li>
		                                                    </ul>
		                                                </div>
		                                            </div> -->
		                                        </div>
		                                    </div>
		                            </li>
		                            <li id="" class="m-nav__item">
		                                <a href="index.html" class="m-nav__link m-dropdown__toggle">
		                                    <span class="m-nav__link-icon m-nav__link-icon-alt">
		                                        <span class="m-nav__link-icon-wrapper">
		                                            <i class="fa fa-power-off"></i>
		                                        </span>
		                                    </span>
		                                </a>
		                            </li>
		                            <!-- <li id="m_quick_sidebar_toggle" class="m-nav__item">
		                                <a href="#" class="m-nav__link m-dropdown__toggle">
		                                    <span class="m-nav__link-icon m-nav__link-icon-alt">
		                                        <span class="m-nav__link-icon-wrapper">
		                                            <i class="flaticon-grid-menu"></i>
		                                        </span>
		                                    </span>
		                                </a>
		                            </li> -->
		                        </ul>
		                    </div>
		                </div>
		                <!-- END: Topbar -->
		            </div>
		        </div>
		    </div>
		</header>
		<!-- END: Header -->


        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
			    <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
			    <!-- BEGIN: Aside Menu -->
			    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" m-menu-scrollable="1" m-menu-dropdown-timeout="500">
			        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			            <!-- <li class="m-menu__section ">
			                <h4 class="m-menu__section-text">ระบบวางแผนการผลิต</h4>
			                <i class="m-menu__section-icon flaticon-more-v2"></i>
			            </li> -->
			            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                <a href="#" class="m-menu__link ">
			                        <i class="m-menu__link-icon flaticon-analytics"></i>
			                        <span class="m-menu__link-text">หน้าแรก</span>
			                    </a>
			            </li>
			            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
			                <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                        <i class="m-menu__link-icon flaticon-interface-8"></i>
			                        <span class="m-menu__link-title"> 
			                            <span class="m-menu__link-wrap"> 
			                                <span class="m-menu__link-text">การตลาด</span> 
			                                <!-- <span class="m-menu__link-badge">
			                                    <span class="m-badge m-badge--danger m-badge--wide">3</span>
			                                </span> --> 
			                            </span>
			                        </span>
			                        <i class="m-menu__ver-arrow la la-angle-right"></i>
			                    </a>
			                <div class="m-menu__submenu ">
			                    <span class="m-menu__arrow"></span>
			                    <ul class="m-menu__subnav">
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                        <span></span>
			                                    </i>
			                                    <span class="m-menu__link-text">รายการรออนุมัติ</span>
			                                </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                        <span></span>
			                                    </i>
			                                    <span class="m-menu__link-text">รายการสอบถาม</span>
			                                </a>
			                        </li>
			                        <li class="m-menu__item" aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="menu1_mk_3.php" class="m-menu__link ">
		                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
		                                        <span></span>
		                                    </i>
		                                    <span class="m-menu__link-text">รายการออเดอร์ </span>
		                                </a>
			                        </li>
			                    </ul>
			                </div>
			            </li>
			            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
			                <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                    <i class="m-menu__link-icon flaticon-calendar-3"></i>
			                    <span class="m-menu__link-text">การวางแผนการผลิต</span>
			                    <i class="m-menu__ver-arrow la la-angle-right"></i>
			                </a>
			                <div class="m-menu__submenu ">
			                    <span class="m-menu__arrow"></span>
			                    <ul class="m-menu__subnav">
			                        <li class="m-menu__item" aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="menu2_plan_1.php" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">วางแผนการผลิต</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">แผนการผลิตปัจจุบัน</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">แผนการผลิตโดยรวม</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">ประวัติวางแผนการผลิต</span>
			                            </a>
			                        </li>
			                    </ul>
			                </div>
			            </li>
			            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
			                <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                    <i class="m-menu__link-icon flaticon-interface-9"></i>
			                    <span class="m-menu__link-text">การผลิต</span>
			                    <i class="m-menu__ver-arrow la la-angle-right"></i>
			                </a>
			                <div class="m-menu__submenu ">
			                    <span class="m-menu__arrow"></span>
			                    <ul class="m-menu__subnav">
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">การผลิตปัจจุบัน</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item" aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                            	<i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                            		<span></span>
			                            	</i>
			                            	<span class="m-menu__link-text">การผลิตส่วนหน้า</span>
											<i class="m-menu__ver-arrow la la-angle-right"></i>
										</a>
			                            <div class="m-menu__submenu ">
			                            	<span class="m-menu__arrow"></span>
											<ul class="m-menu__subnav">
												<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
													<a href="#" class="m-menu__link ">
														<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
														<span class="m-menu__link-title"> 
															<span class="m-menu__link-wrap">
																<span class="m-menu__link-text">ลำดับงานการใช้ใย</span>
															</span>
														</span>
													</a>
												</li>
												<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
													<a href="#" class="m-menu__link ">
														<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
														<span class="m-menu__link-title"> 
															<span class="m-menu__link-wrap">
																<span class="m-menu__link-text">ลำดับงานการทอ</span>
															</span>
														</span>
													</a>
												</li>
												<li class="m-menu__item" aria-haspopup="true" m-menu-link-redirect="1">
													<a href="javascript:;" class="m-menu__link m-menu__toggle">
						                            	<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                            		<span></span>
						                            	</i>
						                            	<span class="m-menu__link-text">บันทึกข้อมูลผลผลิต</span>
														<i class="m-menu__ver-arrow la la-angle-right"></i>
													</a>
													<div class="m-menu__submenu ">
						                            	<span class="m-menu__arrow"></span>
														<ul class="m-menu__subnav">
															<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
																<a href="#" class="m-menu__link ">
																	<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									                                    <span></span>
									                                </i>
																	<span class="m-menu__link-title"> 
																		<span class="m-menu__link-wrap">
																			<span class="m-menu__link-text">เครื่องฉีดใย / ชักใย</span>
																		</span>
																	</span>
																</a>
															</li>
															<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
																<a href="menu3_product_2_3_2.php" class="m-menu__link ">
																	<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									                                    <span></span>
									                                </i>
																	<span class="m-menu__link-title"> 
																		<span class="m-menu__link-wrap">
																			<span class="m-menu__link-text">เครื่องทอ</span>
																		</span>
																	</span>
																</a>
															</li>
														</ul>
													</div>
												</li>
											</ul>
										</div>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">การผลิตส่วนหลัง</span>
			                            </a>
			                        </li>
			                    </ul>
			                </div>
			            </li>
			            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
			                <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                    <i class="m-menu__link-icon flaticon-calendar"></i>
			                    <span class="m-menu__link-text">การซ่อมบำรุง</span>
			                    <i class="m-menu__ver-arrow la la-angle-right"></i>
			                </a>
			                <div class="m-menu__submenu ">
			                    <span class="m-menu__arrow"></span>
			                    <ul class="m-menu__subnav">
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">การซ่อมบำรุงปัจจุบัน</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">แผนงานซ่อมบำรุง</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">ข้อมูลการจอดเครื่องจาก PLC</span>
			                            </a>
			                        </li>
			                    </ul>
			                </div>
			            </li>
			            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
			                <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                    <i class="m-menu__link-icon flaticon-settings-1"></i>
			                    <span class="m-menu__link-text">การตั้งค่า</span>
			                    <i class="m-menu__ver-arrow la la-angle-right"></i>
			                </a>
			                <div class="m-menu__submenu ">
			                    <span class="m-menu__arrow"></span>
			                    <ul class="m-menu__subnav">
			                        <li class="m-menu__item" aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">ข้อมูลหลัก</span>
			                                <i class="m-menu__ver-arrow la la-angle-right"></i>
			                            </a>
			                            <div class="m-menu__submenu ">
						                    <span class="m-menu__arrow"></span>
						                    <ul class="m-menu__subnav">
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">สาขา</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item" aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="menu4_setting_1_2.php" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">โรงทอ</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">สินค้า</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">ลูกค้า</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">เครื่องจักร</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">กระบวนการผลิต</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">ซ่อมบำรุง</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">ข้อมูลการทำงาน</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">ข้อมูลอ้างอิง</span>
						                            </a>
						                        </li>
						                    </ul>
						                </div>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">สูตรคำนวณและเงื่อนไข</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">ตั้งค่าระบบ</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">ภาษา</span>
			                            </a>
			                        </li>
			                    </ul>
			                </div>
			            </li>
			        </ul>
			    </div>
			    <!-- END: Aside Menu -->
			</div>
            <!-- END: Left Aside -->
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- <div class="m-subheader-search">
						<h2 class="m-subheader-search__title">
							รายการออเดอร์
							<span class="m-subheader-search__desc">ข้อความอธิบาย...</span>
						</h2>
					</div> -->
                <div class="m-content">
                    <!--Begin::Section-->
                    <!-- <div class="row">
                        <div class="col-xl-12">
		                    <div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								</button>
								เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256) 
								<a href="#" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air btn_alert_small">
									<i class="fa fa-sign-in-alt"></i>
								</a>
							</div>
						</div>
					</div> -->
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="m-portlet at-m-portlet-content m-portlet--mobile ">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
												จัดการผู้ใช้งานระบบ &nbsp;
												<!-- <span class="at_head_text">ประจำวันที่ 30/11/2018</span> -->
											</h3>

                                        </div>
                                    </div>
                                    <div class="m-portlet__head-tools">
                                    	
                                        <ul class="m-portlet__nav">
											<li class="m-portlet__nav-item">
												<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" id="m_accordion_3">
													<a class="collapsed m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="false">
														<i class="la la-search-plus m--font-brand"></i>
													</a>
												</div>
											</li>
                                            <li class="m-portlet__nav-item">
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
                                                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="พิมพ์">
														<i class="la la-print m--font-brand"></i>
													</a>
                                                </div>
                                            </li>
                                            <li class="m-portlet__nav-item">
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
                                                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ส่งออกข้อมูล">
														<i class="la la-external-link-square m--font-brand"></i>
													</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="m-portlet__body at-m-portlet-content-body">

									<div class="clearfix"></div>

									<div class="m-accordion__item-body collapse" id="m_accordion_3_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_3">
									<div class="m-accordion__item-content">
										<div class="m-portlet m-portlet--skin-dark m-portlet--rounded">
										<!-- <div class="m-portlet m-portlet--skin-dark m-portlet--bordered m-portlet--bordered-semi m-portlet--rounded"> -->
											<!-- <div class="m-portlet__head">
												<div class="m-portlet__head-caption">
													<div class="m-portlet__head-title">
														<h3 class="m-portlet__head-text">
															Semi Bordered Style
														</h3>
													</div>
												</div>
											</div> -->
											<div class="m-portlet__body">
												
												<!-- <div class="form-group m-form__group row">
													<div class="col-lg-4 form-group">
														<label>สาขา</label>
														<input class="form-control form-control-sm m-input" type="text" value="" id="">
													</div>
													<div class="col-lg-4 form-group">
														<label>โรงทอ</label>
														<input class="form-control form-control-sm m-input" type="text" value="" id="">
													</div>
													<div class="col-lg-4 form-group">
														<label>ค้นหา</label>
														<input class="form-control form-control-sm m-input" type="text" value="" id="">
													</div>
													
												</div> -->

												<!-- <h6 class="m--font-brand"><u>ค้นหาแบบละเอียด</u></h6>
												<div class="clearfix"></div> -->

												<div class="form-group m-form__group row">

													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 form-group">
																<label>รหัสพนักงาน</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
															<div class="col-lg-12 form-group">
																<label>สาขา</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 form-group">
																<label>แผนก</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
															<div class="col-lg-12 form-group">
																<label>สิทธิ์การใช้งาน</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 form-group">
																<label>ตำแหน่ง</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
															<div class="col-lg-12 form-group">
																<label>สถานะการใช้งาน</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 form-group">
																<label>ค้นหา</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
														</div>
													</div>
													
												</div>
												<div class="modal-footer m-stack__item m-stack__item--center" style="border-top: 1px solid #66677b;">
													<button type="button" class="btn btn-brand"><i class="fa fa-search"></i> ค้นหา</button>
													<button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> บันทึก</button>
													<button type="button" class="btn btn-metal-drak"><i class="fa fa-times"></i> ยกเลิก</button>
												</div>


											</div>
										</div>
									</div>
									</div>
									<!--begin: Datatable
									<!--begin: Datatable -->
									<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable at-vertical-align-middle" id="m_table_3">
	<thead>
		<tr>
			<th>รหัสพนักงาน</th>
			<th>ชื่อ/นามสกุล</th>
			<th>แผนก</th>
			<th>สิทธิ์</th>
			<th>สถานะการใช้งาน</th>
			<th>ปรับแก้ไข</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
		<tr>
			<td>MK001</td>
			<td>นางสาว ตะข่าย กว้างขวาง</td>
			<td>การตลาด</td>
			<td nowrap>
				<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-center" m-dropdown-toggle="click" aria-expanded="true">
					<a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">
						<i class="la la-ellipsis-v"></i>
					</a>
					<div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1">
						<span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content at-m-dropdown__content">
									<table class="table table-striped- table-bordered table-hover">
										<tbody>
											<tr>
												<td>ตำแหน่ง</td>
												<td class="m--font-brand">ผู้จัดการ</td>
											</tr>
											<tr>
												<td>สาขา</td>
												<td class="m--font-brand">CY เชียงยืน</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="m-badge m-badge--brand m-badge--wide">Administrator</span>
			</td>
			<td nowrap>
				<span class="m-badge m-badge--success m-badge--wide">ใช้งานอยู่</span>
			</td>
			<td nowrap>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-metal m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_1_1" title="">
					<i class="la la-pencil-square-o" style="margin-top: -1px;"></i>
				</a>
				<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ลบ">
					<i class="la la-trash-o"></i>
				</a>
			</td>
		</tr>
	</tbody>
</table>





























































									</div>
									<!--end: Datatable -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </div>
            </div>
        </div>
        <!-- end:: Body -->








<!--begin::Modal-->
<div class="modal fade" id="m_modal_1_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog at-modal-dialog-long-100" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					แก้ไขผู้ใช้งานระบบ 
					<!-- <span class="m--font-brand">DEQ0000003</span> -->
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body at-modal-body-and-footer">
					<div class="row">
				<div class="col-md-12">
						<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
									<div class="m-portlet__head">
										<div class="m-portlet__head-tools">
											<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active show" data-toggle="tab" href="#m_user_profile_tab_1" role="tab" aria-selected="true">
														<i class="flaticon-share m--hide"></i>
														ข้อมูลทั่วไป
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab" aria-selected="false">
														กำหนดสิทธิ์การใช้งาน
													</a>
												</li>
											</ul>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item m-portlet__nav-item--last">
													<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
														<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
															<i class="la la-gear"></i>
														</a>
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div class="tab-content">
										<div class="tab-pane active show" id="m_user_profile_tab_1">
										<div class="" style="padding:25px">
											แสดงรายละเอียดข้อมูลพนักงาน
										</div>
										</div>
										<div class="tab-pane" id="m_user_profile_tab_2">
										<div class="" style="padding:25px">
											แสดงรายละเอียดสิทธิ์การใช้งานของพนักงาน
										</div>
										
										</div>
									</div>
								</div>
						</div>
				</div>
			</div>
			<div class="modal-footer m-stack__item m-stack__item--center">
				<div class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-left" m-dropdown-toggle="click">
					<a href="#" class="m-dropdown__toggle btn btn-brand dropdown-toggle">
						<i class="fa fa-print"></i> พิมพ์
					</a>
					<div class="m-dropdown__wrapper" style="width: 175px;">
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content">
									<ul class="m-nav">
										<li class="m-nav__item">
											<a href="#" class="btn btn-outline-brand m-btn m-btn--pill m-btn--wide btn-sm">พ.ศ</a>
											<a href="#" class="btn btn-outline-warning m-btn m-btn--pill m-btn--wide btn-sm">ค.ศ</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
					</div>
				</div>
				<!-- <button type="button" class="btn btn-success"><i class="fa fa-save"></i> บันทึก</button> -->
				<button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-save"></i> ส่งออกข้อมูล</button>
				<button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ปิดหน้าต่าง</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="m_modal_1_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog at-modal-dialog-long-100" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					ข้อมูลใบสั่งผลิตสินค้า [ อวนต่างประเทศ ] 
					<span class="m--font-brand">*ยกเลิกใบสั่งทอเลขที่ 617C00348R1 (08/07/2561 13:35 น.)</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body at-modal-body-and-footer">
				<div>
					รายละเอียด
				</div>
			</div>
			<div class="modal-footer m-stack__item m-stack__item--center">
				<div class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-left" m-dropdown-toggle="click">
					<a href="#" class="m-dropdown__toggle btn btn-brand dropdown-toggle">
						<i class="fa fa-print"></i> พิมพ์
					</a>
					<div class="m-dropdown__wrapper" style="width: 175px;">
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content">
									<ul class="m-nav">
										<li class="m-nav__item">
											<a href="#" class="btn btn-outline-brand m-btn m-btn--pill m-btn--wide btn-sm">พ.ศ</a>
											<a href="#" class="btn btn-outline-warning m-btn m-btn--pill m-btn--wide btn-sm">ค.ศ</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
					</div>
				</div>
				<!-- <button type="button" class="btn btn-brand"><i class="fa fa-print"></i> พิมพ์</button> -->
				<button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-save"></i> ส่งออกข้อมูล</button>
				<button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ปิดหน้าต่าง</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->











<?php include('inc.footer.php');?>