<?php include('inc.header.php');?>
<!-- BEGIN: Header -->
<!-- BEGIN: Header -->
<!-- BEGIN: Header -->
<!-- BEGIN: Header -->
<!-- BEGIN: Header -->
<link rel="stylesheet" href="assets/production_machine/scheduler/codebase/dhtmlxscheduler.css">
<style>
    .dhtmlXTooltip.tooltip {
        -moz-box-shadow: 3px 3px 3px #888 !important;
        -webkit-box-shadow: 3px 3px 3px #888 !important;
        -o-box-shadow: 3px 3px 3px #888 !important;
        box-shadow: 3px 3px 3px #888 !important;
        filter: progid:DXImageTransform.Microsoft.Shadow(color='#888888', Direction=135, Strength=5) !important;
        background-color: #fff !important;
        cursor: default !important;
        padding: 10px !important;
        position: fixed !important;
        opacity: 1 !important;
    }

    .dhtmlXTooltip, .dhtmlx_message_area, .dhtmlx_modal_box, .dhx_cal_container, .dhx_cal_light {
        text-rendering: optimizeLegibility !important;
        -webkit-font-smoothing: antialiased !important;
        -moz-osx-font-smoothing: grayscale !important;
    }

    .dhtmlXTooltip.tooltip {
        border-left: 1px dotted #e0e0e0 !important;
        border-top: 1px dotted #e0e0e0 !important;
        /*font-family: Roboto,Arial !important;*/
        font-size: 12px !important;
        color: rgba(0,0,0,.75) !important;
        z-index: 1050 !important;
    }

    .dhtmlXTooltip.tooltip b {
        font-weight: 500 !important;
    }

    .dhtmlXTooltip.tooltip[role=tooltip] {
        font-size: 14px !important;
        box-shadow: 0 10px 20px 0 rgba(0,0,0,.2),0 1px 6px 0 rgba(0,0,0,.2) !important;
        border-style: solid !important;
        border-color: transparent !important;
    }

    .dhx_cal_data table .dhx_cal_event_line, .dhx_cal_event_line {
        /*box-sizing: border-box;*/
        text-overflow: ellipsis;
    }

    .dhx_cal_event_line{
        border: solid 1px #fff;
    }

    .dhx_data_table.folder .dhx_matrix_cell, .dhx_matrix_scell.folder {
	    background-color: #222;
	    color: #FFF;
	    cursor: pointer;
	}

	.dhx_cal_container {
	    font-family: 'Prompt', sans-serif;
	    font-size: 10pt;
	    position: relative;
	    overflow: hidden;
	}
	.dhx_cal_event .dhx_body, .dhx_cal_event_line {
	    font-size: 12px;
	    font-family: 'Prompt', sans-serif;
	}
	.dhx_scale_bar {
	    border-left: 1px solid #CECECE;
	    font: 11px/16px 'Prompt', sans-serif;
	    color: #767676;
	    padding-top: 2px;
	    background-color: #fff;
	}

</style>
<!-- BEGIN: Header -->
<!-- BEGIN: Header -->
<!-- BEGIN: Header -->
<!-- BEGIN: Header -->
<!-- BEGIN: Header -->
<!-- BEGIN: Header -->
<!-- BEGIN: Header -->
		<!-- BEGIN: Header -->
		<header id="m_header" class="m-grid__item m-header" m-minimize-offset="200" m-minimize-mobile-offset="200">
		    <div class="m-container m-container--fluid m-container--full-height">
		        <div class="m-stack m-stack--ver m-stack--desktop">
		            <!-- BEGIN: Brand -->
		            <div class="m-stack__item m-brand  m-brand--skin-light ">
		                <div class="m-stack m-stack--ver m-stack--general">
		                    <div class="m-stack__item m-stack__item--middle m-brand__logo">
		                        <a href="index.html" class="m-brand__logo-wrapper">
		                                <img alt="" src="assets/demo/media/img/logo/logo.png" class="pc" />
		                                <img alt="" src="assets/demo/media/img/logo/logo2_color.png" class="mobile" />
		                            </a>
		                        <h3 class="m-header__title">ระบบวางแผนการผลิต</h3>
		                    </div>
		                    <div class="m-stack__item m-stack__item--middle m-brand__tools">
		                        <!-- BEGIN: Responsive Aside Left Menu Toggler -->
		                        <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
		                                <span></span>
		                            </a>
		                        <!-- END -->
		                        <!-- BEGIN: Responsive Header Menu Toggler -->
		                        <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
		                                <span></span>
		                            </a>
		                        <!-- END -->
		                        <!-- BEGIN: Topbar Toggler -->
		                        <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
		                                <i class="flaticon-more"></i>
		                            </a>
		                        <!-- BEGIN: Topbar Toggler -->
		                    </div>
		                </div>
		            </div>
		            <!-- END: Brand -->
		            <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
		                <!-- <div class="m-header__title">
		                        <h3 class="m-header__title-text">รายการออเดอร์</h3>
		                    </div> -->
		                <!-- BEGIN: Horizontal Menu -->
		                <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
		                    <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
		                        <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
		                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link">
		                                <i class="m-menu__link-icon flaticon-line-graph"></i>
		                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน</span>
		                                <i class="m-menu__hor-arrow la la-angle-down"></i>
		                                <i class="m-menu__ver-arrow la la-angle-right"></i>
		                            </a>
		                                <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left"><span class="m-menu__arrow m-menu__arrow--adjust"></span>
		                                    <ul class="m-menu__subnav">
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-chart-area"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 1</span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-chart-bar"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 2</span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-chart-line"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 3</span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-chart-pie"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 5</span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-tasks"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 6</span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-tablets"></i>
		                                                <span class="m-menu__link-text">การวิเคราะห์และรายงาน 7</span>
		                                            </a>
		                                        </li>
		                                    </ul>
		                                </div>
		                            </li>
		                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true"><a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link">
		                                <i class="m-menu__link-icon flaticon-users"></i>
		                                <span class="m-menu__link-text">การตั้งค่าผู้ใช้งาน</span>
		                                <i class="m-menu__hor-arrow la la-angle-down"></i>
		                                <i class="m-menu__ver-arrow la la-angle-right"></i>
		                            </a>
		                                <div class="m-menu__submenu  m-menu__submenu--fixed m-menu__submenu--left" style="width:430px">
		                                    <span class="m-menu__arrow m-menu__arrow--adjust"></span>
		                                    <div class="m-menu__subnav">
		                                        <ul class="m-menu__content">
		                                            <li class="m-menu__item">
		                                                <h3 class="m-menu__heading m-menu__toggle">
		                                                    <i class="m-menu__link-icon fa fa-user-cog"></i>
		                                                    <span class="m-menu__link-text">ผู้ดูแลระบบ</span>
		                                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
		                                                </h3>
		                                                <ul class="m-menu__inner">
		                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                                        <a href="#" class="m-menu__link ">
		                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
		                                                        <span class="m-menu__link-text">
		                                                            กำหนดสิทธิ์ผู้ใช้งานระบบ
		                                                        </span>
		                                                        </a>
		                                                    </li>
		                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                                        <a href="menu5_user_2.php" class="m-menu__link ">
		                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
		                                                        <span class="m-menu__link-text">
		                                                            จัดการผู้ใช้งานระบบ
		                                                        </span>
		                                                        </a>
		                                                    </li>
		                                                </ul>
		                                            </li>
		                                            <li class="m-menu__item">
		                                                <h3 class="m-menu__heading m-menu__toggle">
		                                                    <i class="m-menu__link-icon fa fa-user-edit"></i>
		                                                    <span class="m-menu__link-text">ผู้ใช้งาน</span>
		                                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
		                                                </h3>
		                                                <ul class="m-menu__inner">
		                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                                        <a href="#" class="m-menu__link ">
		                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
		                                                        <span class="m-menu__link-text">
		                                                            ข้อมูลส่วนตัว
		                                                        </span>
		                                                        </a>
		                                                    </li>
		                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                                        <a href="#" class="m-menu__link ">
		                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
		                                                        <span class="m-menu__link-text">
		                                                            การแจ้งเตือน
		                                                        </span>
		                                                        </a>
		                                                    </li>
		                                                    <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                                        <a href="#" class="m-menu__link ">
		                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
		                                                        <span class="m-menu__link-text">
		                                                            ข้อความ
		                                                        </span>
		                                                        </a>
		                                                    </li>
		                                                </ul>
		                                            </li>
		                                        </ul>
		                                    </div>
		                                </div>
		                            </li>
		                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
		                                <a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link">
		                                    <i class="m-menu__link-icon flaticon-alert"></i>
		                                    <span class="m-menu__link-title"> 
		                                        <span class="m-menu__link-wrap"> 
		                                            <span class="m-menu__link-text">การแจ้งเตือน</span> 
		                                            <span class="m-menu__link-badge">
		                                                <span class="m-badge m-badge--danger m-badge--wide">ใหม่(12)</span>
		                                            </span>
		                                        </span>
		                                    </span>
		                                    <i class="m-menu__hor-arrow la la-angle-down"></i>
		                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
		                                </a>
		                                <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left" style="width:450px">
		                                    <span class="m-menu__arrow m-menu__arrow--adjust"></span>
		                                    <ul class="m-menu__subnav">
		                                        <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-bell text-danger"></i>
		                                                <span class="m-menu__link-text">
		                                                    เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256)
		                                                </span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-bell text-danger"></i>
		                                                <span class="m-menu__link-text">
		                                                    เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256)
		                                                </span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-bell text-danger"></i>
		                                                <span class="m-menu__link-text">
		                                                    เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256)
		                                                </span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-bell text-warning"></i>
		                                                <span class="m-menu__link-text">
		                                                    เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256)
		                                                </span>
		                                            </a>
		                                        </li>
		                                        <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
		                                            <a href="#" class="m-menu__link ">
		                                                <i class="m-menu__link-icon fa fa-bell text-warning"></i>
		                                                <span class="m-menu__link-text">
		                                                    เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256)
		                                                </span>
		                                            </a>
		                                        </li>
		                                    </ul>
		                                </div>
		                            </li>
		                        </ul>
		                    </div>
		                <!-- END: Horizontal Menu -->
		                <!-- BEGIN: Topbar -->
		                <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
		                    <div class="m-stack__item m-stack__item--middle m-dropdown m-dropdown--arrow m-dropdown--large m-dropdown--mobile-full-width m-dropdown--align-right m-dropdown--skin-light m-header-search m-header-search--expandable m-header-search--skin-light" id="m_quicksearch" m-quicksearch-mode="default">
		                        <!--BEGIN: Search Form -->
		                        <!-- <form class="m-header-search__form">
		                            <div class="m-header-search__wrapper">
		                                <span class="m-header-search__icon-search" id="m_quicksearch_search">
		                                        <i class="flaticon-search"></i>
		                                    </span>
		                                <span class="m-header-search__input-wrapper">
		                                        <input autocomplete="off" type="text" name="q" class="m-header-search__input" value="" placeholder="Search..." id="m_quicksearch_input">
		                                    </span>
		                                <span class="m-header-search__icon-close" id="m_quicksearch_close">
		                                        <i class="la la-remove"></i>
		                                    </span>
		                                <span class="m-header-search__icon-cancel" id="m_quicksearch_cancel">
		                                        <i class="la la-remove"></i>
		                                    </span>
		                            </div>
		                        </form> -->
		                        <!--END: Search Form -->
		                        <!--BEGIN: Search Results -->
		                        <div class="m-dropdown__wrapper">
		                            <div class="m-dropdown__arrow m-dropdown__arrow--center"></div>
		                            <div class="m-dropdown__inner">
		                                <div class="m-dropdown__body">
		                                    <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-height="300" data-mobile-height="200">
		                                        <div class="m-dropdown__content m-list-search m-list-search--skin-light">
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                        <!--BEGIN: END Results -->
		                    </div>
		                    <div class="m-stack__item m-topbar__nav-wrapper">
		                        <ul class="m-topbar__nav m-nav m-nav--inline">
		                            <li class="m-nav__item m-topbar__quick-actions m-dropdown m-dropdown--skin-light m-dropdown--large m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light">
		                                <a href="#" class="m-nav__link m-dropdown__toggle">
		                                    <span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
		                                    <span class="m-nav__link-icon">
		                                        <span class="m-nav__link-icon-wrapper">
		                                            <i class="fa fa-expand"></i>
		                                        </span>
		                                    </span>
		                                </a>
		                            </li>
		                            <li class="m-nav__item m-topbar__languages m-dropdown m-dropdown--small m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click">
		                                    <a href="#" class="m-nav__link m-dropdown__toggle">
		                                        <span class="m-nav__link-text">
		                                            <img class="m-topbar__language-selected-img" src="assets/app/media/img/flags/th.png">
		                                        </span>
		                                    </a>
		                                    <div class="m-dropdown__wrapper">
		                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
		                                        <div class="m-dropdown__inner">
		                                            <!-- <div class="m-dropdown__header m--align-center" style="background: url(assets/app/media/img/misc/quick_actions_bg.jpg); background-size: cover;">
		                                                <span class="m-dropdown__header-subtitle">Select your language</span>
		                                            </div> -->
		                                            <div class="m-dropdown__body">
		                                                <div class="m-dropdown__content">
		                                                    <ul class="m-nav m-nav--skin-light">
		                                                        <!-- <li class="m-nav__item">
		                                                            <a href="#" class="m-nav__link">
		                                                                <span class="m-nav__link-icon"><img class="m-topbar__language-img" src="assets/app/media/img/flags/th.png"></span>
		                                                                <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">ไทย</span>
		                                                            </a>
		                                                        </li> -->
		                                                        <li class="m-nav__item m-nav__item--active">
		                                                            <a href="#" class="m-nav__link m-nav__link--active">
		                                                                <span class="m-nav__link-icon"><img class="m-topbar__language-img" src="assets/app/media/img/flags/en.png"></span>
		                                                                <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">English</span>
		                                                            </a>
		                                                        </li>
		                                                        <li class="m-nav__item">
		                                                            <a href="#" class="m-nav__link">
		                                                                <span class="m-nav__link-icon"><img class="m-topbar__language-img" src="assets/app/media/img/flags/cn.png"></span>
		                                                                <span class="m-nav__link-title m-topbar__language-text m-nav__link-text">中国</span>
		                                                            </a>
		                                                        </li>
		                                                    </ul>
		                                                </div>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </li>
		                            <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
		                                <a href="#" class="m-nav__link m-dropdown__toggle">
		                                    <span class="m-topbar__userpic ">
		                                        <img src="assets/app/media/img/users/user1.jpg" class="m--img-rounded m--marginless m--img-centered" alt="" />
		                                    </span>
		                                    <span class="m-nav__link-icon m-topbar__usericon m--hide">
		                                        <span class="m-nav__link-icon-wrapper">
		                                            <i class="fa fa-user-alt"></i>
		                                        </span>
		                                    </span>
		                                    <span class="m-topbar__username m--hide">AAA</span>
		                                </a>
		                                <div class="m-dropdown__wrapper">
		                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
		                                    <div class="m-dropdown__inner">
		                                            <div class="m-dropdown__header m--align-center">
		                                                <div class="m-card-user m-card-user--skin-light">
		                                                    <div class="m-card-user__pic">
		                                                        <img src="assets/app/media/img/users/user1.jpg" class="m--img-rounded m--marginless" alt="" />
		                                                    </div>
		                                                    <div class="m-card-user__details">
		                                                        <span class="m-card-user__name m--font-weight-500">ชื่อ นามสกุล</span>
		                                                        <span class="">ตำแหน่งหรือสิทธิ์การใช้งาน</span>
		                                                    </div>
		                                                </div>
		                                            </div>
		                                            <!-- <div class="m-dropdown__body m--bg-brand">
		                                                <div class="m-dropdown__content">
		                                                    <ul class="m-nav m-nav--skin-light">
		                                                        <li class="m-nav__item">
		                                                            <a href="snippets/pages/user/login-1.html" class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">ออกจากระบบ</a>
		                                                        </li>
		                                                    </ul>
		                                                </div>
		                                            </div> -->
		                                        </div>
		                                    </div>
		                            </li>
		                            <li id="" class="m-nav__item">
		                                <a href="index.html" class="m-nav__link m-dropdown__toggle">
		                                    <span class="m-nav__link-icon m-nav__link-icon-alt">
		                                        <span class="m-nav__link-icon-wrapper">
		                                            <i class="fa fa-power-off"></i>
		                                        </span>
		                                    </span>
		                                </a>
		                            </li>
		                            <!-- <li id="m_quick_sidebar_toggle" class="m-nav__item">
		                                <a href="#" class="m-nav__link m-dropdown__toggle">
		                                    <span class="m-nav__link-icon m-nav__link-icon-alt">
		                                        <span class="m-nav__link-icon-wrapper">
		                                            <i class="flaticon-grid-menu"></i>
		                                        </span>
		                                    </span>
		                                </a>
		                            </li> -->
		                        </ul>
		                    </div>
		                </div>
		                <!-- END: Topbar -->
		            </div>
		        </div>
		    </div>
		</header>
		<!-- END: Header -->


        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
			    <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
			    <!-- BEGIN: Aside Menu -->
			    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" m-menu-scrollable="1" m-menu-dropdown-timeout="500">
			        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			            <!-- <li class="m-menu__section ">
			                <h4 class="m-menu__section-text">ระบบวางแผนการผลิต</h4>
			                <i class="m-menu__section-icon flaticon-more-v2"></i>
			            </li> -->
			            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                <a href="#" class="m-menu__link ">
			                        <i class="m-menu__link-icon flaticon-analytics"></i>
			                        <span class="m-menu__link-text">หน้าแรก</span>
			                    </a>
			            </li>
			            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
			                <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                        <i class="m-menu__link-icon flaticon-interface-8"></i>
			                        <span class="m-menu__link-title"> 
			                            <span class="m-menu__link-wrap"> 
			                                <span class="m-menu__link-text">การตลาด</span> 
			                                <!-- <span class="m-menu__link-badge">
			                                    <span class="m-badge m-badge--danger m-badge--wide">3</span>
			                                </span> --> 
			                            </span>
			                        </span>
			                        <i class="m-menu__ver-arrow la la-angle-right"></i>
			                    </a>
			                <div class="m-menu__submenu ">
			                    <span class="m-menu__arrow"></span>
			                    <ul class="m-menu__subnav">
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                        <span></span>
			                                    </i>
			                                    <span class="m-menu__link-text">รายการรออนุมัติ</span>
			                                </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                        <span></span>
			                                    </i>
			                                    <span class="m-menu__link-text">รายการสอบถาม</span>
			                                </a>
			                        </li>
			                        <li class="m-menu__item" aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="menu1_mk_3.php" class="m-menu__link ">
		                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
		                                        <span></span>
		                                    </i>
		                                    <span class="m-menu__link-text">รายการออเดอร์ </span>
		                                </a>
			                        </li>
			                    </ul>
			                </div>
			            </li>
			            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
			                <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                    <i class="m-menu__link-icon flaticon-calendar-3"></i>
			                    <span class="m-menu__link-text">การวางแผนการผลิต</span>
			                    <i class="m-menu__ver-arrow la la-angle-right"></i>
			                </a>
			                <div class="m-menu__submenu ">
			                    <span class="m-menu__arrow"></span>
			                    <ul class="m-menu__subnav">
			                        <li class="m-menu__item" aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="menu2_plan_1.php" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">วางแผนการผลิต</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">แผนการผลิตปัจจุบัน</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">แผนการผลิตโดยรวม</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">ประวัติวางแผนการผลิต</span>
			                            </a>
			                        </li>
			                    </ul>
			                </div>
			            </li>
			            <li class="m-menu__item  m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
			                <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                    <i class="m-menu__link-icon flaticon-interface-9"></i>
			                    <span class="m-menu__link-text">การผลิต</span>
			                    <i class="m-menu__ver-arrow la la-angle-right"></i>
			                </a>
			                <div class="m-menu__submenu ">
			                    <span class="m-menu__arrow"></span>
			                    <ul class="m-menu__subnav">
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">การผลิตปัจจุบัน</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item  m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                            	<i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                            		<span></span>
			                            	</i>
			                            	<span class="m-menu__link-text">การผลิตส่วนหน้า</span>
											<i class="m-menu__ver-arrow la la-angle-right"></i>
										</a>
			                            <div class="m-menu__submenu ">
			                            	<span class="m-menu__arrow"></span>
											<ul class="m-menu__subnav">
												<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
													<a href="#" class="m-menu__link ">
														<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
														<span class="m-menu__link-title"> 
															<span class="m-menu__link-wrap">
																<span class="m-menu__link-text">ลำดับงานการใช้ใย</span>
															</span>
														</span>
													</a>
												</li>
												<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
													<a href="#" class="m-menu__link ">
														<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
														<span class="m-menu__link-title"> 
															<span class="m-menu__link-wrap">
																<span class="m-menu__link-text">ลำดับงานการทอ</span>
															</span>
														</span>
													</a>
												</li>
												<li class="m-menu__item  m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-link-redirect="1">
													<a href="javascript:;" class="m-menu__link m-menu__toggle">
						                            	<i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                            		<span></span>
						                            	</i>
						                            	<span class="m-menu__link-text">บันทึกข้อมูลผลผลิต</span>
														<i class="m-menu__ver-arrow la la-angle-right"></i>
													</a>
													<div class="m-menu__submenu ">
						                            	<span class="m-menu__arrow"></span>
														<ul class="m-menu__subnav">
															<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
																<a href="#" class="m-menu__link ">
																	<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									                                    <span></span>
									                                </i>
																	<span class="m-menu__link-title"> 
																		<span class="m-menu__link-wrap">
																			<span class="m-menu__link-text">เครื่องฉีดใย / ชักใย</span>
																		</span>
																	</span>
																</a>
															</li>
															<li class="m-menu__item m-menu__item--active" aria-haspopup="true" m-menu-link-redirect="1">
																<a href="menu3_product_2_3_2.php" class="m-menu__link ">
																	<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									                                    <span></span>
									                                </i>
																	<span class="m-menu__link-title"> 
																		<span class="m-menu__link-wrap">
																			<span class="m-menu__link-text">เครื่องทอ</span>
																		</span>
																	</span>
																</a>
															</li>
														</ul>
													</div>
												</li>
											</ul>
										</div>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">การผลิตส่วนหลัง</span>
			                            </a>
			                        </li>
			                    </ul>
			                </div>
			            </li>
			            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
			                <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                    <i class="m-menu__link-icon flaticon-calendar"></i>
			                    <span class="m-menu__link-text">การซ่อมบำรุง</span>
			                    <i class="m-menu__ver-arrow la la-angle-right"></i>
			                </a>
			                <div class="m-menu__submenu ">
			                    <span class="m-menu__arrow"></span>
			                    <ul class="m-menu__subnav">
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">การซ่อมบำรุงปัจจุบัน</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">แผนงานซ่อมบำรุง</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">ข้อมูลการจอดเครื่องจาก PLC</span>
			                            </a>
			                        </li>
			                    </ul>
			                </div>
			            </li>
			            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
			                <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                    <i class="m-menu__link-icon flaticon-settings-1"></i>
			                    <span class="m-menu__link-text">การตั้งค่า</span>
			                    <i class="m-menu__ver-arrow la la-angle-right"></i>
			                </a>
			                <div class="m-menu__submenu ">
			                    <span class="m-menu__arrow"></span>
			                    <ul class="m-menu__subnav">
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">ข้อมูลหลัก</span>
			                                <i class="m-menu__ver-arrow la la-angle-right"></i>
			                            </a>
			                            <div class="m-menu__submenu ">
						                    <span class="m-menu__arrow"></span>
						                    <ul class="m-menu__subnav">
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">สาขา</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="menu4_setting_1_2.php" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">โรงทอ</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">สินค้า</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">ลูกค้า</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">เครื่องจักร</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">กระบวนการผลิต</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">ซ่อมบำรุง</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">ข้อมูลการทำงาน</span>
						                            </a>
						                        </li>
						                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
						                            <a href="#" class="m-menu__link ">
						                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
						                                    <span></span>
						                                </i>
						                                <span class="m-menu__link-text">ข้อมูลอ้างอิง</span>
						                            </a>
						                        </li>
						                    </ul>
						                </div>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">สูตรคำนวณและเงื่อนไข</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">ตั้งค่าระบบ</span>
			                            </a>
			                        </li>
			                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
			                            <a href="#" class="m-menu__link ">
			                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
			                                    <span></span>
			                                </i>
			                                <span class="m-menu__link-text">ภาษา</span>
			                            </a>
			                        </li>
			                    </ul>
			                </div>
			            </li>
			        </ul>
			    </div>
			    <!-- END: Aside Menu -->
			</div>
            <!-- END: Left Aside -->
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- <div class="m-subheader-search">
						<h2 class="m-subheader-search__title">
							รายการออเดอร์
							<span class="m-subheader-search__desc">ข้อความอธิบาย...</span>
						</h2>
					</div> -->
                <div class="m-content">
                    <!--Begin::Section-->
                    <!-- <div class="row">
                        <div class="col-xl-12">
		                    <div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								</button>
								เปลี่ยนแปลงข้อมูลหลัก (เริ่มใช้งาน 18:30 น. 25 ส.ค 256) 
								<a href="#" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air btn_alert_small">
									<i class="fa fa-sign-in-alt"></i>
								</a>
							</div>
						</div>
					</div> -->
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="m-portlet at-m-portlet-content m-portlet--mobile ">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
												เครื่องทอ &nbsp;
												<span class="at_head_text">ประจำวันที่ 30/11/2018</span>
											</h3>

                                        </div>
                                    </div>
                                    <div class="m-portlet__head-tools">
                                    	
                                        <ul class="m-portlet__nav">
											<li class="m-portlet__nav-item">
												<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" id="m_accordion_3">
													<a class="collapsed m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="false">
														<i class="la la-search-plus m--font-brand"></i>
													</a>
												</div>
											</li>
                                            <li class="m-portlet__nav-item">
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
                                                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="พิมพ์">
														<i class="la la-print m--font-brand"></i>
													</a>
                                                </div>
                                            </li>
                                            <li class="m-portlet__nav-item">
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push">
                                                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="ส่งออกข้อมูล">
														<i class="la la-external-link-square m--font-brand"></i>
													</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="m-portlet__body at-m-portlet-content-body">

									<div class="clearfix"></div>

									<div class="m-accordion__item-body collapse" id="m_accordion_3_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_3">
									<div class="m-accordion__item-content">
										<div class="m-portlet m-portlet--skin-dark m-portlet--rounded">
										<!-- <div class="m-portlet m-portlet--skin-dark m-portlet--bordered m-portlet--bordered-semi m-portlet--rounded"> -->
											<!-- <div class="m-portlet__head">
												<div class="m-portlet__head-caption">
													<div class="m-portlet__head-title">
														<h3 class="m-portlet__head-text">
															Semi Bordered Style
														</h3>
													</div>
												</div>
											</div> -->
											<div class="m-portlet__body">
												<div class="form-group m-form__group row">
													<!-- <div class="col-lg-4 form-group">
														<label>ตัวเลือกการค้นหา</label>
														<select class="form-control m-input" id="exampleSelect1">
															<option>ข้อมูลเวลาตามวันสอบถาม</option>
															<option>ข้อมูลเวลาตามวันส่งมอบ</option>
														</select>
													</div>
													<div class="col-lg-4 form-group">
														<label class="">ตามช่วงเวลา</label>
														<select class="form-control m-input" id="exampleSelect1">
															<option>สัปดาห์ที่ผ่านมาและสัปดาห์ปัจจุบัน</option>
															<option>สัปดาห์ปัจจุบัน</option>
															<option>เดือนที่ผ่านมาและเดือนปัจจุบัน</option>
															<option>เดือนปัจจุบัน</option>
															<option>กำหนดเอง</option>
														</select>
													</div> -->
													<div class="col-lg-4 form-group">
														<label>เลือกวันที่</label>
														<div class="input-daterange m_datepicker">
															<input type="text" class="form-control m-input" name="" placeholder="30/11/2018" data-col-index="5" />
														</div>
														<!-- <div class="input-daterange input-group m_datepicker">
															<input type="text" class="form-control m-input" name="start" placeholder="From" data-col-index="5" />
															<div class="input-group-append">
																<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
															</div>
															<input type="text" class="form-control m-input" name="end" placeholder="To" data-col-index="5" />
														</div> -->
													</div>
												</div>
												
												<div class="form-group m-form__group row">
													<div class="col-lg-4 form-group">
														<label>สาขา</label>
														<input class="form-control form-control-sm m-input" type="text" value="" id="">
													</div>
													<div class="col-lg-4 form-group">
														<label>สถานะ</label>
														<input class="form-control form-control-sm m-input" type="text" value="" id="">
													</div>
													<div class="col-lg-4 form-group">
														<label>ค้นหา</label>
														<input class="form-control form-control-sm m-input" type="text" value="" id="">
													</div>
													
												</div>
												<hr class="light">

												<h6 class="m--font-brand"><u>ค้นหาแบบละเอียด</u></h6>
												<div class="clearfix"></div>

												<div class="form-group m-form__group row">

													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 form-group">
																<label>ประเภทใย</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
															<div class="col-lg-12 form-group">
																<label>เงื่อน</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
															<div class="col-lg-12 form-group">
																<label>หู</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 form-group">
																<label>คุณภาพ</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
															<div class="col-lg-12 form-group">
																<label>อบ</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
															<div class="col-lg-12 form-group">
																<label>สี</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 form-group">
																<label>วัตถุดิบ</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
															<div class="col-lg-12 form-group">
																<label>ขนาดตา</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="row">
															<div class="col-lg-12 form-group">
																<label>เบอร์ใย</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
															<div class="col-lg-12 form-group">
																<label>จำนวนตา</label>
																<input class="form-control form-control-sm m-input" type="text" value="" id="">
															</div>
														</div>
													</div>
													
												</div>
												<div class="modal-footer m-stack__item m-stack__item--center" style="border-top: 1px solid #66677b;">
													<button type="button" class="btn btn-brand"><i class="fa fa-search"></i> ค้นหา</button>
													<button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> บันทึก</button>
													<button type="button" class="btn btn-metal-drak"><i class="fa fa-times"></i> ยกเลิก</button>
												</div>


											</div>
										</div>
									</div>
									</div>
<div>
	<a href="#" class="btn btn-metal-drak btn-sm m-btn m-btn m-btn--icon m-btn--pill">
		<span>
			<i class="fa fa-calendar-check"></i>
			<span>ปรับปรุงผลการผลิต</span>
		</span>
	</a>
</div>
									
<div class="at_product_top_icon">
	<div class="at_product_top_icon_box">
		<span class="m-badge m-badge--brand m-badge--wide"></span> แผนการผลิต
	</div>
	<div class="at_product_top_icon_box">
		<span class="m-badge m-badge--success m-badge--wide"></span> ผลผลิตจริง
	</div>
	<div class="at_product_top_icon_box">
		<span class="m-badge m-badge--secondary m-badge--wide"></span> ไม่มีแผนการผลิต / ไม่มีการผลิต
	</div>
	<div class="at_product_top_icon_box">
		<span class="m-badge m-badge--warning m-badge--wide"></span> จอดเครื่องซ่อมบำรุง
	</div>
	<div class="at_product_top_icon_box">
		<span class="m-badge m-badge--focus m-badge--wide"></span> ปรับเปลี่ยนงาน / ตั้งค่าเครื่องจักร
	</div>
</div>
									<!--begin: Datatable -->
<div class="">
	<div id="scheduler" class="dhx_cal_container fix-margin" style='width:100%; min-height:500px;float:left;background-color: transparent;margin-top: -40px;'>
        <div class="dhx_cal_navline hide" style="width:100%;">
            <!-- <div class="dhx_cal_prev_button">&nbsp;</div>
            <div class="dhx_cal_next_button">&nbsp;</div>
            <div class="dhx_cal_today_button" name="today_tab"></div> -->
            <div class="dhx_cal_date" style="display: none;"></div>
            <!-- <div class="dhx_cal_tab" name="timeline_7days_tab" style="left: 0px;"></div>
            <div class="dhx_cal_tab" name="timeline_15days_tab" style="left: 70px;"></div>
            <div class="dhx_cal_tab" name="timeline_30days_tab" style="left: 140px;"></div> -->
        </div>
        <div class="dhx_cal_header">
        </div>
        <div class="dhx_cal_data">
        </div>
    </div>
</div>





























































									</div>
									<!--end: Datatable -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End::Section-->
                </div>
            </div>
        </div>
        <!-- end:: Body -->








<!--begin::Modal-->
<div class="modal fade" id="m_modal_1_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog at-modal-dialog-long-100" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					เลขที่สอบถาม 
					<span class="m--font-brand">DEQ0000003</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body at-modal-body-and-footer">
				<div>
					รายละเอียด
				</div>
			</div>
			<div class="modal-footer m-stack__item m-stack__item--center">
				<div class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-left" m-dropdown-toggle="click">
					<a href="#" class="m-dropdown__toggle btn btn-brand dropdown-toggle">
						<i class="fa fa-print"></i> พิมพ์
					</a>
					<div class="m-dropdown__wrapper" style="width: 175px;">
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content">
									<ul class="m-nav">
										<li class="m-nav__item">
											<a href="#" class="btn btn-outline-brand m-btn m-btn--pill m-btn--wide btn-sm">พ.ศ</a>
											<a href="#" class="btn btn-outline-warning m-btn m-btn--pill m-btn--wide btn-sm">ค.ศ</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
					</div>
				</div>
				<!-- <button type="button" class="btn btn-success"><i class="fa fa-save"></i> บันทึกแผน</button> -->
				<button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-save"></i> ส่งออกข้อมูล</button>
				<button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ปิดหน้าต่าง</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="m_modal_1_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog at-modal-dialog-long-100" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					ข้อมูลใบสั่งผลิตสินค้า [ อวนต่างประเทศ ] 
					<span class="m--font-brand">*ยกเลิกใบสั่งทอเลขที่ 617C00348R1 (08/07/2561 13:35 น.)</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body at-modal-body-and-footer">
				<div>
					รายละเอียด
				</div>
			</div>
			<div class="modal-footer m-stack__item m-stack__item--center">
				<div class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-left" m-dropdown-toggle="click">
					<a href="#" class="m-dropdown__toggle btn btn-brand dropdown-toggle">
						<i class="fa fa-print"></i> พิมพ์
					</a>
					<div class="m-dropdown__wrapper" style="width: 175px;">
						<div class="m-dropdown__inner">
							<div class="m-dropdown__body">
								<div class="m-dropdown__content">
									<ul class="m-nav">
										<li class="m-nav__item">
											<a href="#" class="btn btn-outline-brand m-btn m-btn--pill m-btn--wide btn-sm">พ.ศ</a>
											<a href="#" class="btn btn-outline-warning m-btn m-btn--pill m-btn--wide btn-sm">ค.ศ</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
					</div>
				</div>
				<!-- <button type="button" class="btn btn-brand"><i class="fa fa-print"></i> พิมพ์</button> -->
				<button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-save"></i> ส่งออกข้อมูล</button>
				<button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ปิดหน้าต่าง</button>
			</div>
		</div>
	</div>
</div>

<!--end::Modal-->











<?php include('inc.footer.php');?>







<!-- <script src="assets/production_machine/js/jquery-3.3.1.min.js"></script>
<script src="assets/production_machine/js/jquery/ui/1.12.1/jquery-ui.min.js"></script> -->
<script src="assets/production_machine/scheduler/codebase/dhtmlxscheduler.js"></script>
<script src="assets/production_machine/scheduler/codebase/ext/dhtmlxscheduler_timeline.js"></script>
<script src='assets/production_machine/scheduler/codebase/dhtmlxscheduler-v5.js' type="text/javascript" charset="utf-8"></script>
<script src='assets/production_machine/scheduler/codebase/ext/dhtmlxscheduler_timeline.js' type="text/javascript" charset="utf-8"></script>
<script src='assets/production_machine/scheduler/codebase/ext/dhtmlxscheduler_daytimeline.js' type="text/javascript" charset="utf-8"></script>
<script src='assets/production_machine/scheduler/codebase/ext/dhtmlxscheduler_treetimeline.js' type="text/javascript" charset="utf-8"></script>


<script src='assets/production_machine/js/dhtmlxcommon.js' type="text/javascript" charset="utf-8"></script>
<script src='assets/production_machine/js/dhtmlxmenu.js' type="text/javascript" charset="utf-8"></script>

<script>

    Date.prototype.addDays = function (days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }

    var d = new Date();
    var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + (d.getFullYear() + 543);

    const schedulerDivId = 'scheduler';
    const tableDivId = 'planing-table';
    const defaultDuration = 4;
    var typeAuto = 'auto';
    var typeCancelAuto = 'cancelAuto';
    var typePlanning = 'planing';

    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth();
    var year = d.getFullYear();



    jQuery(document).ready(function ($) {

        scheduler.locale.labels.timeline_tab = "Timeline";
        scheduler.locale.labels.section_custom = "Section";
        scheduler.config.details_on_create = true;
        scheduler.config.details_on_dblclick = true;
        scheduler.config.xml_date = "%Y-%m-%d %H:%i";
        scheduler.config.drag_resize= false;

        // dhtmlXTooltip.config.className = 'dhtmlXTooltip tooltip';
        // dhtmlXTooltip.config.timeout_to_display = 50;
        // dhtmlXTooltip.config.delta_x = 15;
        // dhtmlXTooltip.config.delta_y = -20;

        // scheduler.config.default_date = "%j %M";

        //===============
        //Configuration
        //===============
        var sections = [{
            key: 1,
            label: "MC 10"
        }, {
            key: 2,
            label: "MC 20"
        }, {
            key: 3,
            label: "MC 30"
        }, {
            key: 4,
            label: "MC 40"
        }, ];

        var elements = [ // original hierarhical array to display
            {key:'b1', label:"เครื่อง B02", open: true, children: [
                    {key: 1, label: "แผนการผลิต" },
                    {key: 2, label: "ผลผลิตจริง"},
                ]},
            {key:'b2', label:"เครื่อง B03", open: true, children: [
                    {key: 3, label: "แผนการผลิต" },
                    {key: 4, label: "ผลผลิตจริง"},
                ]},
            {key:'b3', label:"เครื่อง B04", open: true, children: [
                    {key: 5, label: "แผนการผลิต" },
                    {key: 6, label: "ผลผลิตจริง"},
                ]},
            {key:'b4', label:"เครื่อง B05", open: true, children: [
                    {key: 7, label: "แผนการผลิต" },
                    {key: 8, label: "ผลผลิตจริง"},
                ]},
            {key:'b5', label:"เครื่อง B06", open: true, children: [
                    {key: 9, label: "แผนการผลิต" },
                    {key: 10, label: "ผลผลิตจริง"},
                ]},
        ];

        var elements2 = [ // original hierarhical array to display
            {key:'b1', label:"เครื่อง A001", open: true, children: [
                    {key: 11, label: "แผนการผลิต" },
                    {key: 12, label: "ผลผลิตจริง"},
                ]},
            {key:'b2', label:"เครื่อง A002", open: true, children: [
                    {key: 13, label: "แผนการผลิต" },
                    {key: 14, label: "ผลผลิตจริง"},
                ]},
            {key:'b3', label:"เครื่อง A003", open: true, children: [
                    {key: 15, label: "แผนการผลิต" },
                    {key: 16, label: "ผลผลิตจริง"},
                ]},
            {key:'b4', label:"เครื่อง A004", open: true, children: [
                    {key: 17, label: "แผนการผลิต" },
                    {key: 18, label: "ผลผลิตจริง"},
                ]},
            {key:'b5', label:"เครื่อง A005", open: true, children: [
                    {key: 19, label: "แผนการผลิต" },
                    {key: 20, label: "ผลผลิตจริง"},
                ]},
        ];

        scheduler.createTimelineView({
            name: "timeline_7days",
            x_unit: "day",
            x_date: "%d",
            x_step: 1,
            x_size: 7,
            x_start: 0,
            x_length: 48,
            y_unit: elements,
            y_property: "section_id",
            render: "tree",
            folder_dy: 25,
            dy:23,
            section_autoheight: false
        });


        scheduler.createTimelineView({
            name: "timeline_15days",
            x_unit: "day",
            x_date: "%d",
            x_step: 1,
            x_size: 15,
            x_start: 0,
            x_length: 48,
            y_unit: elements,
            y_property: "section_id",
            render: "tree",
            folder_dy: 25,
            dy:23,
            section_autoheight: false
        });

        scheduler.createTimelineView({
            name:"timeline_15days",
            x_unit:"minute",//measuring unit of the X-Axis.
            x_date:"%H:%i", //date format of the X-Axis
            x_step:60,      //X-Axis step in 'x_unit's
            x_size:33,      //X-Axis length specified as the total number of 'x_step's
            x_start:16,     //X-Axis offset in 'x_unit's
            x_length:48,    //number of 'x_step's that will be scrolled at a time
            folder_dy: 25,
            dy:25,
            y_unit:         //sections of the view (titles of Y-Axis)
            elements,
            y_property:"section_id", //mapped data property
            render:"tree",             //view mode
            section_autoheight: false,
            second_scale:{
                x_unit: "day", // the measuring unit of the axis (by default, 'minute')
                x_date: "%F %d" //the date format of the axis ("July 01")
            }
        });

        scheduler.createTimelineView({
            name:"timeline_30days",
            x_unit:"minute",//measuring unit of the X-Axis.
            x_date:"%H:%i", //date format of the X-Axis
            x_step:60,      //X-Axis step in 'x_unit's
            x_size:33,      //X-Axis length specified as the total number of 'x_step's
            x_start:16,     //X-Axis offset in 'x_unit's
            x_length:48,    //number of 'x_step's that will be scrolled at a time
            folder_dy: 25,
            dy:25,
            y_unit:         //sections of the view (titles of Y-Axis)
            elements2,
            y_property:"section_id", //mapped data property
            render:"tree",             //view mode
            section_autoheight: false,
            second_scale:{
                x_unit: "day", // the measuring unit of the axis (by default, 'minute')
                x_date: "%F %d" //the date format of the axis ("July 01")
            }
        });

        // scheduler.createTimelineView({
        //     name: "timeline_7days",
        //     x_unit: "day",
        //     x_date: "%d",
        //     x_step: 1,
        //     x_size: 7,
        //     x_start: 0,
        //     x_length: 48,
        //     y_unit: elements,
        //     y_property: "section_id",
        //     render: "tree",
        //     folder_dy: 25,
        //     dy:23,
        //     section_autoheight: false
        // });


        // scheduler.createTimelineView({
        //     name: "timeline_15days",
        //     x_unit: "day",
        //     x_date: "%d",
        //     x_step: 1,
        //     x_size: 15,
        //     x_start: 0,
        //     x_length: 48,
        //     y_unit: elements,
        //     y_property: "section_id",
        //     render: "tree",
        //     folder_dy: 25,
        //     dy:23,
        //     section_autoheight: false
        // });

        // scheduler.createTimelineView({
        //     name: "timeline_30days",
        //     x_unit: "day",
        //     x_date: "%d",
        //     x_step: 1,
        //     x_size: 30,
        //     x_start: 0,
        //     x_length: 48,
        //     y_unit: elements,
        //     y_property: "section_id",
        //     render: "tree",
        //     folder_dy: 25,
        //     dy:23,
        //     section_autoheight: false
        // });

        //===============
        //Data loading
        //===============
        scheduler.config.lightbox.sections = [{
            name: "description",
            height: 30,
            map_to: "text",
            type: "textarea",
            focus: true
        },
            {
                name: "custom",
                height: 23,
                type: "select",
                options: sections,
                map_to: "section_id"
            },
            {
                name: "time",
                height: 72,
                type: "time",
                map_to: "auto"
            }
        ];
        scheduler.config.time_step = 1440;
        scheduler.config.readonly_form = true;
        scheduler.config.drag_create = false;
        scheduler.config.fix_tab_position = false;

        scheduler.locale.labels.timeline_7days_tab = "7 วัน"
        scheduler.locale.labels.timeline_15days_tab = "15 วัน"
        scheduler.locale.labels.timeline_30days_tab = "30 วัน";
        scheduler.locale.labels.today_tab = "วันนี้";

        scheduler.date.timeline_7days_start = function (date) {
            return scheduler.date.day_start(date); //
        }
        scheduler.date.timeline_15days_start = function (date) {
            return scheduler.date.day_start(date); //
        }
        scheduler.date.timeline_30days_start = function (date) {
            return scheduler.date.day_start(date); //
        }

        scheduler.date.add_timeline_7days = function (date, inc) {
            return scheduler.date.add(date, inc * 7, "day");
        }
        scheduler.date.add_timeline_15days = function (date, inc) {
            return scheduler.date.add(date, inc * 15, "day");
        }
        scheduler.date.add_timeline_30days = function (date, inc) {
            return scheduler.date.add(date, inc * 30, "day");
        }

        scheduler.date.subtract_timeline_7days = function (date, inc) {
            return scheduler.date.subtract(date, inc * -7, "day");
        }
        scheduler.date.subtract_timeline_15days = function (date, inc) {
            return scheduler.date.subtract(date, inc * -15, "day");
        }
        scheduler.date.subtract_timeline_30days = function (date, inc) {
            return scheduler.date.subtract(date, inc * -30, "day");
        }

        scheduler.templates.timeline_7days_date = scheduler.templates.timeline_7days_date;
        scheduler.templates.timeline_15days_date = scheduler.templates.timeline_15days_date;
        scheduler.templates.timeline_30days_date = scheduler.templates.timeline_30days_date;

        scheduler.templates.timeline_7days_scale_date = scheduler.templates.timeline_7days_scale_date;
        scheduler.templates.timeline_15days_scale_date = scheduler.templates.timeline_15days_scale_date;
        scheduler.templates.timeline_30days_scale_date = scheduler.templates.timeline_30days_scale_date;

        scheduler.attachEvent("onSchedulerReady", onSchedulerReady);
        scheduler.attachEvent("onDataRender", updateDroppable);
        scheduler.attachEvent("onEventChanged", onSchedulerEventChanged);
        scheduler.attachEvent("onBeforeEventDelete", onSchedulerBeforeEventDelete);

        // scheduler.init(schedulerDivId, new Date(), "timeline_7days");


        // create event
        scheduler.init(schedulerDivId, new Date(), "timeline_30days");


        for(var i=10; i<20; i++){

            if(i%2==0){
                scheduler.addEvent({
                    id: i+1,
                    start_date: day+'-'+(month+1)+'-'+year + ' 00:00',
                    end_date: day+'-'+(month+1)+'-'+year + ' 21:00',
                    text: '1119P/013010400 : ORDER001 [20/08/2561 - 25/08/2561 : เป้าหมาย  200  ผืน]',
                    section_id: i+1,
                    type: 'current',
                    color: '#007bff',
                    // row: $($('tbody tr')[0])
                });

                scheduler.addEvent({
                    id: i+1 + '-a',
                    start_date: day+'-'+(month+1)+'-'+year + ' 21:00',
                    end_date: d.addDays(1).getDate() +'-'+(d.addDays(1).getMonth()+1)+'-'+d.addDays(1).getFullYear() + ' 03:00',
                    text: '',
                    section_id: i+1,
                    type: 'current',
                    color: '#FF3B30',
                    // row: $($('tbody tr')[0])
                });

                scheduler.addEvent({
                    id: i+1 + '-b',
                    start_date: d.addDays(1).getDate() +'-'+(d.addDays(1).getMonth()+1)+'-'+d.addDays(1).getFullYear() + ' 03:00',
                    end_date: d.addDays(1).getDate() +'-'+(d.addDays(1).getMonth()+1)+'-'+d.addDays(1).getFullYear() + ' 09:00',
                    text: '1ฑ090/013000402 : ORDER002 [20/08/2561 - 25/08/2561 : เป้าหมาย  200  ผืน]',
                    section_id: i+1,
                    type: 'current',
                    color: '#007bff',
                    // row: $($('tbody tr')[0])
                });
            }else{
                scheduler.addEvent({
                    id: i+1,
                    start_date: day+'-'+(month+1)+'-'+year + ' 00:00',
                    end_date: day+'-'+(month+1)+'-'+year + ' 13:00',
                    text: 'ผลผลิตจริงสะสม 150 ผืน   [ 72.45% ] : เป็นไปตามแผน',
                    section_id: i+1,
                    type: 'current',
                    color: '#28ce44',
                    // row: $($('tbody tr')[0])
                });
            }

        }


        // scheduler.addEvent({
        //     id: 'efxTf5s3TH',
        //     start_date: '23-08-2018 00:00',
        //     end_date: '26-08-2018 00:00',
        //     text: 'efxTf5s3TH',
        //     section_id: 1,
        //     // row: $($('tbody tr')[0])
        // });

        // scheduler.addEvent({
        //     id: 'Ou3Fd2JdnX',
        //     start_date: '23-08-2018 00:00',
        //     end_date: '24-08-2018 00:00',
        //     text: 'Ou3Fd2JdnX',
        //     section_id: 2,
        //     // row: $($('tbody tr')[0])
        // });

        // scheduler.addEvent({
        //     id: 'j7vSki4DvE',
        //     start_date: '22-08-2018 00:00',
        //     end_date: '24-08-2018 00:00',
        //     text: 'j7vSki4DvE',
        //     section_id: 3,
        //     // row: $($('tbody tr')[0])
        // });

        // scheduler.addEvent({
        //     id: 'pu7ojUe2Rf',
        //     start_date: '24-08-2018 00:00',
        //     end_date: '26-08-2018 00:00',
        //     text: 'pu7ojUe2Rf',
        //     section_id: 3,
        //     // row: $($('tbody tr')[0])
        // });

        // scheduler.addEvent({
        //     id: '8gIjvEmCh2',
        //     start_date: '27-08-2018 00:00',
        //     end_date: '30-08-2018 00:00',
        //     text: '8gIjvEmCh2',
        //     section_id: 3,
        //     // row: $($('tbody tr')[0])
        // });


        scheduler.attachEvent("onConfirmedBeforeEventDelete", function(id, event){
            // your custom code
        });

        scheduler.attachEvent("onEventChanged", function (id, event) {
            // your custom code
        });

        // scheduler.attachEvent("onEventAdded", function (id, event) {
        //         // your custom code
        //         console.log(event);
        // });

        scheduler.templates.event_class = function (start, end, event) {
            if (event.type == 'current') return "current_event";
            return "planning_event";
        };

        scheduler.attachEvent("onBeforeDrag", function (id, mode, e){
            if(e.target.className.indexOf('current_event') != -1){
                return false;
            }else{
                return true;
            }
        });


        scheduler.templates.tooltip_text = function(start,end,event) {
            return "<b>Event:</b> "+event.text+"<b>";
        };



        console.log('ggg');

        // var menu = new dhtmlXMenuObject();
        // 		menu.setSkin("dhx_terrace");
        // 		menu.setIconsPath("./data/imgs/");
        // 		menu.renderAsContextMenu();
        // 		menu.loadStruct("./data/dhxmenu.xml?e=" + new Date().getTime());


        // scheduler.attachEvent("onContextMenu", function(event_id, native_event_object) {
        //     console.log('sssssd', event_id);
        //     if (event_id) {
        //         var posx = 0;
        //         var posy = 0;
        //         if (native_event_object.pageX || native_event_object.pageY) {
        //             posx = native_event_object.pageX;
        //             posy = native_event_object.pageY;
        //         } else if (native_event_object.clientX || native_event_object.clientY) {
        //             posx = native_event_object.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        //             posy = native_event_object.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        //         }
        //         //menu.showContextMenu(posx, posy);
        //         return false; // prevent default action and propagation
        //     }
        //     return true;
        // });
    });

    function padNum(number, digit = 2) {
        return ("0".repeat(digit) + number).substr(digit * -1);
    }

    function onSchedulerReady() {
        // const rows = Array.from(document.querySelectorAll(`#${tableDivId} tbody tr`));
        // if (!rows)
        //     return;

        const now = new Date();
        const date = `${padNum(now.getDate())}-${padNum(now.getMonth() + 1)}-${now.getFullYear()}`;

        // tbody tr
        jQuery(`#${tableDivId} tbody tr:not(.row-edit)`).each(function () {
            const r = jQuery(this);
            r.draggable({
                cancel: "a.ui-icon", // clicking an icon won't initiate dragging
                revert: "invalid", // when not dropped, the item will revert back to its initial position
                helper: "clone",
                cursor: "move"
            });
        });
    }

    function updateDroppable() {
        setTimeout(function () {
            jQuery(`#${schedulerDivId} .dhx_matrix_cell`).droppable({
                accept: '.ui-draggable',
                hoverClass: "cell-highlight",
                tolerance: "pointer",
                drop: function (event, ui) {
                    const obj = ui.draggable;
                    if (!obj)
                        return;


                    const duration = obj.data('duration') || defaultDuration;

                    const columnIndex = jQuery(this).index();
                    const sectionIndex = jQuery(this).closest('table').closest('tr').index();

                    const today = moment().startOf('day');
                    const startMoment = moment(today).add(columnIndex, 'day');
                    const endMoment = moment(startMoment).add(duration, 'day');
                    const start = startMoment.format('DD-MM-YYYY 00:00');
                    const end = endMoment.format('DD-MM-YYYY 00:00');
                    // const startShort = startMoment.format('DD-MMM-YYYY');
                    // const endShort = moment(endMoment).subtract(1, 'day').format('DD-MMM-YYYY');
                    const startShort = startMoment.format('DD/MM/YYYY');
                    const endShort = moment(endMoment).subtract(1, 'day').format('DD/MM/YYYY');


                    const row = jQuery(`#${tableDivId} tbody tr[data-name="${obj.data('name')}"]`);

                    var hhh = `${obj.data('name')}`;
                    console.log("row: ", row);

                    row.find('.column-status')
                        .removeClass('at_bg_table_orange')
                        .addClass('at_bg_table_green')
                        .text('(100%) วางแผน');

                    row.find('.column-start')
                        .text(startShort);

                    row.find('.column-end')
                        .text(endShort);


                    console.log("id: ", obj.data('name'));
                    console.log("start_date: ", start);
                    console.log("end_date: ", end);
                    console.log("text: ", obj.data('name'));
                    console.log("section_id: ", sectionIndex + 1);
                    console.log("row: ", row);

                    scheduler.addEvent({
                        id: obj.data('name'),
                        start_date: start,
                        end_date: end,
                        text: obj.data('name'),
                        section_id: sectionIndex + 1,
                        row: row
                    });

                    updateDroppable();

                    var tr = $('#' + tableDivId + ' tbody tr[data-name="'+obj.data('name')+'"]')[0];
                    console.log('tr',tr);
                    $(tr).find('button[name="plan-auto"]').addClass('hide');
                }
            });
        }, 100);
    }

    function onSchedulerEventChanged(id, ev) {
        if (ev.row) {
            const startDate = moment(ev.start_date);
            const endDate = moment(ev.end_date).subtract(1, 'day');
            const start = startDate.format('DD-MMM-YYYY');
            const end = endDate.format('DD-MMM-YYYY');

            ev.row.find('.column-start')
                .text(start);

            ev.row.find('.column-end')
                .text(end);
        }
    }

    function onSchedulerBeforeEventDelete(id, ev) {
        if (ev.row) {
            ev.row.find('.column-status')
                .removeClass('at_bg_table_green')
                .addClass('at_bg_table_orange')
                .text('(0%) วางแผน');

            ev.row.find('.column-start')
                .text('-');

            ev.row.find('.column-end')
                .text('-');
        }
        return true;
    }

    function getRandomInt(min, max){
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
</script>