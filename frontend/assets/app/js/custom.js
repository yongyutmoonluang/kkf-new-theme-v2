jQuery(document).ready(function() {
    $('.m_datepicker').datepicker({
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            }
    });

    $('#btn-switch').change(onSwitchChange);
});

function onSwitchChange(e) {
    var $parent = $(this).parents('.custom-switch');
    var $customs = $parent.find('.btn-switch-content');
    var target = '';
    for (var i = 0; i < $customs.length; i++) {
        target = $($customs[i]).data('target');
        $('#' + target).toggleClass('nut_hide');
    }
}