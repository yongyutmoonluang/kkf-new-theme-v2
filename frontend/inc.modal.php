<!--begin::Modal-->
<div class="modal fade" id="m_modal_1_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog at-modal-dialog-long-100" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    รายละเอียดสอบถาม 
                    <span class="m--font-brand">DEQ1807060</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer">
                <div>
                    <div class="m-portlet" style="border: 0px;">
                      <div class="m-portlet__head at_bg_black">
                        <div class="m-portlet__head-caption">
                          <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                              <i class="la la-gear"></i>
                            </span>
                            <h3 class="">
                              รายละเอียดสอบถาม
                            </h3>
                          </div>
                        </div>
                      </div>
                      <div class="">

                        <!--begin::Section-->
                        <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                            <tbody>
                              <!-- <tr class="at_bg_black">
                                <td colspan="9"><h4 style="margin-bottom: 0;">รายละเอียดสอบถาม</h4></td>
                            </tr> -->
                            <tr>
                                <td>สถานะ</td>
                                <td class="m--font-brand">รอทอ</td>
                                <td>เลขที่สอบถาม</td>
                                <td class="m--font-brand">DEQ1807060</td>
                                <td>เลขที่ออเดอร์</td>
                                <td class="m--font-brand">61/07348</td>
                                <td>จำนวนสินค้า</td>
                                <td class="m--font-brand">5 รายการ</td>
                            </tr>
                            <tr>
                                <td>ลูกค้า/กลุ่ม</td>
                                <td class="m--font-brand">ร้านโชคไพบูลย์</td>
                                <td>วันที่สอบถาม</td>
                                <td class="m--font-brand">5 พ.ย.</td>
                                <td>วันที่สั่งซื้อ</td>
                                <td class="m--font-brand">-</td>
                                <td>จำนวนสั่งซื้อ</td>
                                <td class="m--font-brand">1,000 ผืน (10,000 กก.)</td>
                            </tr>
                            <tr>
                                <td>ประเทศ</td>
                                <td class="m--font-brand">YEMEN</td>
                                <td></td>
                                <td class="m--font-brand"></td>
                                <td>วันที่โอนข้อมูลสั่งผลิต</td>
                                <td class="m--font-brand">-</td>
                                <td>เลขที่ใบสั่งผลิต</td>
                                <td class="m--font-brand">-</td>
                            </tr>
                            <tr>
                                <td>การขาย</td>
                                <td class="m--font-brand">ขายต่าง</td>
                                <td></td>
                                <td class="m--font-brand"></td>
                                <td></td>
                                <td class="m--font-brand"></td>
                                <td>วันสุดท้ายที่ตลาดยอมรับ</td>
                                <td class="m--font-brand">-</td>
                            </tr>
                            </tbody>
                        </table>
                        <!--end::Section-->
                      </div>
                    </div>
                    <div class="m-portlet" style="border: 0px;">
                      <!-- <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                          <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                              <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                              รายละเอียดสอบถาม
                            </h3>
                          </div>
                        </div>
                      </div> -->
                      <div class="table-responsive">

                        <!--begin::Section-->
                        <table class="table table-striped- table-bordered table-hover table-checkable at-vertical-align-top" style="white-space: nowrap;">
                              <thead>
                              <tr class="at_bg_black">
                                  <th>ลำดับ</th>
                                  <th>รหัสสินค้าตลาด/ผลิต</th>
                                  <th>ชื่อสินค้า</th>
                                  <th>สาขา</th>
                                  <th>จำนวนสั่งซื้อ</th>
                                  <th>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</th>
                                  <th>จำนวนชุดทอ : ผืนทอ</th>
                                  <th>แผนทอ เริ่มทอ - ทอครบ</th>
                                  <th>สินค้าเข้าคลัง เริ่ม - เสร็จ</th>
                                  <th>สถานะ</th>
                                  <!-- <th>สัญลักษณ์</th>
                                  <th>ตัวเลือก</th> -->
                              </tr>
                              </thead>
                              <tbody>
                              <tr>
                                  <td>1</td>
                                  <td>
                                      ตลาด: H01400*11260250<br>ผลิต : ( รหัส 15 หลัก )
                                  </td>
                                  <td nowrap>

                                      0.14mm 1.1/2"*25md*20yd DK YOKO NW M206
                                  </td>
                                  <td>B&amp;S1</td>
                                  <td>500 PC</td>
                                  <td>15/12/2018</td>
                                  <td nowrap>50 ชุด : 550 ผืน (+10%)</td>
                                  <td nowrap>15/11/2018 - 01/12/2018</td>
                                  <td nowrap>25/11/2018 - 20/12/2018</td>
                                  <td nowrap="">
                                      <!--                                      <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">-->
                                      <!--                                          <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">-->
                                      <!--                                              <i class="la la-ellipsis-v"></i>-->
                                      <!--                                          </a>-->
                                      <!--                                          <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">-->
                                      <!--                                              <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>-->
                                      <!--                                              <div class="m-dropdown__inner">-->
                                      <!--                                                  <div class="m-dropdown__body">-->
                                      <!--                                                      <div class="m-dropdown__content at-m-dropdown__content">-->
                                      <!--                                                          <table class="table table-striped- table-bordered table-hover">-->
                                      <!--                                                              <tbody>-->
                                      <!--                                                              <tr>-->
                                      <!--                                                                  <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>-->
                                      <!--                                                                  <td class="m--font-brand">30/12/2018</td>-->
                                      <!--                                                              </tr>-->
                                      <!--                                                              <tr>-->
                                      <!--                                                                  <td>จำนวนชุดทอ : ผืนทอ</td>-->
                                      <!--                                                                  <td class="m--font-brand">27 ชุด : 324 ผืน (+8%)</td>-->
                                      <!--                                                              </tr>-->
                                      <!--                                                              <tr>-->
                                      <!--                                                                  <td>แผนทอ เริ่มทอ - ทอครบ</td>-->
                                      <!--                                                                  <td class="m--font-brand">05/12/2018 - 15/12/2018</td>-->
                                      <!--                                                              </tr>-->
                                      <!--                                                              <tr>-->
                                      <!--                                                                  <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>-->
                                      <!--                                                                  <td class="m--font-brand">10/12/2018 - 25/12/2018</td>-->
                                      <!--                                                              </tr>-->
                                      <!--                                                              </tbody>-->
                                      <!--                                                          </table>-->
                                      <!--                                                      </div>-->
                                      <!--                                                  </div>-->
                                      <!--                                              </div>-->
                                      <!--                                          </div>-->
                                      <!--                                      </div>-->
                                      <span class="m-badge  m-badge--danger m-badge--wide">รอตลาดอนุมัติ</span>
                                  </td>
                              </tr>
                              <tr>
                                  <td rowspan="4">2</td>
                                  <td rowspan="4">ตลาด: H0A500H82010900<br>ผลิต : ( รหัส 15 หลัก )</td>
                                  <td rowspan="4" class="product-name-line-1-box">110/2 3.70cm*90md*12m SK YOKO BLACK N101</td>
                              </tr><tr>
                                  <td style="border-left: 1px solid #3c3d48;">BWC</td>
                                  <td>400 PC</td>
                                  <td>15/12/2018</td>
                                  <td nowrap>36 ชุด : 432 ผืน (+8%)</td>
                                  <td nowrap>15/11/2018 - 01/12/2018</td>
                                  <td nowrap>25/11/2018 - 20/12/2018</td>
                                  <td nowrap="">
                                      <!--                                      <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">-->
                                      <!--                                          <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">-->
                                      <!--                                              <i class="la la-ellipsis-v"></i>-->
                                      <!--                                          </a>-->
                                      <!--                                          <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">-->
                                      <!--                                              <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>-->
                                      <!--                                              <div class="m-dropdown__inner">-->
                                      <!--                                                  <div class="m-dropdown__body">-->
                                      <!--                                                      <div class="m-dropdown__content at-m-dropdown__content">-->
                                      <!--                                                          <table class="table table-striped- table-bordered table-hover">-->
                                      <!--                                                              <tbody>-->
                                      <!--                                                              <tr>-->
                                      <!--                                                                  <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>-->
                                      <!--                                                                  <td class="m--font-brand">30/12/2018</td>-->
                                      <!--                                                              </tr>-->
                                      <!--                                                              <tr>-->
                                      <!--                                                                  <td>จำนวนชุดทอ : ผืนทอ</td>-->
                                      <!--                                                                  <td class="m--font-brand">27 ชุด : 324 ผืน (+8%)</td>-->
                                      <!--                                                              </tr>-->
                                      <!--                                                              <tr>-->
                                      <!--                                                                  <td>แผนทอ เริ่มทอ - ทอครบ</td>-->
                                      <!--                                                                  <td class="m--font-brand">05/12/2018 - 15/12/2018</td>-->
                                      <!--                                                              </tr>-->
                                      <!--                                                              <tr>-->
                                      <!--                                                                  <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>-->
                                      <!--                                                                  <td class="m--font-brand">10/12/2018 - 25/12/2018</td>-->
                                      <!--                                                              </tr>-->
                                      <!--                                                              </tbody>-->
                                      <!--                                                          </table>-->
                                      <!--                                                      </div>-->
                                      <!--                                                  </div>-->
                                      <!--                                              </div>-->
                                      <!--                                          </div>-->
                                      <!--                                      </div>-->
                                      <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                  </td>
                              </tr>
                              <tr>
                                  <td style="border-left: 1px solid #3c3d48; border-top: 0; "></td>
                                  <td style="border-top: 0; ">300 PC</td>
                                  <td>30/12/2018</td>
                                  <td nowrap>27 ชุด : 324 ผืน (+8%)</td>
                                  <td nowrap>05/11/2018 - 15/12/2018</td>
                                  <td nowrap>10/11/2018 - 25/12/2018</td>
                                  <td nowrap="" style="border-top: 0; ">
<!--                                      <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">-->
<!--                                          <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">-->
<!--                                              <i class="la la-ellipsis-v"></i>-->
<!--                                          </a>-->
<!--                                          <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">-->
<!--                                              <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>-->
<!--                                              <div class="m-dropdown__inner">-->
<!--                                                  <div class="m-dropdown__body">-->
<!--                                                      <div class="m-dropdown__content at-m-dropdown__content">-->
<!--                                                          <table class="table table-striped- table-bordered table-hover">-->
<!--                                                              <tbody>-->
<!--                                                              <tr>-->
<!--                                                                  <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>-->
<!--                                                                  <td class="m--font-brand">30/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>จำนวนชุดทอ : ผืนทอ</td>-->
<!--                                                                  <td class="m--font-brand">27 ชุด : 324 ผืน (+8%)</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>แผนทอ เริ่มทอ - ทอครบ</td>-->
<!--                                                                  <td class="m--font-brand">05/12/2018 - 15/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>-->
<!--                                                                  <td class="m--font-brand">10/12/2018 - 25/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              </tbody>-->
<!--                                                          </table>-->
<!--                                                      </div>-->
<!--                                                  </div>-->
<!--                                              </div>-->
<!--                                          </div>-->
<!--                                      </div>-->
                                  </td>
                              </tr>
                              <tr>
                                  <td style="border-left: 1px solid #3c3d48; border-top: 0; "></td>
                                  <td style="border-top: 0; ">200 PC</td>
                                  <td>15/12/2018</td>
                                  <td nowrap>18 ชุด : 216 ผืน (+8%)</td>
                                  <td nowrap>25/11/2018 - 30/12/2018</td>
                                  <td nowrap>30/11/2018 - 05/12/2018</td>
                                  <td nowrap="" style="border-top: 0; ">
<!--                                      <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">-->
<!--                                          <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">-->
<!--                                              <i class="la la-ellipsis-v"></i>-->
<!--                                          </a>-->
<!--                                          <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">-->
<!--                                              <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>-->
<!--                                              <div class="m-dropdown__inner">-->
<!--                                                  <div class="m-dropdown__body">-->
<!--                                                      <div class="m-dropdown__content at-m-dropdown__content">-->
<!--                                                          <table class="table table-striped- table-bordered table-hover">-->
<!--                                                              <tbody>-->
<!--                                                              <tr>-->
<!--                                                                  <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>-->
<!--                                                                  <td class="m--font-brand">15/01/2019</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>จำนวนชุดทอ : ผืนทอ</td>-->
<!--                                                                  <td class="m--font-brand">18 ชุด : 216 ผืน (+8%)</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>แผนทอ เริ่มทอ - ทอครบ</td>-->
<!--                                                                  <td class="m--font-brand">25/12/2018 - 30/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>-->
<!--                                                                  <td class="m--font-brand">30/12/2018 - 05/01/2019</td>-->
<!--                                                              </tr>-->
<!--                                                              </tbody>-->
<!--                                                          </table>-->
<!--                                                      </div>-->
<!--                                                  </div>-->
<!--                                              </div>-->
<!--                                          </div>-->
<!--                                      </div>-->
                                  </td>
                              </tr>
                              
                              <tr>
                                  <td rowspan="4">3</td>
                                  <td rowspan="4">ตลาด: H0A500H82010900<br>ผลิต : ( รหัส 15 หลัก )</td>
                                  <td rowspan="4" class="product-name-line-1-box">110/2 3.70cm*90md*12m SK YOKO BLACK N101</td>
                              </tr><tr>
                                  <td style="border-left: 1px solid #3c3d48;">BWC</td>
                                  <td>400 PC</td>
                                  <td>15/12/2018</td>
                                  <td nowrap>36 ชุด : 432 ผืน (+8%)</td>
                                  <td nowrap>15/11/2018 - 25/11/2018</td>
                                  <td nowrap>20/11/2018 - 10/12/2018</td>
                                  <td nowrap="">
<!--                                      <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">-->
<!--                                          <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">-->
<!--                                              <i class="la la-ellipsis-v"></i>-->
<!--                                          </a>-->
<!--                                          <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">-->
<!--                                              <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>-->
<!--                                              <div class="m-dropdown__inner">-->
<!--                                                  <div class="m-dropdown__body">-->
<!--                                                      <div class="m-dropdown__content at-m-dropdown__content">-->
<!--                                                          <table class="table table-striped- table-bordered table-hover">-->
<!--                                                              <tbody>-->
<!--                                                              <tr>-->
<!--                                                                  <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>-->
<!--                                                                  <td class="m--font-brand">15/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>จำนวนชุดทอ : ผืนทอ</td>-->
<!--                                                                  <td class="m--font-brand">36 ชุด : 432 ผืน (+8%)</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>แผนทอ เริ่มทอ - ทอครบ</td>-->
<!--                                                                  <td class="m--font-brand">15/11/2018 - 25/11/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>-->
<!--                                                                  <td class="m--font-brand">20/11/2018 - 10/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              </tbody>-->
<!--                                                          </table>-->
<!--                                                      </div>-->
<!--                                                  </div>-->
<!--                                              </div>-->
<!--                                          </div>-->
<!--                                      </div>-->
                                      <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                  </td>
                              </tr>
                              <tr>
                                  <td style="border-left: 1px solid #3c3d48;  border-top: 0; "></td>
                                  <td style=" border-top: 0; ">300 PC</td>
                                  <td>30/12/2018</td>
                                  <td nowrap>27 ชุด : 324 ผืน (+8%)</td>
                                  <td nowrap>05/12/2018 - 15/12/2018</td>
                                  <td nowrap>10/12/2018 - 25/12/2018</td>
                                  <td nowrap="" style="border-top: 0; ">

                                  </td>
                              </tr>
                              <tr>
                                  <td style="border-left: 1px solid #3c3d48;  border-top: 0; "></td>
                                  <td style=" border-top: 0; ">200 PC</td>
                                  <td>15/01/2019</td>
                                  <td nowrap>18 ชุด : 216 ผืน (+8%)</td>
                                  <td nowrap>25/12/2018 - 30/12/2018</td>
                                  <td nowrap>30/12/2018 - 05/01/2019</td>
                                  <td nowrap="" style="border-top: 0; ">

                                  </td>
                              </tr>
                              <!-- tr 4-->
                              <tr>
                                  <td>4</td>
                                  <td>
                                      ตลาด: M1701/*600Qก320<br>ผลิต : M0706\15CZ1110P
                                  </td>
                                  <td nowrap="">

                                      0.70mm 6*1110md*6 5ml DK YOKO SW
                                  </td>
                                  <td>BWC</td>
                                  <td>50 PC</td>
                                  <td>15/01/2019</td>
                                  <td nowrap>8 ชุด : 56 ผืน (+12%)</td>
                                  <td nowrap>15/11/2018 - 01/12/2018</td>
                                  <td nowrap>25/11/2018 - 20/12/2018</td>
                                  <td nowrap="">
<!--                                      <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">-->
<!--                                          <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">-->
<!--                                              <i class="la la-ellipsis-v"></i>-->
<!--                                          </a>-->
<!--                                          <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">-->
<!--                                              <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>-->
<!--                                              <div class="m-dropdown__inner">-->
<!--                                                  <div class="m-dropdown__body">-->
<!--                                                      <div class="m-dropdown__content at-m-dropdown__content">-->
<!--                                                          <table class="table table-striped- table-bordered table-hover">-->
<!--                                                              <tbody>-->
<!--                                                              <tr>-->
<!--                                                                  <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>-->
<!--                                                                  <td class="m--font-brand">15/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>จำนวนชุดทอ : ผืนทอ</td>-->
<!--                                                                  <td class="m--font-brand">8 ชุด : 56 ผืน (+12%)</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>แผนทอ เริ่มทอ - ทอครบ</td>-->
<!--                                                                  <td class="m--font-brand">15/11/2018 - 01/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>-->
<!--                                                                  <td class="m--font-brand">25/11/2018 - 20/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              </tbody>-->
<!--                                                          </table>-->
<!--                                                      </div>-->
<!--                                                  </div>-->
<!--                                              </div>-->
<!--                                          </div>-->
<!--                                      </div>-->
                                      <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                  </td>
                              </tr>
                              <!-- tr 5-->
                              <tr>
                                  <td>5</td>
                                  <td>
                                      ตลาด: X02009095<br>ผลิต : ( รหัส 15 หลัก )
                                  </td>
                                  <td nowrap="">
                                      MONO LINE 0.23mm 250 g / spool GREEN M528
                                  </td>
                                  <td>KKF2</td>
                                  <td>90 PC</td>
                                  <td>15/01/2019</td>
                                  <td nowrap>100PC (+12%)</td>
                                  <td nowrap>15/11/2018 - 01/12/2018</td>
                                  <td nowrap>25/11/2018 - 20/12/2018</td>
                                  <td nowrap="">
<!--                                      <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">-->
<!--                                          <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">-->
<!--                                              <i class="la la-ellipsis-v"></i>-->
<!--                                          </a>-->
<!--                                          <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">-->
<!--                                              <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>-->
<!--                                              <div class="m-dropdown__inner">-->
<!--                                                  <div class="m-dropdown__body">-->
<!--                                                      <div class="m-dropdown__content at-m-dropdown__content">-->
<!--                                                          <table class="table table-striped- table-bordered table-hover">-->
<!--                                                              <tbody>-->
<!--                                                              <tr>-->
<!--                                                                  <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>-->
<!--                                                                  <td class="m--font-brand">15/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>จำนวนชุดทอ : ผืนทอ</td>-->
<!--                                                                  <td class="m--font-brand">100PC (+12%)</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>แผนทอ เริ่มทอ - ทอครบ</td>-->
<!--                                                                  <td class="m--font-brand">15/11/2018 - 01/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>-->
<!--                                                                  <td class="m--font-brand">25/11/2018 - 20/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              </tbody>-->
<!--                                                          </table>-->
<!--                                                      </div>-->
<!--                                                  </div>-->
<!--                                              </div>-->
<!--                                          </div>-->
<!--                                      </div>-->
<!--                                                                <span class="m-badge m-badge--focus m-badge--wide">กำลังทอ</span>-->
                                      <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                  </td>
                              </tr>
                              <!-- tr 6-->
                              <tr>
                                  <td>6</td>
                                  <td>
                                      ตลาด: X02009095<br>ผลิต : ( รหัส 15 หลัก )
                                  </td>
                                  <td nowrap="">
                                      MONO LINE 0.23mm 250 g / spool GREEN M528
                                  </td>
                                  <td>KKF2</td>
                                  <td>190 KG <span class="m--font-brand">(152 PC)</span></td>
                                  <td>15/01/2019</td>
                                  <td nowrap>160PC (+5.3%)</td>
                                  <td nowrap>15/11/2018 - 01/12/2018</td>
                                  <td nowrap>25/11/2018 - 20/12/2018</td>
                                  <td nowrap="">
<!--                                      <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">-->
<!--                                          <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">-->
<!--                                              <i class="la la-ellipsis-v"></i>-->
<!--                                          </a>-->
<!--                                          <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">-->
<!--                                              <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>-->
<!--                                              <div class="m-dropdown__inner">-->
<!--                                                  <div class="m-dropdown__body">-->
<!--                                                      <div class="m-dropdown__content at-m-dropdown__content">-->
<!--                                                          <table class="table table-striped- table-bordered table-hover">-->
<!--                                                              <tbody>-->
<!--                                                              <tr>-->
<!--                                                                  <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>-->
<!--                                                                  <td class="m--font-brand">15/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>จำนวนชุดทอ : ผืนทอ</td>-->
<!--                                                                  <td class="m--font-brand">160PC (+5.3%)</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>แผนทอ เริ่มทอ - ทอครบ</td>-->
<!--                                                                  <td class="m--font-brand">15/11/2018 - 01/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>-->
<!--                                                                  <td class="m--font-brand">25/11/2018 - 20/12/2018</td>-->
<!--                                                              </tr>-->
<!--                                                              </tbody>-->
<!--                                                          </table>-->
<!--                                                      </div>-->
<!--                                                  </div>-->
<!--                                              </div>-->
<!--                                          </div>-->
<!--                                      </div>-->
                                      <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                  </td>
                              </tr>
                              <!-- tr 7-->
                              <tr>
                                  <td>7</td>
                                  <td>
                                      <span>ตลาด: P16000/1205+140</span>
                                      <br>
                                      <span class="m--font-brand">[1st] : M1600//1205+140</span>
                                      <br>
                                      <span class="m--font-brand">ผลิต : ( รหัส 15 หลัก )</span>
                                      <br>
                                      <span class="m--font-brand">[2nd] : M190O/40005+028</span>
                                      <br>
                                      <span class="m--font-brand">ผลิต : ( รหัส 15 หลัก )</span>
                                  </td>
                                  <td nowrap="">
                                      <br>
                                      [1st] : 0.60mm  120mmsq*14.5md*500ml  DK  YOKO  DARK GREEN M502
                                      <br>
                                      [2nd] : 0.90mm  400mmsq*2.5md*100ml  DK  YOKO  SW M215
                                  </td>
                                  <td>
                                      KKF2
                                      <br>
                                      <span class="m--font-brand">B&amp;S1</span>
                                      <br>
                                      <span class="m--font-brand">NR</span>

                                  </td>
                                  <td>
                                      500 PC
                                      <br>
                                      <span class="m--font-brand">500 PC</span>
                                      <br>
                                      <span class="m--font-brand">1000 PC</span>
                                  </td>
                                  <td>[เร่งด่วน : 45 วัน]</td>
                                  <td nowrap></td>
                                  <td nowrap></td>
                                  <td nowrap></td>
                                  <td nowrap="">
<!--                                      <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">-->
<!--                                          <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">-->
<!--                                              <i class="la la-ellipsis-v"></i>-->
<!--                                          </a>-->
<!--                                          <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">-->
<!--                                              <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>-->
<!--                                              <div class="m-dropdown__inner">-->
<!--                                                  <div class="m-dropdown__body">-->
<!--                                                      <div class="m-dropdown__content at-m-dropdown__content">-->
<!--                                                          <table class="table table-striped- table-bordered table-hover">-->
<!--                                                              <tbody>-->
<!--                                                              <tr>-->
<!--                                                                  <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>-->
<!--                                                                  <td class="m--font-brand">[เร่งด่วน : 45 วัน]</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>จำนวนชุดทอ : ผืนทอ</td>-->
<!--                                                                  <td class="m--font-brand"></td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>แผนทอ เริ่มทอ - ทอครบ</td>-->
<!--                                                                  <td class="m--font-brand"></td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>-->
<!--                                                                  <td class="m--font-brand"></td>-->
<!--                                                              </tr>-->
<!--                                                              </tbody>-->
<!--                                                          </table>-->
<!--                                                      </div>-->
<!--                                                  </div>-->
<!--                                              </div>-->
<!--                                          </div>-->
<!--                                      </div>-->
                                      <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                  </td>
                              </tr>
                              <!-- tr 8-->
                              <tr>
                                  <td>8</td>
                                  <td>
                                      <span>ตลาด: P1129B4.5060503</span>
                                      <br>
                                      <span class="m--font-brand">[1st] : M1129//4.5060503</span>
                                      <br>
                                      <span class="m--font-brand">ผลิต : ( รหัส 15 หลัก )</span>
                                      <br>
                                      <span class="m--font-brand">[2nd] : N0040/*5125+121</span>
                                      <br>
                                      <span class="m--font-brand">ผลิต : ( รหัส 15 หลัก )</span>
                                  </td>
                                  <td nowrap="">
                                      <br>
                                      [1st] : 0.60mm  120mmsq*14.5md*500ml  DK  YOKO  DARK GREEN M502
                                      <br>
                                      [2nd] : 0.90mm  400mmsq*2.5md*100ml  DK  YOKO  SW M215
                                  </td>
                                  <td>
                                      KKF2
                                      <br>
                                      <span class="m--font-brand">B&amp;S1</span>
                                      <br>
                                      <span class="m--font-brand">NR</span>

                                  </td>
                                  <td>
                                      200 PC
                                      <br>
                                      <span class="m--font-brand">200 PC</span>
                                      <br>
                                      <span class="m--font-brand">200 PC</span>
                                  </td>
                                  <td>[เร่งด่วน : 60 วัน]</td>
                                  <td nowrap></td>
                                  <td nowrap></td>
                                  <td nowrap></td>
                                  <td nowrap="">
<!--                                      <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">-->
<!--                                          <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">-->
<!--                                              <i class="la la-ellipsis-v"></i>-->
<!--                                          </a>-->
<!--                                          <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">-->
<!--                                              <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>-->
<!--                                              <div class="m-dropdown__inner">-->
<!--                                                  <div class="m-dropdown__body">-->
<!--                                                      <div class="m-dropdown__content at-m-dropdown__content">-->
<!--                                                          <table class="table table-striped- table-bordered table-hover">-->
<!--                                                              <tbody>-->
<!--                                                              <tr>-->
<!--                                                                  <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>-->
<!--                                                                  <td class="m--font-brand">[เร่งด่วน : 60 วัน]</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>จำนวนชุดทอ : ผืนทอ</td>-->
<!--                                                                  <td class="m--font-brand"></td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>แผนทอ เริ่มทอ - ทอครบ</td>-->
<!--                                                                  <td class="m--font-brand"></td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>-->
<!--                                                                  <td class="m--font-brand"></td>-->
<!--                                                              </tr>-->
<!--                                                              </tbody>-->
<!--                                                          </table>-->
<!--                                                      </div>-->
<!--                                                  </div>-->
<!--                                              </div>-->
<!--                                          </div>-->
<!--                                      </div>-->
                                      <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                  </td>
                              </tr>
                              <!-- tr 9-->
                              <tr>
                                  <td>9</td>
                                  <td>
                                      <span>ตลาด : </span>
                                      <br>
                                      <span class="">ผลิต : </span>
                                  </td>
                                  <td nowrap="">
                                      <div>( สินค้าเตลา )</div>
                                  </td>
                                  <td>
                                      BWC
                                  </td>
                                  <td>
                                      50 PC
                                  </td>
                                  <td>8 ชุด : 56 ผืน (+12%)</td>
                                  <td nowrap></td>
                                  <td nowrap></td>
                                  <td nowrap></td>
                                  <td nowrap="">
<!--                                      <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right" m-dropdown-toggle="click" aria-expanded="true">-->
<!--                                          <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-outline-brand btn-xs m-btn m-btn--icon m-btn--pill">-->
<!--                                              <i class="la la-ellipsis-v"></i>-->
<!--                                          </a>-->
<!--                                          <div class="m-dropdown__wrapper at-m-dropdown__wrapper_box1 wa-width-auto">-->
<!--                                              <span class="m-dropdown__arrow m-dropdown__arrow--right"></span>-->
<!--                                              <div class="m-dropdown__inner">-->
<!--                                                  <div class="m-dropdown__body">-->
<!--                                                      <div class="m-dropdown__content at-m-dropdown__content">-->
<!--                                                          <table class="table table-striped- table-bordered table-hover">-->
<!--                                                              <tbody>-->
<!--                                                              <tr>-->
<!--                                                                  <td>วันที่ตลาดต้องการ<br>(วันที่ตลาดยอมรับได้)</td>-->
<!--                                                                  <td class="m--font-brand">8 ชุด : 56 ผืน (+12%)</td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>จำนวนชุดทอ : ผืนทอ</td>-->
<!--                                                                  <td class="m--font-brand"></td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>แผนทอ เริ่มทอ - ทอครบ</td>-->
<!--                                                                  <td class="m--font-brand"></td>-->
<!--                                                              </tr>-->
<!--                                                              <tr>-->
<!--                                                                  <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>-->
<!--                                                                  <td class="m--font-brand"></td>-->
<!--                                                              </tr>-->
<!--                                                              </tbody>-->
<!--                                                          </table>-->
<!--                                                      </div>-->
<!--                                                  </div>-->
<!--                                              </div>-->
<!--                                          </div>-->
<!--                                      </div>-->
                                      <span class="m-badge  m-badge--primary m-badge--wide">รอลูกค้า Confirm Order</span>
                                  </td>
                              </tr>
                              
                              </tbody>
                          </table>

                        <!--end::Section-->
                      </div>
                    </div>
                    <div class="m-portlet">
                      <div class="m-portlet__head at_bg_black">
                        <div class="m-portlet__head-caption">
                          <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                              <i class="la la-gear"></i>
                            </span>
                            <h4 class="">
                              คำอธิบายและความคิดเห็นเพิ่มเติม
                            </h4>
                          </div>
                        </div>
                      </div>
                      <div class="m-portlet__body">

                        <!--begin::Section-->
                        แสดงรายละเอียด

                        <!--end::Section-->
                      </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <div class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-left" m-dropdown-toggle="click">
                    <a href="#" class="m-dropdown__toggle btn btn-brand dropdown-toggle">
                        <i class="fa fa-print"></i> พิมพ์
                    </a>
                    <div class="m-dropdown__wrapper" style="width: 175px;">
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                        <li class="m-nav__item">
                                            <a href="#" class="btn btn-outline-brand m-btn m-btn--pill m-btn--wide btn-sm">พ.ศ</a>
                                            <a href="#" class="btn btn-outline-warning m-btn m-btn--pill m-btn--wide btn-sm">ค.ศ</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                    </div>
                </div>
                <!-- <button type="button" class="btn btn-brand"><i class="fa fa-print"></i> พิมพ์</button> -->
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-save"></i> ส่งออกข้อมูล</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ปิดหน้าต่าง</button>
            </div>
        </div>
    </div>
</div>

<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="m_modal_1_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog at-modal-dialog-long-100" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    ข้อมูลใบสั่งผลิตสินค้า [ อวนต่างประเทศ ] 
                    <span class="m--font-brand">*ยกเลิกใบสั่งทอเลขที่ 617C00348R1 (08/07/2561 13:35 น.)</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer">
                <div>

                  แสดงรายละเอียด

                </div>
            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <div class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-left" m-dropdown-toggle="click">
                    <a href="#" class="m-dropdown__toggle btn btn-brand dropdown-toggle">
                        <i class="fa fa-print"></i> พิมพ์
                    </a>
                    <div class="m-dropdown__wrapper" style="width: 175px;">
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                        <li class="m-nav__item">
                                            <a href="#" class="btn btn-outline-brand m-btn m-btn--pill m-btn--wide btn-sm">พ.ศ</a>
                                            <a href="#" class="btn btn-outline-warning m-btn m-btn--pill m-btn--wide btn-sm">ค.ศ</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                    </div>
                </div>
                <!-- <button type="button" class="btn btn-brand"><i class="fa fa-print"></i> พิมพ์</button> -->
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-save"></i> ส่งออกข้อมูล</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ปิดหน้าต่าง</button>
            </div>
        </div>
    </div>
</div>

<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="m_modal_1_bank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog at-modal-dialog-long-100" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    หัวข้อ 
                    <span class="m--font-brand">*ข้อความ</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer">
                <div>

                  แสดงรายละเอียด

                </div>
            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <div class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-left" m-dropdown-toggle="click">
                    <a href="#" class="m-dropdown__toggle btn btn-brand dropdown-toggle">
                        <i class="fa fa-print"></i> พิมพ์
                    </a>
                    <div class="m-dropdown__wrapper" style="width: 175px;">
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                        <li class="m-nav__item">
                                            <a href="#" class="btn btn-outline-brand m-btn m-btn--pill m-btn--wide btn-sm">พ.ศ</a>
                                            <a href="#" class="btn btn-outline-warning m-btn m-btn--pill m-btn--wide btn-sm">ค.ศ</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                    </div>
                </div>
                <!-- <button type="button" class="btn btn-brand"><i class="fa fa-print"></i> พิมพ์</button> -->
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-save"></i> ส่งออกข้อมูล</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ปิดหน้าต่าง</button>
            </div>
        </div>
    </div>
</div>

<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="m_modal_1_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog at-modal-dialog-long-100" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">


                    <div class="row">
                        <div class="col-lg-3">
                            เลขที่สอบถาม <span class="m--font-brand">DEQ0000003</span><br>
                            เลขที่สออเดอร์ <span class="m--font-brand">61/07348</span>
                        </div>
                        <div class="col-lg-3">
                            รหัสสินค้าตลาด <span class="m--font-brand">H01400*11260250</span><br>
                            รหัสผลิต <span class="m--font-brand">( รหัส 15 หลัก )</span>
                        </div>
                        <div class="col-lg-6">
                            ชื่อสินค้า <span class="m--font-brand">0.14mm 1.1/2"*25md*20yd DK YOKO NW M206</span><br>
                        </div>
                    </div>


                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer" style="height: calc(100vh - 235px);">
                <div>
                    <script src="https://code.highcharts.com/gantt/highcharts-gantt.js"></script>
                    <script src="https://code.highcharts.com/gantt/modules/exporting.js"></script>
                    <!-- <style type="text/css">
                        #container, #button-container {
                            width: 1700px;
                            margin: 1em auto;
                        }
                    </style> -->

                    <div id="container"></div>
                    <div id="button-container">

                    </div>
                </div>
            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <!-- <div class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--small m-dropdown--arrow m-dropdown--align-left" m-dropdown-toggle="click">
                    <a href="#" class="m-dropdown__toggle btn btn-brand dropdown-toggle">
                        <i class="fa fa-print"></i> พิมพ์
                    </a>
                    <div class="m-dropdown__wrapper" style="width: 175px;">
                        <div class="m-dropdown__inner">
                            <div class="m-dropdown__body">
                                <div class="m-dropdown__content">
                                    <ul class="m-nav">
                                        <li class="m-nav__item">
                                            <a href="#" class="btn btn-outline-brand m-btn m-btn--pill m-btn--wide btn-sm">พ.ศ</a>
                                            <a href="#" class="btn btn-outline-warning m-btn m-btn--pill m-btn--wide btn-sm">ค.ศ</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                    </div>
                </div> -->
                <!-- <button type="button" class="btn btn-brand"><i class="fa fa-print"></i> พิมพ์</button> -->
                <!-- <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-save"></i> ส่งออกข้อมูล</button> -->
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ปิดหน้าต่าง</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_Sp_Date" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="margin-left: 15px;">แบ่งรายการ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label class="col-lg-12">วันที่ส่งมอบปัจจุบันคือ 15 ธ.ค.</label>
                    <label class="col-lg-12">จำนวนสั่งซื้อคือ 500 KG (350 PC)</label>
                </div>
                <hr>
                <div class="form-group">
                    <label class="col-lg-12">แบ่งรายการแบบทยอยส่ง</label>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th class="wa_td_style">ครั้งที่</th>
                        <th class="wa_td_style">วันที่ตลาดต้องการ</th>
                        <th colspan="2" class="wa_td_style">จำนวน (PC)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>
                            <div class="input-group date">
                                <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
                                </div>
                                <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="01/01/2562">
                            </div>
                        </td>
                        <td>
                            <input class="form-control" type="text" value="250">
                        </td>
                        <td>
                            =357.14 KG
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>
                            <div class="input-group date">
                                <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
                                </div>
                                <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="15/01/2562">
                            </div>
                        </td>
                        <td>
                            <input class="form-control" type="text" value="100">
                        </td>
                        <td>
                            =142.86 KG
                        </td>
                    </tr>

                    </tbody>
                </table>

<!--                <div class="form-group">-->
<!--                    <label class="col-lg-12">แบ่งรายการแบบทยอยส่ง</label>-->
<!--                </div>-->
<!--                <div class="form-group wa_mgin_form">-->
<!--                    <div class="row">-->
<!--                        <div class="col-lg-3 col-sm-3">-->
<!--                            <label class="wa_la_form">ครั้งที่</label>-->
<!--                        </div>-->
<!--                        <div class="col-lg-3 col-sm-3">-->
<!--                            <label class="wa_la_form">วันที่ตลาดต้องการ</label>-->
<!--                        </div>-->
<!--                        <div class="col-lg-3 col-sm-3">-->
<!--                            <label class="wa_la_form">จำนวน (PC)</label>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="row">-->
<!--                        <div class="col-lg-3">-->
<!--                            <label class="wa_la_form2">1</label>-->
<!--                        </div>-->
<!--                        <div class="col-lg-3 wa_respon_mgin">-->
<!--                            <div class="input-group date">-->
<!--                                <div class="input-group-append">-->
<!--													<span class="input-group-text">-->
<!--														<i class="la la-calendar-check-o"></i>-->
<!--													</span>-->
<!--                                </div>-->
<!--                                <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="01/01/2562">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-3 wa_respon_mgin">-->
<!--                            <input class="form-control" type="text" value="250">-->
<!--                        </div>-->
<!--                        <div class="col-lg-3">-->
<!--                            <label class="wa_la_form2">=357.14 KG</label>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <br>-->
<!--                    <div class="row">-->
<!--                        <div class="col-lg-3">-->
<!--                            <label class="wa_la_form2">2</label>-->
<!--                        </div>-->
<!--                        <div class="col-lg-3 wa_respon_mgin">-->
<!--                            <div class="input-group date">-->
<!--                                <div class="input-group-append">-->
<!--													<span class="input-group-text">-->
<!--														<i class="la la-calendar-check-o"></i>-->
<!--													</span>-->
<!--                                </div>-->
<!--                                <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="15/01/2562">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-3 wa_respon_mgin">-->
<!--                            <input class="form-control" type="text" value="100">-->
<!--                        </div>-->
<!--                        <div class="col-lg-3">-->
<!--                            <label class="wa_la_form2">=142.86 KG</label>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="form-group">
                    <label class="col-lg-3">
                        <a href="#?" class="wa_hover_insert">+ เพิ่ม</a>
                    </label>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-brand" data-dismiss="modal"><i class="fab fa-firstdraft"></i> แบ่งรายการ</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ปิดหน้าต่าง</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->


<!--Modal by wa-->
<!--    start::Modal-->
<div class="modal fade" id="modal_setdayover" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">รายการอนุมัติ/ไม่อนุมัติ</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">วันที่ตลาดต้องการคือ 10 ธ.ค.</div>
                </div>
                <div class="row">
                    <div class="col-lg-12">ส่งเข้าคลังสินค้าครบตามแผนได้ <label>20 ธ.ค.</label></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <label class="m-radio">
                            <input type="radio" name="toasts" checked="checked" value="" class="wa_hide_text">อนุมัติรายการนี้
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <label class="m-radio">
                            <input type="radio" name="toasts" value="" class="wa_show_text">ไม่อนุมัติรายการ (ขอปรับแผน)
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 wa_show_text2">
                        <hr>
                        กำหนดวันส่งมอบช้าสุด
                        <div class="input-group date ">
                            <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
                            </div>
                            <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-brand" data-dismiss="modal">ดำเนินการต่อ</button>
                <button type="button" class="btn btn-metal" data-dismiss="modal">ยกเลิก</button>

            </div>
        </div>
    </div>
</div>
<!--    end::Modal-->

<!--    start::Modal-->
<div class="modal fade" id="modal_setflag" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">รายการอนุมัติ/ไม่อนุมัติ</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">Lead Time มาตรฐานของสินค้าดรณีเร่งด่วน กำหนดไว้ 45 วัน (20 ธ.ค.)</div>
                </div>
                <div class="row">
                    <div class="col-lg-12">แผนที่วางไว้ <label>55 วัน (30 ธ.ค.)</label></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <!--                            <label class="m-radio m-radio--check-bold">-->
                        <!--                                <input type="radio" name="toasts" checked="checked" >อนุมัติรายการนี้-->
                        <!--                                <span></span>-->
                        <!--                            </label>  type="radio" name="example_5_1" value="2" checked="checked"-->
                        <label class="m-radio m-radio--check-bold">
                            <input type="radio" name="example_5_1" value="2" checked="checked" class="wa_hide_text" >อนุมัติรายการนี้
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <!--                            <label class="m-radio m-radio--check-bold">-->
                        <!--                                <input type="radio" name="toasts" value="2" class="wa_show_text">ไม่อนุมัติรายการ (ขอปรับแผน)-->
                        <!--                                <span></span>-->
                        <!--                            </label>-->
                        <label class="m-radio m-radio--check-bold">
                            <input type="radio" name="example_5_1" value="3"  class="wa_show_text">ไม่อนุมัติรายการ (ขอปรับแผน)
                            <span></span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 wa_show_text2">
                        <hr>
                        <div class="row">
                            <div class="col-lg-5">
                                ตัวเลือก
                            </div>
                            <div class="col-lg-3">
                                <label class="m-radio">
                                    <input type="radio" name="toasts" checked="checked" class="wa_show_textbox">จำนวนวัน
                                    <span></span>
                                </label>
                            </div>
                            <div class="col-lg-3">
                                <label class="m-radio">
                                    <input type="radio" name="toasts" value="" class="wa_show_date">วันที่
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                กำหนดจำนวนวัน
                            </div>
                            <div class="col-lg-6 wa_show_t">
                                <input type="text" class="form-control">
                            </div>
                            <div class="col-lg-6 wa_show_d">
                                <div class="input-group date ">
                                    <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
                                    </div>
                                    <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-brand" data-dismiss="modal">ดำเนินการต่อ</button>
                <button type="button" class="btn btn-metal" data-dismiss="modal">ยกเลิก</button>

            </div>
        </div>
    </div>
</div>
<!--    end::Modal-->

<!--    end::Modal-->

<div class="modal fade" id="m_modal_tree_icon_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-md" role="document"><!-- at-modal-dialog-long-100 -->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    รายละเอียดสอบถาม
                    <span class="m--font-brand">DEQ1807060</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer" style="height: auto;">
                <div class="row">

                   <div class="col-lg-12">
                        <div class="m-portlet m-portlet--full-height wa_p_card">
                            <div class="row">
                                <div class="col-lg-12">วันที่ตลาดต้องการคือ <span class="m--font-brand">10 ธ.ค.</span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">ส่งเข้าคลังสินค้าครบตามแผนได้ <span class="m--font-danger">20 ธ.ค.</span></div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <!--                                    <label class="m-radio m-radio--check-bold">-->
                                    <!--                                        <input type="radio" name="toasts" checked="checked" value="" class="wa_hide_text">อนุมัติรายการนี้-->
                                    <!--                                        <span></span>-->
                                    <!--                                    </label>-->
                                    <label class="m-radio m-radio--check-bold">
                                        <input type="radio" name="example_8" value="1" checked="checked" class="wa_hide_text">อนุมัติรายการนี้
                                        <span></span>
                                    </label>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio m-radio--check-bold"">
                                    <input type="radio"name="example_8" value="2" class="wa_show_text">ไม่อนุมัติรายการ (ขอปรับแผน)
                                    <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 wa_show_text2">
                                    <hr>
                                    กำหนดวันส่งมอบช้าสุด
                                    <div class="input-group date ">
                                        <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-calendar-check-o"></i>
                                                    </span>
                                        </div>
                                        <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="">
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>


                </div>
            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <button type="button" class="btn btn-brand" data-dismiss="modal"><i class="fa fa-save"></i> อนุมัติรายการ</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ยกเลิก</button>
            </div>
        </div>
    </div>
</div>

<!--    end::Modal-->

<div class="modal fade" id="m_modal_tree_icon_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg" role="document"><!-- at-modal-dialog-long-100 -->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    รายละเอียดสอบถาม
                    <span class="m--font-brand">DEQ1807060</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer" style="height: auto;">
                <div class="row">

                    <div class="col-lg-6">
                        <div class="m-portlet m-portlet--full-height wa_p_card">
                            <div class="row">
                                <div class="col-lg-12">วันที่ตลาดต้องการคือ <span class="m--font-brand">10 ธ.ค.</span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">ส่งเข้าคลังสินค้าครบตามแผนได้ <span class="m--font-danger">20 ธ.ค.</span></div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <!--                                    <label class="m-radio m-radio--check-bold">-->
                                    <!--                                        <input type="radio" name="toasts" checked="checked" value="" class="wa_hide_text">อนุมัติรายการนี้-->
                                    <!--                                        <span></span>-->
                                    <!--                                    </label>-->
                                    <label class="m-radio m-radio--check-bold">
                                        <input type="radio" name="example_8" value="1" checked="checked" class="wa_hide_text">อนุมัติรายการนี้
                                        <span></span>
                                    </label>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio m-radio--check-bold"">
                                    <input type="radio"name="example_8" value="2" class="wa_show_text">ไม่อนุมัติรายการ (ขอปรับแผน)
                                    <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 wa_show_text2">
                                    <hr>
                                    กำหนดวันส่งมอบช้าสุด
                                    <div class="input-group date ">
                                        <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-calendar-check-o"></i>
                                                    </span>
                                        </div>
                                        <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="">
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                    <!--                        ---------------------------------->
                    <div class="col-lg-6">
                        <div class="m-portlet m-portlet--full-height wa_p_card">
                            <div class="row">
                                <div class="col-lg-12">Lead Time มาตรฐานของสินค้าดรณีเร่งด่วน กำหนดไว้ <span class="m--font-brand">45 วัน (20 ธ.ค.)</span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">แผนที่วางไว้ <span class="m--font-danger">55 วัน (30 ธ.ค.)</span></div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio">
                                        <input type="radio" name="example_9" value="3" checked="checked" class="wa_h_1" >อนุมัติรายการนี้
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio">
                                        <input type="radio" name="example_9" value="4"  class="wa_s_1">ไม่อนุมัติรายการ (ขอปรับแผน)
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 wa_s_detail">
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            ตัวเลือก
                                        </div>
                                        <div class="col-lg-3 wa_dayamount">
                                            <label class="m-radio">
                                                <input type="radio" name="example_10" checked="checked" class="">จำนวนวัน
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-lg-3 wa_day">
                                            <label class="m-radio">
                                                <input type="radio" name="example_10" value="" class="">วันที่
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            กำหนดจำนวนวัน
                                        </div>
                                        <div class="col-lg-6 wa_t_s_auto">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-lg-6 wa_sh">
                                            <div class="input-group date ">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-calendar-check-o"></i>
                                                    </span>
                                                </div>
                                                <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <button type="button" class="btn btn-brand" data-dismiss="modal"><i class="fa fa-save"></i> อนุมัติรายการ</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ยกเลิก</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="m_modal_tree_icon_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog at-modal-dialog-long-100" role="document"><!-- at-modal-dialog-long-100 -->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    รายละเอียดสอบถาม
                    <span class="m--font-brand">DEQ1807060</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer" style="height: auto;">
                <div class="row">

                    <div class="col-lg-4">
                        <div class="m-portlet m-portlet--full-height wa_p_card">
                            <div class="row">
                                <div class="col-lg-12">วันที่ตลาดต้องการคือ <span class="m--font-brand">10 ธ.ค.</span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">ส่งเข้าคลังสินค้าครบตามแผนได้ <span class="m--font-danger">20 ธ.ค.</span></div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <!--                                    <label class="m-radio m-radio--check-bold">-->
                                    <!--                                        <input type="radio" name="toasts" checked="checked" value="" class="wa_hide_text">อนุมัติรายการนี้-->
                                    <!--                                        <span></span>-->
                                    <!--                                    </label>-->
                                    <label class="m-radio m-radio--check-bold">
                                        <input type="radio" name="example_8" value="1" checked="checked" class="wa_hide_text">อนุมัติรายการนี้
                                        <span></span>
                                    </label>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio m-radio--check-bold"">
                                    <input type="radio"name="example_8" value="2" class="wa_show_text">ไม่อนุมัติรายการ (ขอปรับแผน)
                                    <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 wa_show_text2">
                                    <hr>
                                    กำหนดวันส่งมอบช้าสุด
                                    <div class="input-group date ">
                                        <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
                                        </div>
                                        <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="">
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                    <!--                        ---------------------------------->
                    <div class="col-lg-4">
                        <div class="m-portlet m-portlet--full-height wa_p_card">
                            <div class="row">
                                <div class="col-lg-12">Lead Time มาตรฐานของสินค้าดรณีเร่งด่วน กำหนดไว้ <span class="m--font-brand">45 วัน (20 ธ.ค.)</span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">แผนที่วางไว้ <span class="m--font-danger">55 วัน (30 ธ.ค.)</span></div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio">
                                        <input type="radio" name="example_9" value="3" checked="checked" class="wa_h_1" >อนุมัติรายการนี้
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio">
                                        <input type="radio" name="example_9" value="4"  class="wa_s_1">ไม่อนุมัติรายการ (ขอปรับแผน)
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 wa_s_detail">
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            ตัวเลือก
                                        </div>
                                        <div class="col-lg-3 wa_dayamount">
                                            <label class="m-radio">
                                                <input type="radio" name="example_10" checked="checked" class="">จำนวนวัน
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-lg-3 wa_day">
                                            <label class="m-radio">
                                                <input type="radio" name="example_10" value="" class="">วันที่
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            กำหนดจำนวนวัน
                                        </div>
                                        <div class="col-lg-6 wa_t_s_auto">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-lg-6 wa_sh">
                                            <div class="input-group date ">
                                                <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
                                                </div>
                                                <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--                        ------------------------------------------->
                    <!--                        --------->
                    <div class="col-lg-4">

                        <div class="m-portlet m-portlet--full-height wa_p_card">
                            <div class="row">
                                <div class="col-lg-12">จำนวนสั่งซื้อคือ <span class="m--font-brand">500 PC</span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">เปอร์เซ็นสูงสุดที่ลูกค้ายอมรับได้คือ <span class="m--font-brand">10% (550 PC)</span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">วางแผนมาแล้วได้ <span class="m--font-danger">10.4% (50 ชุดทอ:552 PC)</span></div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio">
                                        <input type="radio" name="example_11" value="5" checked="checked" class="wa_h_2">อนุมัติรายการนี้ (เก็บแผนการผลิตนี้ไว้ ฝ่ายการตลาดจะไปทำการปรับแก้ไขข้อมูลจำนวนสั่งซื้อ)
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio">
                                    <input type="radio"name="example_11" value="6" class="wa_s_2">ไม่อนุมัติรายการ (ขอปรับแผน)
                                    <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 wa_sh_2">
                                    <hr>
                                    กำหนดจำนวนชุดทอ:ผืนทอ
                                    <select class="form-control m-input" id="exampleSelect1">
                                        <option>45 ชุดทอ:540 ผืน</option>
                                        <option>44 ชุดทอ:528 ผืน</option>
                                        <option>43 ชุดทอ:516 ผืน</option>
                                        <option>42 ชุดทอ:504 ผืน</option>
                                        <option>41 ชุดทอ:492 ผืน</option>
                                        <option>40 ชุดทอ:480 ผืน</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <button type="button" class="btn btn-brand" data-dismiss="modal"><i class="fa fa-save"></i> อนุมัติรายการ</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ยกเลิก</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="m_modal_tree_icon_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-dialog-md" role="document"><!-- at-modal-dialog-long-100 -->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    รายละเอียดสอบถาม
                    <span class="m--font-brand">DEQ1807060</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer" style="height: auto;">

                        <div class="m-portlet m-portlet--full-height wa_p_card">
                            <div class="row">
                                <div class="col-lg-12">จำนวนสั่งซื้อคือ <span class="m--font-brand">500 PC</span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">เปอร์เซ็นสูงสุดที่ลูกค้ายอมรับได้คือ 10% <span class="m--font-brand">(550 PC)</span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">วางแผนมาแล้วได้ <span class="m--font-danger">10.4% (50 ชุดทอ:552 PC)</span></div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio">
                                        <input type="radio" name="example_1" value="5" checked="checked" class="wa_h_2">อนุมัติรายการนี้ (เก็บแผนการผลิตนี้ไว้ ฝ่ายการตลาดจะไปทำการปรับแก้ไขข้อมูลจำนวนสั่งซื้อ)
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio">
                                        <input type="radio"name="example_1" value="6" class="wa_s_2">ไม่อนุมัติรายการ (ขอปรับแผน)
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 wa_sh_2">
                                    <hr>
                                    กำหนดจำนวนชุดทอ:ผืนทอ
                                    <select class="form-control m-input" id="exampleSelect1">
                                        <option>45 ชุดทอ:540 ผืน</option>
                                        <option>44 ชุดทอ:528 ผืน</option>
                                        <option>43 ชุดทอ:516 ผืน</option>
                                        <option>42 ชุดทอ:504 ผืน</option>
                                        <option>41 ชุดทอ:492 ผืน</option>
                                        <option>40 ชุดทอ:480 ผืน</option>
                                    </select>
                                </div>
                            </div>
                        </div>

            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <button type="button" class="btn btn-brand" data-dismiss="modal"><i class="fa fa-save"></i> อนุมัติรายการ</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ยกเลิก</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="m_modal_tree_icon_5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-dialog-md" role="document"><!-- at-modal-dialog-long-100 -->
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    รายละเอียดสอบถาม
                    <span class="m--font-brand">DEQ1807060</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer" style="height: auto;">
                    <!--                        ---------------------------------->
                        <div class="m-portlet m-portlet--full-height wa_p_card">
                            <div class="row">
                                <div class="col-lg-12">Lead Time มาตรฐานของสินค้าดรณีเร่งด่วน กำหนดไว้ <span class="m--font-brand">45 วัน (20 ธ.ค.)</span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">แผนที่วางไว้ <span class="m--font-danger">55 วัน (30 ธ.ค.)</span></div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio">
                                        <input type="radio" name="example_3" value="3" checked="checked" class="wa_h_1" >อนุมัติรายการนี้
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="m-radio">
                                        <input type="radio" name="example_3" value="4"  class="wa_s_1">ไม่อนุมัติรายการ (ขอปรับแผน)
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 wa_s_detail">
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            ตัวเลือก
                                        </div>
                                        <div class="col-lg-4 wa_dayamount">
                                            <label class="m-radio">
                                                <input type="radio" name="toasts" checked="checked" class="">จำนวนวัน
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-lg-3 wa_day">
                                            <label class="m-radio">
                                                <input type="radio" name="toasts" value="" class="">วันที่
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-5">
                                            กำหนดจำนวนวัน
                                        </div>
                                        <div class="col-lg-6 wa_t_s_auto">
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-lg-6 wa_sh">
                                            <div class="input-group date ">
                                                <div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
                                                </div>
                                                <input id="setDateLast" class="form-control" type="text" data-provide="datepicker" data-date-format="dd/mm/yyyy" value="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <button type="button" class="btn btn-brand" data-dismiss="modal"><i class="fa fa-save"></i> อนุมัติรายการ</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ยกเลิก</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="m_modal_table" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog at-modal-dialog-long-100" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    รายละเอียดสอบถาม
                    <span class="m--font-brand">DEQ1807060</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer" style="height: auto;">
                <table class="table light table-striped- table-bordered table-hover"><!-- light -->
                    <tbody>
                    <!--                                            <tr>เอาไว้ตรงนี้ไม่เวิร์ค เดียวเอาไปใส่เป็นแถบกับกราฟดีกว่าครับ</tr>-->
                    <tr>
                        <td>รหัสสินค้าของตลาด<br>รหัสสินค้าของผลิต</td>
                        <td class="m--font-brand">1119P/013010400<br>-</td>
                        <td>ชื่อสินค้า<br>ชื่อสินค้าผลิต</td>
                        <td colspan="5" class="m--font-brand">ข่ายเอ็น 0.09 SK 1.3*40*180A. สีง SK00 YOKO เรือใบเงิน<br>-</td>
                        <!--                                                <td colspan="4"></td>-->
                        <!--                                                <td class="m--font-brand"></td>-->
                        <!--                                                <td></td>-->
                        <!--                                                <td class="m--font-brand"></td>-->
                    </tr>
                    <tr class="wa-no-border">
                        <td>ประเภทสินค้า</td>
                        <td class="m--font-brand">ข่ายเอ็น</td>
                        <td>กลุ่มวัตถุดิบ</td>
                        <td class="m--font-brand">MULTI MONO</td>
                        <td>วัตถุดิบ</td>
                        <td class="m--font-brand">MONO ตีเกลียว</td>
                        <td>กระบวนการผลิต</td>
                        <td class="m--font-brand">D02Y</td>
                    </tr>
                    <tr >
                        <td>คุณภาพ</td>
                        <td colspan="7" class="m--font-brand">Outsource เกรด A (ใยอันฮุย/ซานตง)</td>

                        <!--                                                <td>กลุ่มวัตถุดิบ</td>-->
                        <!--                                                <td class="m--font-brand">MULTI MONO</td>-->
                        <!--                                                <td>วัตถุดิบ</td>-->
                        <!--                                                <td class="m--font-brand">MONO ตีเกลียว</td>-->
                        <!--                                                <td>กระบวนการผลิต</td>-->
                        <!--                                                <td class="m--font-brand">D02Y</td>-->
                    </tr>
                    <tr class="wa-no-border">
                        <td>ประเภทจำนวนตา</td>
                        <td class="m--font-brand">MD</td>
                        <td>รหัสขนาดตา</td>
                        <td class="m--font-brand">*112</td>
                        <td>ขนาดตา</td>
                        <td class="m--font-brand">3.81 cm.</td>
                        <td>จำนวนตา</td>
                        <td class="m--font-brand">25</td>
                    </tr>
                    <tr class="wa-no-border">
                        <td>รหัสประเภทการทอ</td>
                        <td class="m--font-brand">01</td>
                        <td>ชื่อประเภทการทอ</td>
                        <td class="m--font-brand">ทอปกติ</td>
                        <td>กลุ่มเครื่องทอหลัก</td>
                        <td class="m--font-brand">DH 1</td>
                        <td>รหัสเครื่องทอย่อย</td>
                        <td class="m--font-brand">01</td>
                    </tr>
                    <tr>
                        <td>ประเภทใย</td>
                        <td class="m--font-brand">โมโน ใยโมโน รุ่นไจ ชุบนุ่ม</td>
                        <td>ขนาดใย</td>
                        <td class="m--font-brand">0.23 mm.</td>
                        <td>เบอร์ใย</td>
                        <td colspan="3" class="m--font-brand">23</td>
                    </tr>
                    <tr class="wa-no-border">
                        <td>การอบ</td>
                        <td class="m--font-brand">YOKO</td>
                        <td>ยาว</td>
                        <td class="m--font-brand">18.29 m.</td>
                        <td>น้ำหนักมาตรฐาน</td>
                        <td class="m--font-brand">1,000 กรัม/ผืน</td>
                        <td>เงื่อน</td>
                        <td class="m--font-brand">0.90 kg / spool</td>
                    </tr>
                    <tr>
                        <td>สี</td>
                        <td class="m--font-brand">RED L026</td>
                        <td>จำนวนเส้น</td>
                        <td colspan="5" class="m--font-brand">- เส้น</td>
                    </tr>
                    <tr class="wa-no-border">
                        <td>ลักษณะหู</td>
                        <td class="m--font-brand">หูทอ G : หูปกติ</td>
                        <td>หู</td>
                        <td class="m--font-brand">1MD SSTB BY 0.20MM</td>
                        <td>สีหู</td>
                        <td colspan="3" class="m--font-brand">NATURAL WHITE M206</td>
                        <!--                                                <td></td>-->
                        <!--                                                <td class="m--font-brand"></td>-->
                    </tr>
                    <tr>
                        <td>หูอบ</td>
                        <td colspan="9" class="m--font-brand">Double (หูคู่อบ)</td>
                    </tr>
                    <tr>
                        <td>TB</td>
                        <td colspan="9" class="m--font-brand">0.5md DSTB by 210/12<br>2 : 210/12 0.5md<br>2 : 210/12 0.5md</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <button type="button" class="btn btn-brand" data-dismiss="modal"><i class="fa fa-save"></i> ดำเนินการต่อ</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ยกเลิก</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="waModal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >

    <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    รายละเอียดสอบถาม
                    <span class="m--font-brand">DEQ1807060</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer" style="height: auto;">
                <div class="m-dropdown__wrapper wa-width-auto">
                    <span class="m-dropdown__arrow light m-dropdown__arrow--left"></span><!-- light -->
                    <div class="m-dropdown__inner at_inner_light"><!-- light -->
                        <div class="m-dropdown__body">
                            <div class="m-dropdown__content at-m-dropdown__content">
                                <table class="table light table-striped- table-bordered"><!-- light   table-hover-->
                                    <tbody>
                                    <tr>
                                        <td>เลขที่สอบถาม</td>
                                        <td class="m--font-brand">DEQ0000003</td>
                                        <td>วันที่สอบถาม</td>
                                        <td class="m--font-brand">01/11/2018</td>
                                    </tr>
                                    <tr>
                                        <td>สถานะ</td>
                                        <td class="m--font-warning">รอตลาดอนุมัติ</td>
                                        <td>ลูกค้า/กลุ่ม</td>
                                        <td class="m--font-brand">Al Sabban</td>
                                    </tr>
                                    <tr>
                                        <td>ประเทศ</td>
                                        <td class="m--font-brand">YEMEN</td>
                                        <td>การขาย</td>
                                        <td class="m--font-brand">ขายต่าง</td>
                                    </tr>
                                    <tr>
                                        <td>เลขที่ออเดอร์</td>
                                        <td class="m--font-brand">-</td>
                                        <td>วันที่สั่งซื้อ</td>
                                        <td class="m--font-brand">-</td>
                                    </tr>
                                    <tr>
                                        <td>วันที่โอนข้อมูลสั่งผลิต</td>
                                        <td class="m--font-brand">-</td>
                                        <td>จำนวนสินค้า</td>
                                        <td class="m--font-brand">5 รายการ</td>
                                    </tr>
                                    <tr>
                                        <td>จำนวนสั่งซื้อ</td>
                                        <td class="m--font-brand">1,000 ผืน (10,000 กก.)</td>
                                        <td>เลขที่ใบสั่งผลิต</td>
                                        <td class="m--font-brand">-</td>
                                    </tr>
                                    <tr>
                                        <td>วันที่ปิด Lot ตามแผนครั้งแรก</td>
                                        <td class="m--font-brand">27/12/2018</td>
                                        <td>วันที่ปิด Lot จริง</td>
                                        <td class="m--font-brand">27/1/2019</td>
                                    </tr>
                                    <tr>
                                        <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                        <td colspan="3" class="m--font-brand">15/11/2018 - 26/12/2018</td>
                                    </tr>
                                    <tr>
                                        <td>แผนผลิตส่วนหลัง เริ่ม - เสร็จ</td>
                                        <td colspan="3" class="m--font-brand">22/11/2018 - 27/12/2018</td>
                                    </tr>
                                    <tr>
                                        <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                        <td colspan="3" class="m--font-brand">24/11/2018 - 30/12/2018</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <button type="button" class="btn btn-brand" data-dismiss="modal"><i class="fa fa-save"></i> ดำเนินการต่อ</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ยกเลิก</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="waModal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog at-modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    รายละเอียดสอบถาม
                    <span class="m--font-brand">DEQ1807060</span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body at-modal-body-and-footer" style="height: auto;">
                <div class="m-dropdown__wrapper wa-width-auto">
                    <span class="m-dropdown__arrow light m-dropdown__arrow--left"></span><!-- light -->
                    <div class="m-dropdown__inner at_inner_light"><!-- light -->
                        <div class="m-dropdown__body">
                            <div class="m-dropdown__content at-m-dropdown__content">
                                <table class="table light table-striped- table-bordered"><!-- light   table-hover-->
                                    <tbody>
                                    <tr>
                                        <td>เลขที่สอบถาม</td>
                                        <td class="m--font-brand">DEQ0000003</td>
                                        <td>วันที่สอบถาม</td>
                                        <td class="m--font-brand">01/11/2018</td>
                                    </tr>
                                    <tr>
                                        <td>สถานะ</td>
                                        <td class="m--font-warning">รอตลาดอนุมัติ</td>
                                        <td>ลูกค้า/กลุ่ม</td>
                                        <td class="m--font-brand">Al Sabban</td>
                                    </tr>
                                    <tr>
                                        <td>ประเทศ</td>
                                        <td class="m--font-brand">YEMEN</td>
                                        <td>การขาย</td>
                                        <td class="m--font-brand">ขายต่าง</td>
                                    </tr>
                                    <tr>
                                        <td>เลขที่ออเดอร์</td>
                                        <td class="m--font-brand">-</td>
                                        <td>วันที่สั่งซื้อ</td>
                                        <td class="m--font-brand">-</td>
                                    </tr>
                                    <tr>
                                        <td>วันที่โอนข้อมูลสั่งผลิต</td>
                                        <td class="m--font-brand">-</td>
                                        <td>จำนวนสินค้า</td>
                                        <td class="m--font-brand">5 รายการ</td>
                                    </tr>
                                    <tr>
                                        <td>จำนวนสั่งซื้อ</td>
                                        <td class="m--font-brand">1,000 ผืน (10,000 กก.)</td>
                                        <td>เลขที่ใบสั่งผลิต</td>
                                        <td class="m--font-brand">-</td>
                                    </tr>
                                    <tr>
                                        <td>วันที่ปิด Lot ตามแผนครั้งแรก</td>
                                        <td class="m--font-brand">27/12/2018</td>
                                        <td>วันที่ปิด Lot จริง</td>
                                        <td class="m--font-brand">27/1/2019</td>
                                    </tr>
                                    <tr>
                                        <td>แผนทอ เริ่มทอ - ทอครบ</td>
                                        <td colspan="3" class="m--font-brand">15/11/2018 - 26/12/2018</td>
                                    </tr>
                                    <tr>
                                        <td>แผนผลิตส่วนหลัง เริ่ม - เสร็จ</td>
                                        <td colspan="3" class="m--font-brand">22/11/2018 - 27/12/2018</td>
                                    </tr>
                                    <tr>
                                        <td>สินค้าเข้าคลัง เริ่ม - เสร็จ</td>
                                        <td colspan="3" class="m--font-brand">24/11/2018 - 30/12/2018</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer m-stack__item m-stack__item--center">
                <button type="button" class="btn btn-brand" data-dismiss="modal"><i class="fa fa-save"></i> ดำเนินการต่อ</button>
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ยกเลิก</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_amountChuthor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">สินค้าที่สามารถทอพ่วงได้</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered m-table">
                    <thead>
                    <tr>
                        <th class="wa_td_style" >รหัสสินค้าตลาด</th>
                        <th class="wa_td_style" >ชื่อสินค้า</th>
                        <th class="wa_td_style" >วันที่เริ่ม</th>
                        <th class="wa_td_style" >วันที่ทอเสร็จ</th>
                        <th class="wa_td_style" >จำนวน</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>H01400*11260250</td>
                        <td>0.14mm 1.1/2"*25md*20yd DK YOKO NW M206</td>
                        <td>1/12/2018</td>
                        <td>1/1/2019</td>
                        <td>140 ผืน</td>
                    </tr>
                    <tr>
                        <td>H0A500H82010900</td>
                        <td>110/2 3.70cm*90md*12m SK YOKO BLACK N101</td>
                        <td>5/12/2018</td>
                        <td>10/1/2019</td>
                        <td>90 ผืน</td>
                    </tr>
                    </tbody>
                </table>

<!--                <hr>-->
<!--                <div class="row">-->
<!--                    <div class="col-lg-3">-->
<!--                        รายการที่ต้องการรวม-->
<!--                    </div>-->
<!--                    <div class="col-lg-8">-->
<!--                        <div class="wa-combine-box2">-->
<!--                            <p>รหัสผลิต : 1119P/013010400</p>-->
<!--                            <p>ชื่อสินค้า : DEQ1807060</p>-->
<!--                            <p>วันที่เริ่ม : 1/12/2018</p>-->
<!--                            <p>วันที่ทอเสร็จ : 1/1/2019</p>-->
<!--                            <p>จำนวน : 140 ผืน</p>-->
<!--                        </div>-->
<!--                        <div class="wa-combine-box2">-->
<!--                            <p>รหัสผลิต : 25119P/040850403</p>-->
<!--                            <p>ชื่อสินค้า : DEQ1807060</p>-->
<!--                            <p>วันที่เริ่ม : 5/12/2018</p>-->
<!--                            <p>วันที่ทอเสร็จ : 10/1/2019</p>-->
<!--                            <p>จำนวน : 90 ผืน</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-metal-drak" data-dismiss="modal"><i class="fa fa-times"></i> ปิดหน้าต่าง</button>
<!--                <button type="button" class="btn btn-brand" data-dismiss="modal">ตกลง</button>-->
                <!--                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
            </div>
        </div>
    </div>
</div>

<!--end Modal by wa-->


<script>
    // function hide_Function()
    // {
    //     var x = document.getElementById("icon-hide");
    //     console.log("hi");
    //     if(x.style.display === "none")
    //     {
    //         setTimeout(function(){
    //             x.style.display = "block";
    //         },200);
    //     } else {
    //         x.style.display = "none";
    //     }
    // }
</script>


<script type="text/javascript">
    


Highcharts.setOptions({
  lang : {
    months:[
      'มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'
    ]
    
  }
});

Highcharts.ganttChart('container', {

chart: {
    zoomType: 'x' 
  },
  title: {
    text: ''
  }, credits: {
                enabled: false
            }, exporting: {
                enabled: false
            },
xAxis: {type: 'datetime',
    plotLines: [{
    color: '#0000FF',
    dashStyle: 'Solid',
    value: Date.UTC(2562, 1, 7,10,35),
    width: 5,
      label: { 
    text: 'พุธ 06/04/2562 15:35',"rotation": 0,
    style: {
         color: '#0000FF'
      }}
  }],plotBands: [{
    color: '#DC381F',
    from: Date.UTC(2562, 3, 11,10,35),
    to: Date.UTC(2562, 3, 17,10,35),
    label: { 
    text: 'หยุด : 11/04/2562 - 17/04/2562',y:-3,
    style: {
         color: '#FF0000'
      }}
  }],
        min: Date.UTC(2561, 9, 1),
    max: Date.UTC(2562, 11, 18)
  },
tooltip: {
    xDateFormat: '%a %b %d, %H:%M'
  },

  series: [{
    name: 'N3F51//020V100ช',
    data: [{
      start: Date.UTC(2561, 10, 1,15,35),
      end: Date.UTC(2562, 8, 5,22,57),
      completed: 0.31555,
      color: '#ffc107',
      name: 'ฉีดใยก่อนทอ',
      id: 'ฉีดใยก่อนทอ',
      dataLabels: {
        enabled: true,
        align: 'left',
       format:'25.48%'}
    }, {
      start: Date.UTC(2561, 10, 6,0,0),
      end: Date.UTC(2562, 8, 28,0,0),
      completed: 0.2855,
      color: '#28ce44',
      name: 'ทอ',
      id: 'ทอ',
       dataLabels: {
        enabled: true,
        align: 'left',
       format:'22.59%'}
    },{
      start: Date.UTC(2561, 10, 6,0,0),
      end: Date.UTC(2562, 7, 15,0,0),
      completed: 0.33,
      color: '#6cdc7f',
      name: 'เครื่องทอ MC.0001',
      parent: 'ทอ',
       dataLabels: {
        enabled: true,
        align: 'left',
       format:'24.15%'}
    }, {
      start: Date.UTC(2561, 11, 6,0,0),
      end: Date.UTC(2562, 8, 28,0,0),
      completed: 0.213,
      color: '#6cdc7f',
      name: 'เครื่องทอ MC.0002',
      parent: 'ทอ',
       dataLabels: {
        enabled: true,
        align: 'left',
       format:'21.43%'}
    }, {
      start: Date.UTC(2561, 11, 15,0,0),
      end: Date.UTC(2562, 10, 18,0,0),
      completed: 0.16,
      color: '#9900f8',
      name: 'ผลิตส่วนหลัง D02Y',
      id: 'ผลิตส่วนหลัง D02Y',
       dataLabels: {
        enabled: true,
        align: 'left',
       format:'16.64%'}
    }, {
      start: Date.UTC(2561, 11, 15,0,0),
      end: Date.UTC(2562, 9, 8,0,0),
      completed: 0.182,
      color: '#c27eec',
      name: 'สถานี 1',
      parent: 'ผลิตส่วนหลัง D02Y',
       dataLabels: {
        enabled: true,
        align: 'left',
       format:'19.64%'}
    },{
      start: Date.UTC(2561, 11, 20,0,0),
      end: Date.UTC(2562, 9, 15,0,0),
      completed: 0.165,
      color: '#c27eec',
      name: 'สถานี 2',
      parent: 'ผลิตส่วนหลัง D02Y',
       dataLabels: {
        enabled: true,
        align: 'left',
       format:'17.17%'}
    },{
      start: Date.UTC(2561, 11, 25,0,0),
      end: Date.UTC(2562, 9, 30,0,0),
      completed: 0.143,
      color: '#c27eec',
      name: 'สถานี 3',
      parent: 'ผลิตส่วนหลัง D02Y',
       dataLabels: {
        enabled: true,
        align: 'left',
       format:'16.39%'}
    },{
      start: Date.UTC(2561, 11, 30,0,0),
      end: Date.UTC(2562, 10, 18,0,0),
      completed: 0.121,
      color: '#c27eec',
      name: 'สถานี 4',
      parent: 'ผลิตส่วนหลัง D02Y',
       dataLabels: {
        enabled: true,
        align: 'left',
       format:'13.86%'}
    },{
      start: Date.UTC(2561, 12, 5,0,0),
      end: Date.UTC(2562, 10, 18,0,0),
      completed: 0.105,
      color: '#ffb900',
      name: 'ส่งเข้าคลังสินค้า',
       dataLabels: {
        enabled: true,
        align: 'left',
       format:'10.45%'}
    },]
  }]
});


</script>