<!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
                <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
                <!-- BEGIN: Aside Menu -->
                <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" m-menu-scrollable="1" m-menu-dropdown-timeout="500">
                    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                        <!-- <li class="m-menu__section ">
                            <h4 class="m-menu__section-text">ระบบวางแผนการผลิต</h4>
                            <i class="m-menu__section-icon flaticon-more-v2"></i>
                        </li> -->
                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                            <a href="#" class="m-menu__link ">
                                <i class="m-menu__link-icon flaticon-analytics"></i>
                                <span class="m-menu__link-text">หน้าแรก</span>
                            </a>
                        </li>
                        <? if ($select == 'open_mk'){?>
                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--open" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <? }else{?>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <? } ?>
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-interface-8"></i>
                                <span class="m-menu__link-title">
                                        <span class="m-menu__link-wrap"> 
                                            <span class="m-menu__link-text">การตลาด</span>
                                            <!-- <span class="m-menu__link-badge">
                                                <span class="m-badge m-badge--danger m-badge--wide">3</span>
                                            </span> -->
                                        </span>
                                    </span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <? if ($select2 == 'mk_1'){?>
                                        <li class="m-menu__item m-menu__item--active" aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="menu1_mk_1.php" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">รายการรออนุมัติ</span>
                                            </a>
                                        </li>
                                    <? }else{?>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="menu1_mk_1.php" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">รายการรออนุมัติ</span>
                                            </a>
                                        </li>
                                    <? } ?>
                                    <? if ($select2 == 'mk_2'){?>
                                        <li class="m-menu__item m-menu__item--active" aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="menu1_mk_2.php" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">รายการสอบถาม</span>
                                            </a>
                                        </li>
                                    <? }else{?>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="menu1_mk_2.php" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">รายการสอบถาม</span>
                                            </a>
                                        </li>
                                    <? } ?>
                                    <? if ($select2 == 'mk_3'){?>
                                        <li class="m-menu__item m-menu__item--active" aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="menu1_mk_3.php" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">รายการออเดอร์ </span>
                                            </a>
                                        </li>
                                    <? }else{?>
                                        <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                            <a href="menu1_mk_3.php" class="m-menu__link ">
                                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                    <span></span>
                                                </i>
                                                <span class="m-menu__link-text">รายการออเดอร์ </span>
                                            </a>
                                        </li>
                                    <? } ?>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-calendar-3"></i>
                                <span class="m-menu__link-text">การวางแผนการผลิต</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="menu2_plan_1.php" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">วางแผนการผลิต</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="#" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">แผนการผลิตปัจจุบัน</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="#" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">แผนการผลิตโดยรวม</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="#" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">ประวัติวางแผนการผลิต</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-interface-9"></i>
                                <span class="m-menu__link-text">การผลิต</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="#" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">การผลิตปัจจุบัน</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">การผลิตส่วนหน้า</span>
                                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                                        </a>
                                        <div class="m-menu__submenu ">
                                            <span class="m-menu__arrow"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="#" class="m-menu__link ">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-title">
                                                            <span class="m-menu__link-wrap">
                                                                <span class="m-menu__link-text">ลำดับงานการใช้ใย</span>
                                                            </span>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="#" class="m-menu__link ">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-title">
                                                            <span class="m-menu__link-wrap">
                                                                <span class="m-menu__link-text">ลำดับงานการทอ</span>
                                                            </span>
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item" aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-text">บันทึกข้อมูลผลผลิต</span>
                                                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                                                    </a>
                                                    <div class="m-menu__submenu ">
                                                        <span class="m-menu__arrow"></span>
                                                        <ul class="m-menu__subnav">
                                                            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                                <a href="#" class="m-menu__link ">
                                                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                                        <span></span>
                                                                    </i>
                                                                    <span class="m-menu__link-title">
                                                                        <span class="m-menu__link-wrap">
                                                                            <span class="m-menu__link-text">เครื่องฉีดใย / ชักใย</span>
                                                                        </span>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                            <li class="m-menu__item" aria-haspopup="true" m-menu-link-redirect="1">
                                                                <a href="menu3_product_2_3_2.php" class="m-menu__link ">
                                                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                                        <span></span>
                                                                    </i>
                                                                    <span class="m-menu__link-title">
                                                                        <span class="m-menu__link-wrap">
                                                                            <span class="m-menu__link-text">เครื่องทอ</span>
                                                                        </span>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="#" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">การผลิตส่วนหลัง</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-calendar"></i>
                                <span class="m-menu__link-text">การซ่อมบำรุง</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="#" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">การซ่อมบำรุงปัจจุบัน</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="#" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">แผนงานซ่อมบำรุง</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="#" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">ข้อมูลการจอดเครื่องจาก PLC</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon flaticon-settings-1"></i>
                                <span class="m-menu__link-text">การตั้งค่า</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">ข้อมูลหลัก</span>
                                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                                        </a>
                                        <div class="m-menu__submenu ">
                                            <span class="m-menu__arrow"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-text">สาขา</span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="menu4_setting_1_2.php" class="m-menu__link ">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-text">โรงทอ</span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="#" class="m-menu__link ">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-text">สินค้า</span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="#" class="m-menu__link ">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-text">ลูกค้า</span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="#" class="m-menu__link ">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-text">เครื่องจักร</span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="#" class="m-menu__link ">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-text">กระบวนการผลิต</span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="#" class="m-menu__link ">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-text">ซ่อมบำรุง</span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="#" class="m-menu__link ">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-text">ข้อมูลการทำงาน</span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                                    <a href="#" class="m-menu__link ">
                                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                            <span></span>
                                                        </i>
                                                        <span class="m-menu__link-text">ข้อมูลอ้างอิง</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="#" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">สูตรคำนวณและเงื่อนไข</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="#" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">ตั้งค่าระบบ</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
                                        <a href="#" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                                <span></span>
                                            </i>
                                            <span class="m-menu__link-text">ภาษา</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- END: Aside Menu -->
            </div>
            <!-- END: Left Aside -->