<?php

$tokenId = date('YdmHis').'_'.rand(1000,9999);

?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />
    <title>โปรแกรมวางแผนการผลิต</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
    </script>
    <!--end::Web font -->
    <!--begin:: Global Mandatory Vendors -->
    <link href="vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <!--end:: Global Mandatory Vendors -->
    <!--begin:: Global Optional Vendors -->
    <link href="vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
    <link href="vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="vendors/bootstrap-datetime-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="vendors/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
    <link href="vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
    <link href="vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
    <link href="vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
    <link href="vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link href="vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
    <link href="vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
    <link href="vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
    <link href="vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
    <link href="vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
    <link href="vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
    <link href="vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
    <link href="vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
    <link href="vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
    <link href="vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link href="vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <link href="vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
    <link href="vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
    <!--end:: Global Optional Vendors -->
    <!--begin::Global Theme Styles -->
    <link href="assets/demo/base/style.bundle.css?token=<?=$tokenId?>" rel="stylesheet" type="text/css" />
    <link href="assets/demo/base/at.style.bundle.css?token=<?=$tokenId?>" rel="stylesheet" type="text/css" />
	<link href="assets/demo/base/nut.style.bundle.css?token=<?=$tokenId?>" rel="stylesheet" type="text/css" />
    <link href="assets/demo/base/wa.style.bundle.css?token=<?=$tokenId?>" rel="stylesheet" type="text/css" />
    <!--RTL version:<link href="assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <!--end::Global Theme Styles -->
    <!--begin::Page Vendors Styles -->
    <link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--RTL version:<link href="assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <!--begin::Page Vendors Styles -->
    <link href="assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <!--begin::Page Vendors Styles -->
    <link href="assets/component-custom-switch.css" rel="stylesheet" type="text/css" />

    <!--end::Page Vendors Styles -->
    <link rel="shortcut icon" href="logo512_blue.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="logo512_blue.png">
    <link rel="icon" type="image/png" href="logo512_blue.png" sizes="32x32">
    <link rel="icon" type="image/png" href="logo512_blue.png" sizes="16x16">
    <link rel="mask-icon" href="logo512_blue.svg" color="#007bff">
</head>
<!-- end::Head -->
<!-- begin::Body -->

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-aside--offcanvas-default">
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">